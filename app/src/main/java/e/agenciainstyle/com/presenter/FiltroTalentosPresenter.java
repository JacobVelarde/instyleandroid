package e.agenciainstyle.com.presenter;

import android.content.Context;

import e.agenciainstyle.com.model.TalentoPanel;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.FiltraTalentoRequest;
import e.agenciainstyle.com.model.request.IdRequest;
import e.agenciainstyle.com.model.response.TalentoResponse;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FiltroTalentosPresenter {

    public static final int PRIMERA_CARGA = 9;
    public static final int FILTRO = 10;
    public static final int ADD_TALENTO = 12;

    private Context mContext;
    private ServerImp serverImp;

    public FiltroTalentosPresenter(Context mContext, ServerImp serverImp){
        this.mContext = mContext;
        this.serverImp = serverImp;
    }

    public void cargaImagenesPrincipales(User user){
        Retrofit retrofit = ServerConexion.getConexion(user.getId(), user.getToken());
        Server server = retrofit.create(Server.class);

        Call<TalentoResponse> talentoResponseCall = server.talentosPanel(user.getId());

        talentoResponseCall.enqueue(new Callback<TalentoResponse>() {
            @Override
            public void onResponse(Call<TalentoResponse> call, Response<TalentoResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO || codigo == 206){
                            serverImp.serverSuccess(response.body(), PRIMERA_CARGA);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), PRIMERA_CARGA);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", PRIMERA_CARGA);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", PRIMERA_CARGA);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", PRIMERA_CARGA);
                }
            }

            @Override
            public void onFailure(Call<TalentoResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", PRIMERA_CARGA);

            }
        });
    }

    public void agregaTalentoDreamTeam(User user, TalentoPanel talentoPanel){

        Retrofit retrofit = ServerConexion.getConexion(user.getId(), user.getToken());
        Server server = retrofit.create(Server.class);
        Call<GenericResponse> call = server.addDreamTeam(new IdRequest(talentoPanel.getUser_id()));

        call.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_MENSAJE_SI_ES_NECESARIO){
                            serverImp.serverSuccess(response.body(), ADD_TALENTO);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), ADD_TALENTO);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", ADD_TALENTO);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", ADD_TALENTO);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", ADD_TALENTO);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", ADD_TALENTO);
            }
        });
    }

    public void aplicarFiltro(FiltraTalentoRequest filtraTalentoRequest, User user){
        Retrofit retrofit = ServerConexion.getConexion(user.getId(), user.getToken());
        Server server = retrofit.create(Server.class);

        Call<TalentoResponse> talentoResponseCall = server.filtroTalentos(filtraTalentoRequest);
        talentoResponseCall.enqueue(new Callback<TalentoResponse>() {
            @Override
            public void onResponse(Call<TalentoResponse> call, Response<TalentoResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO || codigo == 206){
                            serverImp.serverSuccess(response.body(), FILTRO);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), FILTRO);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", FILTRO);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", FILTRO);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", FILTRO);
                }
            }

            @Override
            public void onFailure(Call<TalentoResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", FILTRO);
            }
        });
    }
}
