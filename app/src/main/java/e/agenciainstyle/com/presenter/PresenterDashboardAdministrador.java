package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.request.EventoCalendarResponse;
import e.agenciainstyle.com.model.response.DashBoardAdministradorResponse;
import e.agenciainstyle.com.model.response.EventoDetalleDiaResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PresenterDashboardAdministrador {

    public static final int PANEL = 565;
    public static final int EVENTOS_CALENDAR = 320;
    public static final int EVENTOS_DIA = 240;

    private ServerImp serverImp;

    public PresenterDashboardAdministrador(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void getPanel(String panel, String userId, String token){

        Retrofit retrofit = ServerConexion.getConexion(userId, token);
        Server server = retrofit.create(Server.class);
        Call<DashBoardAdministradorResponse> dashBoardAdministradorResponseCall = server.panel(panel);
        dashBoardAdministradorResponseCall.enqueue(new Callback<DashBoardAdministradorResponse>() {
            @Override
            public void onResponse(Call<DashBoardAdministradorResponse> call, Response<DashBoardAdministradorResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO || codigo == 206){
                            serverImp.serverSuccess(response.body(), PANEL);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), PANEL);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", PANEL);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", PANEL);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", PANEL);
                }
            }

            @Override
            public void onFailure(Call<DashBoardAdministradorResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", PANEL);
            }
        });
    }

    public void getEventosCalendario(String anio, String mes, String userId, String token){
        Retrofit retrofit = ServerConexion.getConexion(userId, token);
        Server server = retrofit.create(Server.class);
        Call<EventoCalendarResponse> eventoCalendarResponseCall = server.eventoCalendario(anio+"-"+getNumberMes(mes));
        eventoCalendarResponseCall.enqueue(new Callback<EventoCalendarResponse>() {
            @Override
            public void onResponse(Call<EventoCalendarResponse> call, Response<EventoCalendarResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO){
                            serverImp.serverSuccess(response.body(), EVENTOS_CALENDAR);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), EVENTOS_CALENDAR);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_CALENDAR);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_CALENDAR);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_CALENDAR);
                }
            }

            @Override
            public void onFailure(Call<EventoCalendarResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_CALENDAR);
            }
        });
    }


    public void getDetalleDias(int day, int month, int year, String userId, String token) {
        Retrofit retrofit = ServerConexion.getConexion(userId, token);
        Server server = retrofit.create(Server.class);
        Call<EventoDetalleDiaResponse> eventoDetalleDiaResponseCall = server.getEventoDia(year+"-"+month+"-"+day);
        eventoDetalleDiaResponseCall.enqueue(new Callback<EventoDetalleDiaResponse>() {
            @Override
            public void onResponse(Call<EventoDetalleDiaResponse> call, Response<EventoDetalleDiaResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO){
                            serverImp.serverSuccess(response.body(), EVENTOS_DIA);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), EVENTOS_DIA);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_DIA);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_DIA);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_DIA);
                }
            }

            @Override
            public void onFailure(Call<EventoDetalleDiaResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", EVENTOS_DIA);

            }
        });
    }

    private String getNumberMes(String mes){
        switch (mes){
            case "ENERO":
                return "01";

            case "FEBRERO":
                return "02";

            case "MARZO":
                return "03";

            case "ABRIL":
                return "04";

            case "MAYO":
                return "05";

            case "JUNIO":
                return "06";

            case "JULIO":
                return "07";

            case "AGOSTO":
                return "08";

            case "SEPTIEMBRE":
                return "09";

            case "OCTUBRE":
                return "10";

            case "NOVIEMBRE":
                return "11";

            case "DICIEMBRE":
                return "12";
        }

        return "01";
    }
}
