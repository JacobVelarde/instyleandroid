package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.response.ReportesResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Jacob Velarde on 08,September,2020
 */
public class ReportesPresenter {

    private static final int REPORTE = 0;

    private ServerImp serverImp;

    public ReportesPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void getReportes(String userId, String token){
        Retrofit retrofit = ServerConexion.getConexion(userId, token);
        Server server = retrofit.create(Server.class);
        Call<ReportesResponse> reportesResponseCall = server.listaReportesFavoritos();

        reportesResponseCall.enqueue(new Callback<ReportesResponse>() {
            @Override
            public void onResponse(Call<ReportesResponse> call, Response<ReportesResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), REPORTE);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), REPORTE);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", REPORTE);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", REPORTE);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", REPORTE);
                }
            }

            @Override
            public void onFailure(Call<ReportesResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", REPORTE);
            }
        });
    }
}
