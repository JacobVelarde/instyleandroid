package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.response.BanderasResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BanderasPresenter {

    private static final int BANDERAS = 1000;

    private ServerImp serverImp;

    public BanderasPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void getBanderas(){
        Retrofit retrofit = ServerConexion.getConexion("","");
        Server server = retrofit.create(Server.class);
        Call<BanderasResponse> banderasResponseCall = server.banderasCodigos();

        banderasResponseCall.enqueue(new Callback<BanderasResponse>() {
            @Override
            public void onResponse(Call<BanderasResponse> call, Response<BanderasResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), BANDERAS);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), BANDERAS);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", BANDERAS);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", BANDERAS);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", BANDERAS);
                }
            }

            @Override
            public void onFailure(Call<BanderasResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", BANDERAS);
            }
        });

    }
}
