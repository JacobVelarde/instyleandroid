package e.agenciainstyle.com.presenter;

import android.content.Context;

import e.agenciainstyle.com.model.request.LoginRequest;
import e.agenciainstyle.com.model.response.LoginResponse;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginPresenter {

    public static final int LOGIN = 0;
    public static final int RECUPERAR_CONTRASENA = 1;

    private ServerImp serverImp;
    private Context context;

    public LoginPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void login(String username, String password, String token){

        Retrofit retrofit = ServerConexion.getConexion("","");
        Server server = retrofit.create(Server.class);
        Call<LoginResponse> loginResponseCall = server.login(new LoginRequest(username, password, token, "2"));

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), LOGIN);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), LOGIN);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", LOGIN);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", LOGIN);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", LOGIN);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", LOGIN);
            }
        });
    }

    public void recuperarContraseña(String email){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(email);

        Retrofit retrofit = ServerConexion.getConexion("","");
        Server server = retrofit.create(Server.class);
        Call<GenericResponse> genericResponseCall = server.recuperarContraseña(loginRequest);

        genericResponseCall.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_MENSAJE){
                            serverImp.serverSuccess(response.body(), RECUPERAR_CONTRASENA);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), RECUPERAR_CONTRASENA);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", RECUPERAR_CONTRASENA);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", RECUPERAR_CONTRASENA);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", RECUPERAR_CONTRASENA);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", RECUPERAR_CONTRASENA);
            }
        });
    }
}
