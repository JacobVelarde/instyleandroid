package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.IdNotificacion;
import e.agenciainstyle.com.model.response.NotificacionServicioResponse;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class NotificacionesPresenter {

    public static final int NOTIFICACIONES = 0;

    private ServerImp serverImp;

    public NotificacionesPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void getNotificaciones(String userId, String token){
        Retrofit retrofit = ServerConexion.getConexion(userId,token);
        Server server = retrofit.create(Server.class);
        Call<NotificacionServicioResponse> notificacionServicioResponseCall = server.getNotificaciones();

        notificacionServicioResponseCall.enqueue(new Callback<NotificacionServicioResponse>() {
            @Override
            public void onResponse(Call<NotificacionServicioResponse> call, Response<NotificacionServicioResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), NOTIFICACIONES);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), NOTIFICACIONES);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);
                }
            }

            @Override
            public void onFailure(Call<NotificacionServicioResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);
            }
        });

    }

    public void notificationRead(String userId, String token, String idNotification){
        Retrofit retrofit = ServerConexion.getConexion(userId, token);
        Server server = retrofit.create(Server.class);
        Call<GenericResponse> genericResponseCall = server.notificationRead(new IdNotificacion(idNotification));

        genericResponseCall.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                String algo = "";
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                String algo = "";
            }
        });

    }
}
