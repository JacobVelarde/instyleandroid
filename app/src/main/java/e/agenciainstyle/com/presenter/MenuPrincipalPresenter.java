package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.response.MenuPrincipalEstatusResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class MenuPrincipalPresenter {

    public static final int NOTIFICACIONES = 0;

    private ServerImp serverImp;

    public MenuPrincipalPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }
    public void getStatusPanel(String userId, String token){
        Retrofit retrofit = ServerConexion.getConexion(userId,token);
        Server server = retrofit.create(Server.class);
        Call<MenuPrincipalEstatusResponse> menuPrincipalEstatusResponseCall = server.panelNotificaciones();

        menuPrincipalEstatusResponseCall.enqueue(new Callback<MenuPrincipalEstatusResponse>() {
            @Override
            public void onResponse(Call<MenuPrincipalEstatusResponse> call, Response<MenuPrincipalEstatusResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), NOTIFICACIONES);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), NOTIFICACIONES);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);
                }
            }

            @Override
            public void onFailure(Call<MenuPrincipalEstatusResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", NOTIFICACIONES);

            }
        });

    }

}
