package e.agenciainstyle.com.presenter;

import android.content.Context;

import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.DetalleEventoResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetalleEventoPresenter {

    public static final int DETALLE_EVENTO = 901;

    private ServerImp serverImp;
    private Context context;

    public DetalleEventoPresenter(Context context, ServerImp serverImp){
        this.context = context;
        this.serverImp = serverImp;
    }

    public void getDetalle(User user, String idEvento){
        Retrofit retrofit = ServerConexion.getConexion(user.getId(), user.getToken());
        Server server = retrofit.create(Server.class);
        Call<DetalleEventoResponse> detalleEventoResponseCall = server.detalleEvento(idEvento);

        detalleEventoResponseCall.enqueue(new Callback<DetalleEventoResponse>() {
            @Override
            public void onResponse(Call<DetalleEventoResponse> call, Response<DetalleEventoResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO || codigo == 206){
                            serverImp.serverSuccess(response.body(), DETALLE_EVENTO);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), DETALLE_EVENTO);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", DETALLE_EVENTO);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", DETALLE_EVENTO);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", DETALLE_EVENTO);
                }
            }

            @Override
            public void onFailure(Call<DetalleEventoResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", DETALLE_EVENTO);
            }
        });
    }
}
