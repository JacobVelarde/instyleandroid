package e.agenciainstyle.com.presenter;

import android.content.Context;

import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.DetalleTalentoRequest;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetalleTalentoPresenter {

    public int DETALLE_TALENTO = 122;

    private Context context;
    private ServerImp serverImp;

    public DetalleTalentoPresenter(Context context, ServerImp serverImp){
        this.context = context;
        this.serverImp = serverImp;
    }

    public void getDetalleTalento(String idTalento, User user){
        Retrofit retrofit = ServerConexion.getConexion(user.getId(),user.getToken());
        Server server = retrofit.create(Server.class);

        Call<DetalleTalentoRequest> detalleTalentoRequestCall = server.detalleTalento(idTalento);

        detalleTalentoRequestCall.enqueue(new Callback<DetalleTalentoRequest>() {
            @Override
            public void onResponse(Call<DetalleTalentoRequest> call, Response<DetalleTalentoRequest> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();
                        if (codigo == Constantes.LEER_OBJETO || codigo == 206){
                            serverImp.serverSuccess(response.body(), DETALLE_TALENTO);
                        }else{
                            serverImp.serverError(response.body().getMensaje(), DETALLE_TALENTO);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", DETALLE_TALENTO);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", DETALLE_TALENTO);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", DETALLE_TALENTO);
                }
            }

            @Override
            public void onFailure(Call<DetalleTalentoRequest> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", DETALLE_TALENTO);
            }
        });

    }

}
