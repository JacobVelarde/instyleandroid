package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.request.FiltroActivacion;
import e.agenciainstyle.com.model.response.BusquedaResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BuscarActivacionesPresenter {

    public static final int BUSCA_ACTIVACIONES = 34;

    private ServerImp serverImp;

    public BuscarActivacionesPresenter(ServerImp buscarActivacionesImp){
        this.serverImp = buscarActivacionesImp;
    }

    public void buscarActivaciones(FiltroActivacion filtroActivacion, String userId, String token){
        Retrofit retrofit = ServerConexion.getConexion(userId,token);
        Server server = retrofit.create(Server.class);
        Call<BusquedaResponse> busquedaResponseCall = server.getActivaciones(filtroActivacion);

        busquedaResponseCall.enqueue(new Callback<BusquedaResponse>() {
            @Override
            public void onResponse(Call<BusquedaResponse> call, Response<BusquedaResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), BUSCA_ACTIVACIONES);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), BUSCA_ACTIVACIONES);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", BUSCA_ACTIVACIONES);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", BUSCA_ACTIVACIONES);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", BUSCA_ACTIVACIONES);
                }
            }

            @Override
            public void onFailure(Call<BusquedaResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", BUSCA_ACTIVACIONES);
            }
        });
    }
}
