package e.agenciainstyle.com.presenter;

import android.content.Context;

import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.EdecanRequest;
import e.agenciainstyle.com.model.request.IdRequest;
import e.agenciainstyle.com.model.response.PrimeraVezResponse;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import e.agenciainstyle.com.tools.Preferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FormularioEdecanPresenter {

    public static final int FORMULARIO_EDECAN = 20;
    public static final int PRIMERA_VEZ = 10;

    private ServerImp serverImp;
    private Context context;

    public FormularioEdecanPresenter(ServerImp serverImp, Context context){
        this.serverImp = serverImp;
        this.context = context;
    }

    public void getPrimerosDatos(){
        User user = (User) Preferences.getObjectWithKey(context, Preferences.USER, User.class);
        if (user != null){
            Retrofit retrofit = ServerConexion.getConexion(user.getId(), user.getToken());
            Server server = retrofit.create(Server.class);
            Call<PrimeraVezResponse> primeraVezResponseCall = server.primeraVez(new IdRequest(user.getId()));

            primeraVezResponseCall.enqueue(new Callback<PrimeraVezResponse>() {
                @Override
                public void onResponse(Call<PrimeraVezResponse> call, Response<PrimeraVezResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            if (response.body().getCodigo() == Constantes.LEER_OBJETO){
                                serverImp.serverSuccess(response.body(), PRIMERA_VEZ);

                            }else if (response.body().getCodigo() == Constantes.GO_DASHBOARD){
                                serverImp.serverSuccess(null, PRIMERA_VEZ);
                            }else{
                                serverImp.serverError("Intente de nuevo mas tarde", PRIMERA_VEZ);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<PrimeraVezResponse> call, Throwable t) {
                    serverImp.serverError("Intente de nuevo mas tarde", PRIMERA_VEZ);

                }
            });
        }
    }

    public void save(EdecanRequest edecanRequest){

        User user = (User) Preferences.getObjectWithKey(context, Preferences.USER, User.class);

        if (user != null){
            Retrofit retrofit = ServerConexion.getConexion(user.getId(),user.getToken());
            Server server = retrofit.create(Server.class);
            Call<GenericResponse> genericResponseCall = server.actualizarDatos(edecanRequest);

            genericResponseCall.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            int codigo = response.body().getCodigo();
                            if (codigo == Constantes.LEER_MENSAJE){
                                serverImp.serverSuccess(response.body(), FORMULARIO_EDECAN);
                            }else{
                                serverImp.serverError(response.body().getMensaje(), FORMULARIO_EDECAN);
                            }
                        }else{
                            serverImp.serverError("Intente de nuevo mas tarde", FORMULARIO_EDECAN);
                        }
                    }else if (response.errorBody() != null){
                        serverImp.serverError("Intente de nuevo mas tarde", FORMULARIO_EDECAN);

                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", FORMULARIO_EDECAN);
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    serverImp.serverError("Intente de nuevo mas tarde", FORMULARIO_EDECAN);
                }
            });

        }
    }

    public void uploadPrimeraVezDatos(String id_usuario,
                                      String pathSelfieRostro,
                                      String pathSelfieCompleto,
                                      String pathPolaUno,
                                      String pathPolaDos,
                                      String pathPolaTres,
                                      String pathPolaCuatro,
                                      String pathPolaCinco,
                                      String pathPolaSeis,
                                      String docUno,
                                      String docDos,
                                      String video){

        User user = (User) Preferences.getObjectWithKey(context, Preferences.USER, User.class);
        if (user != null){
            Retrofit retrofit = ServerConexion.getConexion(user.getId(), user.getToken());
            Server server = retrofit.create(Server.class);
            //Call<GenericResponse> genericResponseCall = server.primeraVezDatos()
        }
    }
}
