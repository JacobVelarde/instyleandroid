package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.response.PerfilResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Jacob Velarde on 06,September,2020
 */
public class MiPerfilPresenter {

    public static final int PERFIL = 0;

    private ServerImp serverImp;

    public MiPerfilPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void getPerfil(String userId, String token){
        Retrofit retrofit = ServerConexion.getConexion(userId,token);
        Server server = retrofit.create(Server.class);
        Call<PerfilResponse> perfilRequest = server.getPerfil();

        perfilRequest.enqueue(new Callback<PerfilResponse>() {
            @Override
            public void onResponse(Call<PerfilResponse> call, Response<PerfilResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_OBJETO){

                            serverImp.serverSuccess(response.body(), PERFIL);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), PERFIL);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", PERFIL);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", PERFIL);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", PERFIL);
                }
            }

            @Override
            public void onFailure(Call<PerfilResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", PERFIL);

            }
        });
    }
}
