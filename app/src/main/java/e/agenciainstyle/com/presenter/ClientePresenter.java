package e.agenciainstyle.com.presenter;

import e.agenciainstyle.com.model.Cliente;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ClientePresenter {

    public static final int REGISTRO_CLIENTE = 10;

    private ServerImp serverImp;

    public ClientePresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void registrar(Cliente cliente){

        Retrofit retrofit = ServerConexion.getConexion("","");
        Server server = retrofit.create(Server.class);
        Call<GenericResponse> genericResponseCall = server.registroCliente(cliente);

        genericResponseCall.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_MENSAJE){

                            serverImp.serverSuccess(response.body(), REGISTRO_CLIENTE);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), REGISTRO_CLIENTE);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_CLIENTE);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_CLIENTE);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_CLIENTE);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_CLIENTE);
            }
        });

    }


}
