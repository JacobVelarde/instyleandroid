package e.agenciainstyle.com.presenter;

import android.content.Context;

import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.AgregaTalentoRequest;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AgregaTalentoPresenter {

    public static int TALENTO = 10;

    private Context context;
    private ServerImp serverImp;

    public AgregaTalentoPresenter(Context context, ServerImp serverImp){
        this.context = context;
        this.serverImp = serverImp;
    }

    public void addTalento(AgregaTalentoRequest agregaTalentoRequest, User user){
        Retrofit retrofit = ServerConexion.getConexion(user.getId(),user.getToken());
        Server server = retrofit.create(Server.class);
        Call<GenericResponse> genericResponseCall = server.agregaTalento(agregaTalentoRequest);

        genericResponseCall.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_MENSAJE_SI_ES_NECESARIO){

                            serverImp.serverSuccess(response.body(), TALENTO);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), TALENTO);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", TALENTO);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", TALENTO);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", TALENTO);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", TALENTO);
            }
        });
    }
}
