package e.agenciainstyle.com.presenter;

import java.io.File;

import e.agenciainstyle.com.model.Talento;
import e.agenciainstyle.com.model.response.FotosUneteTalentoResponse;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.Server;
import e.agenciainstyle.com.server.ServerConexion;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Constantes;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TalentoPresenter {

    public static final int REGISTRO_TALENTO = 400;
    public static final int UPLOAD_FOTOS = 20;

    private ServerImp serverImp;

    public TalentoPresenter(ServerImp serverImp){
        this.serverImp = serverImp;
    }

    public void registrar(Talento talento){

        Retrofit retrofit = ServerConexion.getConexion("","");
        Server server = retrofit.create(Server.class);
        Call<FotosUneteTalentoResponse> genericResponseCall = server.registroTalento(talento);

        genericResponseCall.enqueue(new Callback<FotosUneteTalentoResponse>() {
            @Override
            public void onResponse(Call<FotosUneteTalentoResponse> call, Response<FotosUneteTalentoResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        //if (codigo == Constantes.LEER_MENSAJE){

                            serverImp.serverSuccess(response.body(), REGISTRO_TALENTO);

                        //}else{
                        //    serverImp.serverError(response.body().getMensaje(), REGISTRO_TALENTO);
                        //}
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_TALENTO);
                    }
                }else if (response.errorBody() != null){
                    serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_TALENTO);

                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_TALENTO);
                }
            }

            @Override
            public void onFailure(Call<FotosUneteTalentoResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", REGISTRO_TALENTO);
            }
        });

    }

    public void uploadFotos(String idSolicitud,
                            String pathPerfil,
                            String pathBook1,
                            String pathBook2,
                            String pathBook3,
                            String pathBook4,
                            String pathBook5){

        File filePerfil = new File(pathPerfil);
        File fileBook1 = new File(pathBook1);
        File fileBook2 = new File(pathBook2);
        File fileBook3 = new File(pathBook3);
        File fileBook4 = new File(pathBook4);
        File fileBook5 = new File(pathBook5);


        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), idSolicitud);
        RequestBody perfilRequest = RequestBody.create(MediaType.parse("image/*"), filePerfil);
        RequestBody book1Request = RequestBody.create(MediaType.parse("image/*"), fileBook1);
        RequestBody book2Request = RequestBody.create(MediaType.parse("image/*"), fileBook2);
        RequestBody book3Request = RequestBody.create(MediaType.parse("image/*"), fileBook3);
        RequestBody book4Request = RequestBody.create(MediaType.parse("image/*"), fileBook4);
        RequestBody book5Request = RequestBody.create(MediaType.parse("image/*"), fileBook5);

        MultipartBody.Part imagenPerfil = MultipartBody.Part.createFormData("perfil", filePerfil.getName(), perfilRequest);
        MultipartBody.Part imagenBook1 = MultipartBody.Part.createFormData("book_1", fileBook1.getName(), book1Request);
        MultipartBody.Part imagenBook2 = MultipartBody.Part.createFormData("book_2", fileBook2.getName(), book2Request);
        MultipartBody.Part imagenBook3 = MultipartBody.Part.createFormData("book_3", fileBook3.getName(), book3Request);
        MultipartBody.Part imagenBook4 = MultipartBody.Part.createFormData("book_4", fileBook4.getName(), book4Request);
        MultipartBody.Part imagenBook5 = MultipartBody.Part.createFormData("book_5", fileBook5.getName(), book5Request);
        MultipartBody.Part imagenBook6 = MultipartBody.Part.createFormData("book_6", fileBook5.getName(), book5Request);


        Retrofit retrofit = ServerConexion.getConexion("","");
        Server server = retrofit.create(Server.class);
        Call<GenericResponse> genericResponseCall = server.uploadFotosTalento(
                id,
                imagenPerfil,
                imagenBook1,
                imagenBook2,
                imagenBook3,
                imagenBook4,
                imagenBook5,
                imagenBook6);

        genericResponseCall.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        int codigo = response.body().getCodigo();

                        if (codigo == Constantes.LEER_MENSAJE_SI_ES_NECESARIO){

                            serverImp.serverSuccess(response.body(), UPLOAD_FOTOS);

                        }else{
                            serverImp.serverError(response.body().getMensaje(), UPLOAD_FOTOS);
                        }
                    }else{
                        serverImp.serverError("Intente de nuevo mas tarde", UPLOAD_FOTOS);
                    }
                }else{
                    serverImp.serverError("Intente de nuevo mas tarde", UPLOAD_FOTOS);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                serverImp.serverError("Intente de nuevo mas tarde", UPLOAD_FOTOS);
            }
        });

    }
}
