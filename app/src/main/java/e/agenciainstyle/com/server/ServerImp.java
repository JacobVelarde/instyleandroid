package e.agenciainstyle.com.server;

public interface ServerImp {
    void serverSuccess(Object response, int request);
    void serverSuccess(int request);
    void serverError(String error, int request);
}