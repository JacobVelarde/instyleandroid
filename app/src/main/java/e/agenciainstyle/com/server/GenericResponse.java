package e.agenciainstyle.com.server;

public class GenericResponse {

    private int codigo;
    private String mensaje;

    public GenericResponse() {
    }

    public GenericResponse(int codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "GenericResponse{" +
                "codigo=" + codigo +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}
