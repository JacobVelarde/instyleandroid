package e.agenciainstyle.com.server;

import e.agenciainstyle.com.model.Cliente;
import e.agenciainstyle.com.model.IdNotificacion;
import e.agenciainstyle.com.model.Talento;
import e.agenciainstyle.com.model.request.AgregaTalentoRequest;
import e.agenciainstyle.com.model.request.EdecanRequest;
import e.agenciainstyle.com.model.request.EventoCalendarResponse;
import e.agenciainstyle.com.model.request.FiltraTalentoRequest;
import e.agenciainstyle.com.model.request.FiltroActivacion;
import e.agenciainstyle.com.model.request.IdRequest;
import e.agenciainstyle.com.model.request.LoginRequest;
import e.agenciainstyle.com.model.response.BanderasResponse;
import e.agenciainstyle.com.model.response.BusquedaResponse;
import e.agenciainstyle.com.model.response.DashBoardAdministradorResponse;
import e.agenciainstyle.com.model.response.DetalleEventoResponse;
import e.agenciainstyle.com.model.response.DetalleTalentoRequest;
import e.agenciainstyle.com.model.response.EventoDetalleDiaResponse;
import e.agenciainstyle.com.model.response.FotosUneteTalentoResponse;
import e.agenciainstyle.com.model.response.LoginResponse;
import e.agenciainstyle.com.model.response.MenuPrincipalEstatusResponse;
import e.agenciainstyle.com.model.response.NotificacionServicioResponse;
import e.agenciainstyle.com.model.response.PerfilResponse;
import e.agenciainstyle.com.model.response.PrimeraVezResponse;
import e.agenciainstyle.com.model.response.ReportesResponse;
import e.agenciainstyle.com.model.response.TalentoResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Server {
    String BASE_URL = "https://rest.agenciainstyle.com/";

    @POST("auth/login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("auth/recuperar")
    Call<GenericResponse> recuperarContraseña(@Body LoginRequest loginRequest);

    @GET("registro/getCodigos")
    Call<BanderasResponse> banderasCodigos();

    @POST("registro/cliente")
    Call<GenericResponse> registroCliente(@Body Cliente cliente);

    @POST("registro/talento")
    Call<FotosUneteTalentoResponse> registroTalento(@Body Talento talento);

    @Multipart
    @POST("/registro/carga")
    Call<GenericResponse> uploadFotosTalento(@Part("id_solicitud") RequestBody idSolicitud,
                                             @Part MultipartBody.Part perfil,
                                             @Part MultipartBody.Part book_1,
                                             @Part MultipartBody.Part book_2,
                                             @Part MultipartBody.Part book_3,
                                             @Part MultipartBody.Part book_4,
                                             @Part MultipartBody.Part book_5,
                                             @Part MultipartBody.Part book_6);

    @POST("talento/actualizarDatos")
    Call<GenericResponse> actualizarDatos(@Body EdecanRequest edecanRequest);

    @GET("admin/panel")
    Call<DashBoardAdministradorResponse> panel(@Query("pantalla") String pantalla);

    @GET("admin/detalleEvento/{idEvento}")
    Call<DetalleEventoResponse> detalleEvento(@Path("idEvento") String idEvento);

    @GET("admin/calendario")
    Call<EventoCalendarResponse> eventoCalendario(@Query("anio_mes") String anioMes);

    @GET("admin/calendarioDetalle")
    Call<EventoDetalleDiaResponse> getEventoDia(@Query("anio_mes_dia") String anioMesDia);

    @POST("admin/activaciones")
    Call<BusquedaResponse> getActivaciones(@Body FiltroActivacion filtroActivacion);

    @POST("talento/perfil")
    Call<PrimeraVezResponse> primeraVez(@Body IdRequest idRequest);

    @Multipart
    @POST("registro/completaperfildocs")
    Call<GenericResponse> primeraVezDatos(@Part("id_usuario") RequestBody idUsuario,
                                          @Part MultipartBody.Part selfie_rostro,
                                          @Part MultipartBody.Part selfie_ccompleto,
                                          @Part MultipartBody.Part pola_1,
                                          @Part MultipartBody.Part pola_2,
                                          @Part MultipartBody.Part pola_3,
                                          @Part MultipartBody.Part pola_4,
                                          @Part MultipartBody.Part pola_5,
                                          @Part MultipartBody.Part pola_6,
                                          @Part MultipartBody.Part doc_2,
                                          @Part MultipartBody.Part video);

    @GET("admin/paneltalentos")
    Call<TalentoResponse> talentosPanel(@Query("user_id") String userId);

    @POST("admin/filtrartalentos")
    Call<TalentoResponse> filtroTalentos(@Body FiltraTalentoRequest filtraTalentoRequest);

    @POST("admin/agregatalento")
    Call<GenericResponse> agregaTalento(@Body AgregaTalentoRequest agregaTalentoRequest);

    @GET("admin/detalletalento")
    Call<DetalleTalentoRequest> detalleTalento(@Query("user_id") String userId);

    @POST("dreamteam/add")
    Call<GenericResponse> addDreamTeam(@Body IdRequest userId);

    @GET("notification/panel")
    Call<MenuPrincipalEstatusResponse> panelNotificaciones();

    @GET("notification/get")
    Call<NotificacionServicioResponse> getNotificaciones();

    @POST("notification/read")
    Call<GenericResponse> notificationRead(@Body IdNotificacion idNotification);

    @GET("/perfil")
    Call<PerfilResponse> getPerfil();

    @GET("/activaciones/getfav")
    Call<ReportesResponse> listaReportesFavoritos();

    /*@GET("OperacionesApp")
    Call<LoginFaseDos> loginF2(@Query("idOperacion") String operacion,
                               @Query("codigo") String codigo,
                               @Query("token") String token);

    @GET("OperacionesApp")
    Call<Recarga> recarga(@Query("idOperacion") String operacion,
                          @Query("telefono") String telefono,
                          @Query("firma") String firma,
                          @Query("gpsLatitud") String latitud,
                          @Query("gpsLongitud") String longitud);

    @GET("OperacionesApp")
    Call<ReporteResponse> reporte(@Query("idOperacion") String operacion,
                                  @Query("fechaInicio") String fechaInicio,
                                  @Query("fechaFin") String fechaFin,
                                  @Query("firma") String firma);*/
}
