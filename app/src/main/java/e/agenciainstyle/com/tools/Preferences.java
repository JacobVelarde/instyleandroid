package e.agenciainstyle.com.tools;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class Preferences {

    private static final String PERSISTENCIA = "persistencia";
    public static final String NO_DISPONIBLE_STRING = "not_found";
    public static final String USER = "user";

    public static void saveDataString(Context context, String key, String dato){
        SharedPreferences settings = context.getSharedPreferences(PERSISTENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, dato);
        editor.apply();
    }

    public static String getDataStringWithKey(Context context, String key){
        SharedPreferences settings = context.getSharedPreferences(PERSISTENCIA, Context.MODE_PRIVATE);
        if (settings.contains(key)){
            return settings.getString(key, NO_DISPONIBLE_STRING);
        }else{
            return NO_DISPONIBLE_STRING;
        }
    }

    public static void saveObject(Context context, String key, Object object){
        String jsonObject = new Gson().toJson(object);
        SharedPreferences settings = context.getSharedPreferences(PERSISTENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, jsonObject);
        editor.apply();
    }

    public static Object getObjectWithKey(Context context, String key, Class<?> object){
        SharedPreferences settings = context.getSharedPreferences(PERSISTENCIA, Context.MODE_PRIVATE);
        if (settings.contains(key)){
            String jsonObject = settings.getString(key, null);
            if (jsonObject != null){
                return new Gson().fromJson(jsonObject, object);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

}
