package e.agenciainstyle.com.tools;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import e.agenciainstyle.com.R;

public class FirebaseMessaginService extends FirebaseMessagingService {

    private static final String NOTIFICATION_CHANNEL_ID = "testchannel";
    private static final String NOTIFICATION_CHANNEL_NAME = "inStyle Notifications";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int ID_NOTIFICATION_RANDOM = new Random().nextInt(1000 + 1); //Se asigna un id diferente para que no se reemplacen las notificaciones en el status bar
        if (notificationManager != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Notification.Builder nb = getChannel(notificationManager);
                nb.setContentTitle("Notificacion prueba");
                nb.setContentText("Misael enviame el payload de firebase");
                nb.setStyle(new Notification.BigTextStyle().bigText("Misael enviame el payload de firebase"));
                nb.setAutoCancel(true);
                //nb.setLargeIcon(largeIcon);
                nb.setSmallIcon(R.mipmap.notificacion);
                //nb.setContentIntent(pendingIntent);
                nb.setAutoCancel(true);
                notificationManager.notify(ID_NOTIFICATION_RANDOM, nb.build());
            } else {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
                notificationBuilder.setContentTitle("Notificacion prueba");
                notificationBuilder.setContentText("Misael enviame el payload de firebase");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("Misael enviame el payload de firebase"));
                notificationBuilder.setAutoCancel(true);
                //notificationBuilder.setLargeIcon(largeIcon);
                notificationBuilder.setSmallIcon(R.mipmap.notificacion);
                notificationBuilder.setAutoCancel(true);
                //notificationBuilder.setContentIntent(pendingIntent);
                notificationManager.notify(ID_NOTIFICATION_RANDOM, notificationBuilder.build());
            }
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

    }

    /**
     * A partir de Android Oreo se crean canales a través de las notificaciones, en este método se crea un canal con el nombre "Cinepolis Notifications" para
     * que el usuario pueda elegir si desea habilitarlo o no.
     *
     * @param notificationManager
     * @return
     */
    @TargetApi(Build.VERSION_CODES.O)
    private Notification.Builder getChannel(NotificationManager notificationManager) {
        Notification.Builder notificationBuilder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID);
        createNotificationChannel(notificationManager);
        notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
        return notificationBuilder;
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(NotificationManager notificationManager) {
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setDescription("Channel description");
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationChannel.enableVibration(true);
        notificationManager.createNotificationChannel(notificationChannel);
    }
}
