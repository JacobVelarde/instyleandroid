package e.agenciainstyle.com.tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Bandera;

public class CodigoBanderasAdapter extends ArrayAdapter<Bandera> {

    private Context context;
    private ArrayList<Bandera> banderaArrayList;

    public CodigoBanderasAdapter(Context context, ArrayList<Bandera> banderaArrayList){
        super(context, -1, banderaArrayList);

        this.context = context;
        this.banderaArrayList = banderaArrayList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_codigo_bandera, parent, false);

        ImageView imageView = rowView.findViewById(R.id.bandera);
        TextView textView = rowView.findViewById(R.id.prefijo);

        Bandera bandera = banderaArrayList.get(position);

        textView.setText(bandera.getPrefijo());

        return rowView;

    }
}
