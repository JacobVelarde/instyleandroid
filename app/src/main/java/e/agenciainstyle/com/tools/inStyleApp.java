package e.agenciainstyle.com.tools;

import android.app.Application;

public class inStyleApp extends Application {

    @Override
    public void onCreate() {
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/avenirnextregular.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
    }
}
