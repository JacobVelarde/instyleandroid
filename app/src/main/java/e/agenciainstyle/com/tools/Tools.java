package e.agenciainstyle.com.tools;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.model.response.LoginResponse;
import e.agenciainstyle.com.server.ServerConexion;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class Tools {

    public static boolean validImage(CircleImageView imageView){
        if (imageView != null){
            BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
            if (bitmapDrawable != null){
                Bitmap bitmap = bitmapDrawable.getBitmap();
                if (bitmap != null){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static Object parseErrorBody(ResponseBody json, Type type){

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.fromJson(json.charStream(), LoginResponse.class);

    }

    public static LoginResponse parseError(Response<?> response) {
        Converter<ResponseBody, LoginResponse> converter =
                ServerConexion.getConexion("","")
                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);

        LoginResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new LoginResponse();
        }

        return error;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static String formatNumber(double number, String pattern){
        DecimalFormat decimal = new DecimalFormat(pattern);
        //decimal.setRoundingMode(RoundingMode.CEILING);

        return decimal.format(number);
    }

    public interface ToolsToken{
        void token(String token);
    }

    public static void getTokenFirebase(ToolsToken toolsToken){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            toolsToken.token("ERROR");
                        }else{
                            toolsToken.token(task.getResult().getToken());
                        }
                    }
                });
    }
}
