package e.agenciainstyle.com.tools;

import android.app.Activity;
import android.content.Intent;

public class ActivityManager {

    public static void nextWithFinish(Activity activity, Class<?> nextActivity){
        activity.startActivity(new Intent().setClass(activity, nextActivity));
        activity.finish();
    }

    public static void next(Activity activity, Class<?> nextActivity){
        activity.startActivity(new Intent().setClass(activity, nextActivity));
    }

    public static void removeAll(Activity activity, Class<?> nextActivity){
        activity.startActivity(new Intent().setClass(activity, nextActivity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        activity.finish();

    }
}
