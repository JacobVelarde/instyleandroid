package e.agenciainstyle.com.tools;

public class Constantes {

    /*
    Codigo servidor
     */

    public static final int LEER_MENSAJE_SI_ES_NECESARIO = 200;
    public static final int LEER_OBJETO = 201;
    public static final int LEER_MENSAJE = 206;

    public static final int GO_DASHBOARD = 208;

    public static final String tipoCliente = "tipoCliente";
}
