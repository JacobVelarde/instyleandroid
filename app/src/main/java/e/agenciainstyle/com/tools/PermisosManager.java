package e.agenciainstyle.com.tools;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class PermisosManager implements EasyPermissions.PermissionCallbacks {

    private Activity activity;
    public final static int PERMISSION_LOCATION = 1;
    public final static int PERMISSION_CAMERA = 2;
    private PermisosManagerListener listener;

    private String[] permisosSelfie = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private PermisosManager(){ }

    public PermisosManager(Activity activity, PermisosManagerListener listener){
        this.activity = activity;
        this.listener = listener;
    }

    public interface PermisosManagerListener{
        void permisosSuccess(int permiso);
        void permisosCancel(int permiso);
    }

    public void solicitarPermisosLocalizacion(){
        EasyPermissions.requestPermissions(activity,
                "Necesitamos permisos para saber tu ubicación",
                PERMISSION_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public void solicitarPermisosSelfie(){
        EasyPermissions.requestPermissions(activity,
                "Necesitamos permisos para hacer uso de la camara",
                PERMISSION_CAMERA,
                permisosSelfie);
    }

    public void setPermisosRecibidos(int requestCode,
                                  @NonNull String[] permissions,
                                  @NonNull int[] grantResults){

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        listener.permisosSuccess(requestCode);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        listener.permisosCancel(requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    }

    public boolean tienePermisosLocalizacion(){
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        }else{
            return false;
        }
    }

    public boolean tienePermisosSelfie(){
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            return true;
        }else{
            return false;
        }
    }
}
