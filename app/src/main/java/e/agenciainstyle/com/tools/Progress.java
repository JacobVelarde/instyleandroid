package e.agenciainstyle.com.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import e.agenciainstyle.com.R;

public class Progress extends AlertDialog {

    private Activity activity;

    public Progress(Activity a) {
        super(a);
        this.activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_view);
        setInverseBackgroundForced(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
    }

    public void show(){
        super.show();
    }

    public void dismiss(){
        super.dismiss();
    }
}