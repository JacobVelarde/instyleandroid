package e.agenciainstyle.com.tools;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Bandera;

public class CodigosBanderasView extends AlertDialog {

    @BindView(R.id.lista_banderas)
    ListView listaBanderas;

    private CodigoBanderasAdapter codigoBanderasAdapter;
    private Context context;
    private ArrayList<Bandera> banderaArrayList;
    private SelectBandera selectBandera;

    public interface SelectBandera{
        void bandera(Bandera bandera);
    }

    public CodigosBanderasView(Context context, ArrayList<Bandera> banderaArrayList, SelectBandera selectBandera) {
        super(context);

        this.context = context;
        this.banderaArrayList = banderaArrayList;
        this.selectBandera = selectBandera;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_codigos_banderas);
        ButterKnife.bind(this);

        codigoBanderasAdapter = new CodigoBanderasAdapter(context, banderaArrayList);
        listaBanderas.setAdapter(codigoBanderasAdapter);

        listaBanderas.setOnItemClickListener((a, b, position, c) ->{
            Bandera bandera = banderaArrayList.get(position);
            selectBandera.bandera(bandera);
            this.dismiss();
        });

        if (!banderaArrayList.isEmpty()){
            if (getWindow() != null){
                int heigth = 500;
                if (banderaArrayList.size() == 2){
                    heigth = 400;
                }
                this.getWindow().setLayout(500,heigth);
            }
        }

    }
}
