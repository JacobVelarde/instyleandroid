package e.agenciainstyle.com.tools;

public enum Usuario{
    ADMIN("1"),
    CLIENTE("2"),
    TALENTO("3"),
    BOOKER("7"),
    PRODUCTOR("9"),
    COORDINADOR("4");

    private String tipoCliente;

    Usuario(String tipoCliente){
        this.tipoCliente = tipoCliente;
    }

    public String getTipoCliente(){
        return tipoCliente;
    }
}
