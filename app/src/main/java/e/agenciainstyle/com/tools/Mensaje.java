package e.agenciainstyle.com.tools;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;

public class Mensaje extends AlertDialog {

    @BindView(R.id.text_mensaje)
    TextView textMensaje;
    @BindView(R.id.imagen)
    ImageView imagen;
    @BindView(R.id.btn_aceptar)
    TextView btnAceptar;

    private String mensaje;
    private boolean iconVisible;
    private ButtonAceptar buttonAceptar;

    public interface ButtonAceptar{
        void onClick();
    }

    public Mensaje(Context context, String mensaje, boolean iconVisible) {
        super(context);
        this.mensaje = mensaje;
        this.iconVisible = iconVisible;
    }

    public void setInterface(ButtonAceptar buttonAceptar){
        this.buttonAceptar = buttonAceptar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_mensaje);
        ButterKnife.bind(this);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        textMensaje.setText(mensaje);

        if (iconVisible){
            imagen.setVisibility(View.VISIBLE);
        }else{
            imagen.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_aceptar)
    public void onViewClicked() {

        if (buttonAceptar != null){
            buttonAceptar.onClick();
        }

        this.dismiss();
    }
}
