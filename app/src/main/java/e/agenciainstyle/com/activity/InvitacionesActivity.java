package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Console;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterListaInvitaciones;
import e.agenciainstyle.com.model.Invitacion;

public class InvitacionesActivity extends AppCompatActivity {

    @BindView(R.id.btn_menu)
    TextView btnMenu;
    @BindView(R.id.btn_e)
    TextView btnE;
    @BindView(R.id.lista_invitaciones)
    RecyclerView listaInvitaciones;
    private AdapterListaInvitaciones adapterListaInvitaciones;
    private ArrayList<Invitacion> invitaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitaciones);
        ButterKnife.bind(this);

        initAdapter();

    }

    private void initAdapter() {
        invitaciones = new ArrayList<>();
        invitaciones.add(new Invitacion("1", "Invitación 1"));
        invitaciones.add(new Invitacion("2", "Invitación 2"));
        invitaciones.add(new Invitacion("3", "Invitación 3"));

        adapterListaInvitaciones = new AdapterListaInvitaciones(invitaciones, new AdapterListaInvitaciones.InvitacionImp() {
            @Override
            public void clickAceptar(Invitacion invitacion) {
                Log.v("Aceptar", invitacion.getId());
            }

            @Override
            public void clickCancelar(Invitacion invitacion) {
                Log.v("Cancelar", invitacion.getId());
            }
        });

        //adapterListaInvitaciones.notifyDataSetChanged();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listaInvitaciones.setLayoutManager(layoutManager);
        listaInvitaciones.setAdapter(adapterListaInvitaciones);
    }

    @OnClick({R.id.btn_menu, R.id.btn_e})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_menu:
                onBackPressed();
                break;
            case R.id.btn_e:
                onBackPressed();
                break;
        }
    }
}
