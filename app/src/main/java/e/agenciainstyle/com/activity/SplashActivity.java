package e.agenciainstyle.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.StyleAnimation;
import e.agenciainstyle.com.tools.Usuario;

public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.texto)
    TextView texto;
    @BindView(R.id.btnAcceder)
    TextView btnAcceder;
    @BindView(R.id.btnCliente)
    TextView btnCliente;
    @BindView(R.id.btnTalento)
    TextView btnTalento;
    @BindView(R.id.layout_opciones)
    LinearLayout layoutOpciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnAcceder, R.id.btnCliente, R.id.btnTalento})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnAcceder:

                //StyleAnimation.fadeOut(this, btnAcceder);
                //StyleAnimation.fadeIn(this, layoutOpciones);
                ActivityManager.next(this, LoginActivity.class);

                break;
            case R.id.btnCliente:

                Intent intentCliente = new Intent(this, IniciarUneteActivity.class);
                intentCliente.putExtra("tipoCliente", Usuario.CLIENTE);
                startActivity(intentCliente);

                break;
            case R.id.btnTalento:
                Intent intentTalento = new Intent(this, IniciarUneteActivity.class);
                intentTalento.putExtra("tipoCliente", Usuario.TALENTO);
                startActivity(intentTalento);
                break;
        }
    }
}
