package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterCarousel;
import e.agenciainstyle.com.model.AreaInteres;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Usuario;

public class AreasDeInteresActivity extends AppCompatActivity {

    @BindView(R.id.carousel_view)
    RecyclerView carouselView;
    private ArrayList<AreaInteres> areaInteres;
    private AdapterCarousel adapterCarousel;
    private LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_areas_de_interes);
        ButterKnife.bind(this);

        areaInteres = new ArrayList<>();
        areaInteres.add(new AreaInteres(R.mipmap.foto_mm, "MODEL MANAGEMENT"));
        areaInteres.add(new AreaInteres(R.mipmap.foto_mie, "MODELOS, IMAGEN & EDECAN<font color='#22A8B7'>E</font>S"));
        areaInteres.add(new AreaInteres(R.mipmap.foto_at, "ACTORES & TALENTOS"));

        adapterCarousel = new AdapterCarousel(areaInteres, new AdapterCarousel.CarouselClickItem() {
            @Override
            public void carouselClick(AreaInteres areaInteres) {

                switch (areaInteres.getImage()){
                    case R.mipmap.foto_mm:
                        Toast.makeText(AreasDeInteresActivity.this, "En construcción",Toast.LENGTH_LONG).show();

                        break;

                    case R.mipmap.foto_mie:

                        User user = (User) Preferences.getObjectWithKey(AreasDeInteresActivity.this,Preferences.USER, User.class);
                        if (user != null){
                            if (user.getPerfil().compareTo(Usuario.ADMIN.getTipoCliente()) == 0){

                                ActivityManager.next(AreasDeInteresActivity.this, MenuPrincipalActivity.class);

                            }else if (user.getPerfil().compareTo(Usuario.CLIENTE.getTipoCliente()) == 0){

                                ActivityManager.next(AreasDeInteresActivity.this, MenuPrincipalActivity.class);

                            }else if (user.getPerfil().compareTo(Usuario.TALENTO.getTipoCliente()) == 0){
                                if (user.getPrimeraVez().compareTo("1") == 0){  //ES PRIMERA VEZ
                                    ActivityManager.next(AreasDeInteresActivity.this, FormularioUneteEdecanActivity.class);
                                }else{
                                    ActivityManager.next(AreasDeInteresActivity.this, MenuPrincipalActivity.class);
                                }
                            }else if (user.getPerfil().compareTo(Usuario.BOOKER.getTipoCliente()) == 0){

                            }else if (user.getPerfil().compareTo(Usuario.COORDINADOR.getTipoCliente()) == 0){

                                ActivityManager.next(AreasDeInteresActivity.this, MenuPrincipalActivity.class);
                            }
                        }

                        break;

                    case R.mipmap.foto_at:
                        Toast.makeText(AreasDeInteresActivity.this, "En construcción",Toast.LENGTH_LONG).show();

                        break;
                }

            }
        });

        layoutManager = new LinearLayoutManager(this);
        carouselView.setLayoutManager(layoutManager);
        carouselView.setHasFixedSize(true);
        carouselView.setAdapter(adapterCarousel);
        carouselView.scrollToPosition(500);

    }

    /*private void initRecyclerView(final RecyclerView recyclerView, final CarouselLayoutManager layoutManager, final AdapterCarousel adapter) {
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        layoutManager.setMaxVisibleItems(1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new CenterScrollListener());
        DefaultChildSelectionListener.initCenterItemListener(new DefaultChildSelectionListener.OnCenterItemClickListener() {
            @Override
            public void onCenterItemClicked(@NonNull final RecyclerView recyclerView, @NonNull final CarouselLayoutManager carouselLayoutManager, @NonNull final View v) {
                final int position = recyclerView.getChildLayoutPosition(v);
                final String msg = String.format(Locale.US, "Item %1$d was clicked", position);
                Toast.makeText(AreasDeInteresActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        }, recyclerView, layoutManager);

        layoutManager.addOnItemSelectionListener(new CarouselLayoutManager.OnCenterItemSelectionListener() {

            @Override
            public void onCenterItemChanged(final int adapterPosition) {
                if (CarouselLayoutManager.INVALID_POSITION != adapterPosition) {
                    //final int value = adapter.mPosition[adapterPosition];
/*
                    adapter.mPosition[adapterPosition] = (value % 10) + (value / 10 + 1) * 10;
                    adapter.notifyItemChanged(adapterPosition);

                }
            }
        });
    }*/
}
