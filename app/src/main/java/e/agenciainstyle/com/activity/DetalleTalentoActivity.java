package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Multimedia;
import e.agenciainstyle.com.model.TalentoDetalle;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.DetalleTalentoRequest;
import e.agenciainstyle.com.presenter.DetalleEventoPresenter;
import e.agenciainstyle.com.presenter.DetalleTalentoPresenter;
import e.agenciainstyle.com.presenter.FiltroTalentosPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Tools;

public class DetalleTalentoActivity extends AppCompatActivity {

    String idPerfil;
    @BindView(R.id.btn_mujer)
    TextView btnMujer;
    @BindView(R.id.btn_hombre)
    TextView btnHombre;
    @BindView(R.id.text_edad)
    TextView textEdad;
    @BindView(R.id.seek_bar_edad)
    SeekBar seekBarEdad;
    @BindView(R.id.layout_edad)
    LinearLayout layoutEdad;
    @BindView(R.id.text_altura)
    TextView textAltura;
    @BindView(R.id.text_altura_ingles)
    TextView textAlturaIngles;
    @BindView(R.id.seek_bar_altura)
    SeekBar seekBarAltura;
    @BindView(R.id.layout_altura)
    LinearLayout layoutAltura;
    @BindView(R.id.text_camisa)
    TextView textCamisa;
    @BindView(R.id.text_camisa_ingles)
    TextView textCamisaIngles;
    @BindView(R.id.seek_bar_camisa)
    SeekBar seekBarCamisa;
    @BindView(R.id.layout_camisa)
    LinearLayout layoutCamisa;
    @BindView(R.id.text_saco)
    TextView textSaco;
    @BindView(R.id.text_saco_ingles)
    TextView textSacoIngles;
    @BindView(R.id.seek_bar_saco)
    SeekBar seekBarSaco;
    @BindView(R.id.layout_saco)
    LinearLayout layoutSaco;
    @BindView(R.id.text_pantalon)
    TextView textPantalon;
    @BindView(R.id.text_pantalon_ingles)
    TextView textPantalonIngles;
    @BindView(R.id.seek_bar_pantalon)
    SeekBar seekBarPantalon;
    @BindView(R.id.layout_pantalon)
    LinearLayout layoutPantalon;
    @BindView(R.id.text_busto)
    TextView textBusto;
    @BindView(R.id.text_busto_ingles)
    TextView textBustoIngles;
    @BindView(R.id.seek_bar_busto)
    SeekBar seekBarBusto;
    @BindView(R.id.layout_busto)
    LinearLayout layoutBusto;
    @BindView(R.id.text_cintura)
    TextView textCintura;
    @BindView(R.id.text_cintura_ingles)
    TextView textCinturaIngles;
    @BindView(R.id.seek_bar_cintura)
    SeekBar seekBarCintura;
    @BindView(R.id.layout_cintura)
    LinearLayout layoutCintura;
    @BindView(R.id.text_cadera)
    TextView textCadera;
    @BindView(R.id.text_cadera_ingles)
    TextView textCaderaIngles;
    @BindView(R.id.seek_bar_cadera)
    SeekBar seekBarCadera;
    @BindView(R.id.layout_cadera)
    LinearLayout layoutCadera;
    @BindView(R.id.text_peso)
    TextView textPeso;
    @BindView(R.id.text_peso_ingles)
    TextView textPesoIngles;
    @BindView(R.id.seek_bar_peso)
    SeekBar seekBarPeso;
    @BindView(R.id.layout_peso)
    LinearLayout layoutPeso;
    @BindView(R.id.text_calzado)
    TextView textCalzado;
    @BindView(R.id.text_calzado_ingles)
    TextView textCalzadoIngles;
    @BindView(R.id.text_calzado_min_limite_ingles)
    TextView textCalzadoMinLimiteIngles;
    @BindView(R.id.text_calzado_max_limite_ingles)
    TextView textCalzadoMaxLimiteIngles;
    @BindView(R.id.seek_bar_calzado)
    SeekBar seekBarCalzado;
    @BindView(R.id.text_calzado_min_limite)
    TextView textCalzadoMinLimite;
    @BindView(R.id.text_calzado_max_limite)
    TextView textCalzadoMaxLimite;
    @BindView(R.id.layout_calzado)
    LinearLayout layoutCalzado;
    @BindView(R.id.spinner_ojos)
    Spinner spinnerOjos;
    @BindView(R.id.spinner_cabello)
    Spinner spinnerCabello;
    @BindView(R.id.btn_modelo)
    TextView btnModelo;
    @BindView(R.id.btn_edecan)
    TextView btnEdecan;
    @BindView(R.id.btn_actriz)
    TextView btnActriz;
    @BindView(R.id.btn_imagen)
    TextView btnImagen;
    @BindView(R.id.btn_promotora)
    TextView btnPromotora;
    @BindView(R.id.btn_animadora)
    TextView btnAnimadora;
    @BindView(R.id.btn_conductora)
    TextView btnConductora;
    @BindView(R.id.btn_coordinadora)
    TextView btnCoordinadora;
    @BindView(R.id.btn_bailarina)
    TextView btnBailarina;
    @BindView(R.id.btn_influencer)
    TextView btnInfluencer;
    @BindView(R.id.layout_areas_de_despecialidad)
    LinearLayout layoutAreasDeDespecialidad;
    @BindView(R.id.image_foto_selfie_uno)
    CircleImageView imageFotoSelfieUno;
    @BindView(R.id.image_foto_selfie_dos)
    CircleImageView imageFotoSelfieDos;
    @BindView(R.id.image_foto_selfie_tres)
    CircleImageView imageFotoSelfieTres;
    @BindView(R.id.layout_selfies)
    LinearLayout layoutSelfies;
    @BindView(R.id.image_foto_book_uno)
    CircleImageView imageFotoBookUno;
    @BindView(R.id.image_foto_book_dos)
    CircleImageView imageFotoBookDos;
    @BindView(R.id.image_foto_book_tres)
    CircleImageView imageFotoBookTres;
    @BindView(R.id.image_foto_book_cuatro)
    CircleImageView imageFotoBookCuatro;
    @BindView(R.id.image_foto_book_cinco)
    CircleImageView imageFotoBookCinco;
    @BindView(R.id.image_foto_book_seis)
    CircleImageView imageFotoBookSeis;
    @BindView(R.id.image_foto_polaroid_uno)
    CircleImageView imageFotoPolaroidUno;
    @BindView(R.id.image_foto_polaroid_dos)
    CircleImageView imageFotoPolaroidDos;
    @BindView(R.id.image_foto_polaroid_tres)
    CircleImageView imageFotoPolaroidTres;
    @BindView(R.id.image_foto_polaroid_cuatro)
    CircleImageView imageFotoPolaroidCuatro;
    @BindView(R.id.image_foto_polaroid_cinco)
    CircleImageView imageFotoPolaroidCinco;
    @BindView(R.id.image_foto_polaroid_seis)
    CircleImageView imageFotoPolaroidSeis;
    @BindView(R.id.image_video_demo_reel)
    CircleImageView imageVideoDemoReel;
    @BindView(R.id.btn_adjuntar_seguro)
    TextView btnAdjuntarSeguro;
    @BindView(R.id.btn_adjuntar_ine)
    TextView btnAdjuntarIne;
    @BindView(R.id.btn_adjuntar_seguro_extranjero)
    TextView btnAdjuntarSeguroExtranjero;
    @BindView(R.id.btn_adjuntar_ine_extranjero)
    TextView btnAdjuntarIneExtranjero;

    private DetalleTalentoPresenter detalleTalentoPresenter;
    private Progress progress;
    private boolean statusMasculino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_talento);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            idPerfil = bundle.getString("id");
        }

        progress = new Progress(this);
        initSeekBar();
        initPresenter();
    }

    private void initPresenter(){
        detalleTalentoPresenter = new DetalleTalentoPresenter(this, new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                DetalleTalentoRequest detalleTalentoRequest = (DetalleTalentoRequest) response;
                TalentoDetalle detalle = detalleTalentoRequest.getObjeto();
                statusMasculino = detalle.getGenero().compareTo("Masculino") == 0;

                setSeekBarProgressInt(seekBarEdad, detalle.getEdad(), 18);
                setSeekBarProgressFloat(seekBarAltura, detalle.getAltura_mts(), 1.6f);
                setSeekBarProgressFloat(seekBarCamisa, detalle.getH_camisa(), 0f);
                setSeekBarProgressFloat(seekBarSaco, detalle.getH_saco(), 0f);
                setSeekBarProgressFloat(seekBarPantalon, detalle.getH_pantalon(), 0f);
                setSeekBarProgressFloat(seekBarBusto, detalle.getM_pecho(), 0f);
                setSeekBarProgressFloat(seekBarCintura, detalle.getM_cintura(), 0f);
                setSeekBarProgressFloat(seekBarCadera, detalle.getM_cadera(), 0f);
                setSeekBarProgressInt(seekBarPeso, detalle.getPeso_kg(), 45);

                if (statusMasculino){
                    showFormularioHombre();
                    setSeekBarProgressFloat(seekBarCalzado, detalle.getCalzado(),22);
                }else{
                    showFormularioMujer();
                    setSeekBarProgressFloat(seekBarCalzado, detalle.getCalzado(), 22);
                }

                switch (detalle.getCabello()){
                    case "cualquiera":
                        spinnerCabello.setSelection(0);
                        break;

                    case "negro":
                        spinnerCabello.setSelection(1);
                        break;

                    case "castaño":
                    case "castano":
                        spinnerCabello.setSelection(2);
                        break;

                    case "rubio":
                        spinnerCabello.setSelection(3);
                        break;

                    case "pelirrojo":
                        spinnerCabello.setSelection(4);
                        break;
                }

                switch (detalle.getOjos()){
                    case "selecciona":
                        spinnerOjos.setSelection(0);
                        break;

                    case "negro":
                        spinnerOjos.setSelection(1);
                        break;

                    case "café":
                    case "cafe":
                        spinnerOjos.setSelection(2);
                        break;

                    case "verde":
                        spinnerOjos.setSelection(3);
                        break;

                    case "azul":
                        spinnerOjos.setSelection(4);
                        break;

                    case "gris":
                        spinnerOjos.setSelection(5);
                        break;
                }


                for (Multimedia multimedia : detalle.getMultimedia()){
                    switch (multimedia.getCategoria()){
                        case "selfie_perfil":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoSelfieDos);
                            break;

                        case "selfie_rostro":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoSelfieUno);
                            break;

                        case "selfie_ccompleto":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoSelfieTres);
                            break;

                        case "book_1":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoBookUno);
                            break;

                        case "book_2":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoBookDos);
                            break;

                        case "book_3":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoBookTres);
                            break;

                        case "book_4":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoBookCuatro);
                            break;

                        case "book_5":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoBookCinco);
                            break;

                        case "book_6":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoBookSeis);
                            break;

                        case "pola_rostro":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoPolaroidUno);
                            break;

                        case "pola_frente":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoPolaroidDos);
                            break;

                        case "pola_espalda":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoPolaroidTres);
                            break;

                        case "pola_izquierda":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoPolaroidCuatro);
                            break;

                        case "pola_derecha":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoPolaroidCinco);
                            break;

                        case "pola_34":
                            Glide.with(imageFotoSelfieDos.getRootView())
                                    .load(multimedia.getUrl())
                                    .into(imageFotoPolaroidSeis);
                            break;

                        case "doc_seguro":
                            break;

                        case "doc_permisoine":
                            break;

                        case "video_demo":
                            break;

                        case "video_casting":
                            break;
                    }
                }

            }



            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                new Mensaje(DetalleTalentoActivity.this, error, false).show();
            }
        });

        progress.show();
        User user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        detalleTalentoPresenter.getDetalleTalento(idPerfil, user);

    }

    /**
     *
     * @param seekBar
     * @param progreso
     * @param min valor minimo del seekbar
     */
    private void setSeekBarProgressInt(SeekBar seekBar, String progreso, int min){
        if (progreso != null){
            if (progreso.compareTo("") != 0){
                int progress = Integer.parseInt(progreso.trim()) - min;
                seekBar.post(()-> seekBar.setProgress(progress));
            }
        }
    }

    private void setSeekBarProgressFloat(SeekBar seekBar, String progreso, float min){

        if (progreso != null){
            if (progreso.compareTo("") != 0) {
                float decimales = (Float.parseFloat(progreso.trim()) * 10);
                float resultado = (decimales - (min*10));
                int progress = (int) (resultado * 10);

                seekBar.post(() -> seekBar.setProgress(progress));
            }
        }
    }

    private void initSeekBar() {

        ArrayList<String> opcionesOjos = new ArrayList<>();
        opcionesOjos.add("Selecciona");
        opcionesOjos.add("Negro");
        opcionesOjos.add("Café");
        opcionesOjos.add("Verde");
        opcionesOjos.add("Azul");
        opcionesOjos.add("Gris");

        ArrayList<String> opcionesCabello = new ArrayList<>();
        opcionesCabello.add("Cualquiera");
        opcionesCabello.add("Negro");
        opcionesCabello.add("Castaño");
        opcionesCabello.add("Rubio");
        opcionesCabello.add("Pelirrojo");

        ArrayAdapter<String> adapterOjos = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, opcionesOjos);
        ArrayAdapter<String> adapterCabello = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, opcionesCabello);

        spinnerCabello.setAdapter(adapterCabello);
        spinnerOjos.setAdapter(adapterOjos);


        //TODO Para calcular el SeekBar
        //TODO https://stackoverflow.com/questions/20762001/how-to-set-seekbar-min-and-max-value

        int stepEdad = 1;
        int maxEdad = 35;
        int minEdad = 18;

        seekBarEdad.setMax((maxEdad - minEdad) / stepEdad);

        seekBarEdad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int value = minEdad + (progress * stepEdad);
                textEdad.setText("EDAD: " + value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepAltura = 0.01f;
        float maxAltura = 2;
        float minAltura = 1.6f;

        seekBarAltura.setMax(Math.round((maxAltura - minAltura) / stepAltura));

        seekBarAltura.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minAltura + (progress * stepAltura);
                textAltura.setText("ALTURA: " + Tools.formatNumber(value, "#.##") + " m");
                textAlturaIngles.setText(Tools.formatNumber(value * 3.28084, "#.##") + " ft");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepCamisa = 0.01f;
        float maxCamisa = 140;
        float minCamisa = 0;

        seekBarCamisa.setMax(Math.round((maxCamisa - minCamisa) / stepCamisa));

        seekBarCamisa.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCamisa + (progress * stepCamisa);
                textCamisa.setText("CAMISA: " + Tools.formatNumber(value, "#.##") + " cm");
                textCamisaIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepSaco = 0.01f;
        float maxSaco = 140;
        float minSaco = 0;

        seekBarSaco.setMax(Math.round((maxSaco - minSaco) / stepSaco));

        seekBarSaco.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCamisa + (progress * stepCamisa);
                textSaco.setText("SACO: " + Tools.formatNumber(value, "#.##" + " cm"));
                textSacoIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepPantalon = 0.01f;
        float maxPantalon = 140;
        float minPantalon = 0;

        seekBarPantalon.setMax(Math.round((maxPantalon - minPantalon) / stepPantalon));

        seekBarPantalon.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minPantalon + (progress * stepPantalon);
                textPantalon.setText("PANTALÓN: " + Tools.formatNumber(value, "#.##" + " cm"));
                textPantalonIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepBusto = 0.01f;
        float maxBusto = 140;
        float minBusto = 0;

        seekBarBusto.setMax(Math.round((maxBusto - minBusto) / stepBusto));

        seekBarBusto.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minPantalon + (progress * stepPantalon);
                textBusto.setText("BUSTO: " + Tools.formatNumber(value, "#.##" + " cm"));
                textBustoIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepCintura = 0.01f;
        float maxCintura = 140;
        float minCintura = 0;

        seekBarCintura.setMax(Math.round((maxCintura - minCintura) / stepCintura));

        seekBarCintura.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCintura + (progress * stepCintura);
                textCintura.setText("CINTURA: " + Tools.formatNumber(value, "#.##" + " cm"));
                textCinturaIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepCadera = 0.01f;
        float maxCadera = 140;
        float minCadera = 0;

        seekBarCadera.setMax(Math.round((maxCadera - minCadera) / stepCadera));

        seekBarCadera.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCadera + (progress * stepCadera);
                textCadera.setText("CADERA: " + Tools.formatNumber(value, "#.##" + " cm"));
                textCaderaIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepPeso = 1;
        float maxPeso = 110;
        float minPeso = 45;

        seekBarPeso.setMax(Math.round((maxPeso - minPeso) / stepPeso));

        seekBarPeso.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minPeso + (progress * stepPeso);
                textPeso.setText("PESO: " + Tools.formatNumber(value, "#.##" + " kg"));
                textPesoIngles.setText(Tools.formatNumber(value * 2.20462, "#.##") + " lb");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        initSeekBarCalzado(statusMasculino);

    }

    private void initSeekBarCalzado(boolean isHombre) {

        float stepCalzado = 0.5f;
        float maxCalzado = 0f;
        float minCalzado = 0f;

        if (isHombre) {

            maxCalzado = 31;
            minCalzado = 22;

            textCalzadoMinLimite.setText("22 cm");
            textCalzadoMaxLimite.setText("31 cm");

            textCalzadoMinLimiteIngles.setText("4 in");
            textCalzadoMaxLimiteIngles.setText("13 in");
        } else {

            maxCalzado = 27;
            minCalzado = 22;

            textCalzadoMinLimite.setText("22 cm");
            textCalzadoMaxLimite.setText("27 cm");

            textCalzadoMinLimiteIngles.setText("5 in");
            textCalzadoMaxLimiteIngles.setText("10 in");
        }


        seekBarCalzado.setMax(Math.round((maxCalzado - minCalzado) / stepCalzado));

        float finalMinCalzado = minCalzado;
        seekBarCalzado.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = finalMinCalzado + (progress * stepCalzado);
                textCalzado.setText("CALZADO: " + value + " cm");
                if (isHombre) {
                    textCalzadoIngles.setText(value - 18 + " in");
                } else {
                    textCalzadoIngles.setText(value - 17 + " in");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void showFormularioMujer(){
        btnMujer.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
        btnMujer.setTextColor(getResources().getColor(R.color.colorBlanco));

        btnHombre.setBackground(getResources().getDrawable(R.drawable.round_color_green));
        btnHombre.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

        statusMasculino = false;
        showFormularioSexo();
        initSeekBarCalzado(statusMasculino);
    }

    private void showFormularioHombre(){
        btnMujer.setBackground(getResources().getDrawable(R.drawable.round_color_green));
        btnMujer.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

        btnHombre.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
        btnHombre.setTextColor(getResources().getColor(R.color.colorBlanco));

        statusMasculino = true;
        showFormularioSexo();
        initSeekBarCalzado(statusMasculino);
    }

    private void showFormularioSexo() {
        if (statusMasculino) {
            layoutCamisa.setVisibility(View.VISIBLE);
            layoutSaco.setVisibility(View.VISIBLE);
            layoutPantalon.setVisibility(View.VISIBLE);

            layoutBusto.setVisibility(View.GONE);
            layoutCintura.setVisibility(View.GONE);
            layoutCadera.setVisibility(View.GONE);

            btnEdecan.setText("GIO");
            btnActriz.setText("ACTOR");
            btnPromotora.setText("PROMOTOR");
            btnAnimadora.setText("ANIMADOR");
            btnConductora.setText("CONDUCTOR");
            btnCoordinadora.setText("COORDINADOR");
            btnBailarina.setText("BAILARIN");

        } else {
            layoutCamisa.setVisibility(View.GONE);
            layoutSaco.setVisibility(View.GONE);
            layoutPantalon.setVisibility(View.GONE);

            layoutBusto.setVisibility(View.VISIBLE);
            layoutCintura.setVisibility(View.VISIBLE);
            layoutCadera.setVisibility(View.VISIBLE);

            btnEdecan.setText("EDECAN");
            btnActriz.setText("ACRTÍZ");
            btnPromotora.setText("PROMOTORA");
            btnAnimadora.setText("ANIMADORA");
            btnConductora.setText("CONDUCTORA");
            btnCoordinadora.setText("COORDINADORA");
            btnBailarina.setText("BAILARINA");
        }
    }
}
