package e.agenciainstyle.com.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Talento;
import e.agenciainstyle.com.model.response.FotosUneteTalentoResponse;
import e.agenciainstyle.com.presenter.TalentoPresenter;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.ImageFilePath;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.PermisosManager;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Tools;

public class FormularioUneteTalentoActivity extends AppCompatActivity implements PermisosManager.PermisosManagerListener {

    @BindView(R.id.btn_mujer)
    TextView btnMujer;
    @BindView(R.id.btn_hombre)
    TextView btnHombre;
    @BindView(R.id.text_nombres)
    EditText textNombres;
    @BindView(R.id.text_apellidos)
    EditText textApellidos;
    @BindView(R.id.text_email)
    EditText textEmail;
    @BindView(R.id.text_confirmar_email)
    EditText textConfirmarEmail;
    @BindView(R.id.text_contrasenia)
    EditText textContrasenia;
    @BindView(R.id.text_mes)
    EditText textMes;
    @BindView(R.id.text_dia)
    EditText textDia;
    @BindView(R.id.text_anio)
    EditText textAnio;
    @BindView(R.id.text_url_instagram)
    EditText textInstagram;
    @BindView(R.id.image_foto_book_uno)
    CircleImageView imageFotoBookUno;
    @BindView(R.id.image_foto_book_dos)
    CircleImageView imageFotoBookDos;
    @BindView(R.id.image_foto_book_tres)
    CircleImageView imageFotoBookTres;
    @BindView(R.id.image_foto_book_cuatro)
    CircleImageView imageFotoBookCuatro;
    @BindView(R.id.image_foto_book_cinco)
    CircleImageView imageFotoBookCinco;
    @BindView(R.id.image_foto_selfie)
    CircleImageView imageFotoSelfie;
    @BindView(R.id.check_terminos_condiciones)
    CheckBox checkTerminosCondiciones;
    @BindView(R.id.btn_enviar_para_aprobacion)
    TextView btnEnviarParaAprobacion;

    private Progress progress;
    private TalentoPresenter talentoPresenter;
    private PermisosManager permisosManager;
    private ImageView imageViewSelected;
    private Uri imageUri;
    private boolean isChangeFotoBookUno = false, isChangeFotoBookDos = false,
            isChangeFotoBookTres = false, isChangeFotoBookCuatro = false, isChangeFotoBookCinco = false,
            isChangeFotoSelfie = false;

    private boolean generoSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_unete_talento);
        ButterKnife.bind(this);

        permisosManager = new PermisosManager(this, this);
        initTalentoPresenter();
        progress = new Progress(this);

        onChanceTextView(textNombres);
        onChanceTextView(textApellidos);
        onChanceTextView(textEmail);
        onChanceTextView(textConfirmarEmail);
        onChanceTextView(textContrasenia);
        onChanceTextView(textMes);
        onChanceTextView(textDia);
        onChanceTextView(textAnio);

        checkTerminosCondiciones.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String formulario = validaFormulario();
                if (formulario.equals("OK")){
                    btnEnviarParaAprobacion.setEnabled(true);
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorBlanco));
                }else{
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorGrisAccent));
                }
            }
        });
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), inImage, UUID.randomUUID().toString() + ".png", "drawing");
        return Uri.parse(path);
    }

    public Uri getImageUriCompress(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), inImage, UUID.randomUUID().toString() + ".png", "drawing");
        return Uri.parse(path);
    }

    private void initTalentoPresenter() {
        talentoPresenter = new TalentoPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                if (request == TalentoPresenter.REGISTRO_TALENTO){
                    FotosUneteTalentoResponse fotosUneteTalentoResponse = (FotosUneteTalentoResponse) response;

                    uploadFotos(fotosUneteTalentoResponse.getObjeto().getIdSolicitud()+"");

                }else if(request == TalentoPresenter.UPLOAD_FOTOS){
                    GenericResponse genericResponse = (GenericResponse) response;
                    Mensaje mensaje = new Mensaje(
                            FormularioUneteTalentoActivity.this,
                            genericResponse.getMensaje(),
                            false);
                    mensaje.setInterface(new Mensaje.ButtonAceptar() {
                        @Override
                        public void onClick() {
                            ActivityManager.removeAll(FormularioUneteTalentoActivity.this, SplashActivity.class);
                        }
                    });
                    mensaje.show();
                }
            }

            @Override
            public void serverSuccess(int request) {

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();

                Mensaje mensaje = new Mensaje(
                        FormularioUneteTalentoActivity.this,
                        error,
                        false);
                mensaje.show();

            }
        });

    }

    private void uploadFotos(String id) {
        Bitmap selfieBitmap =((BitmapDrawable)imageFotoSelfie.getDrawable()).getBitmap();
        Bitmap book1Bitmap =((BitmapDrawable)imageFotoBookUno.getDrawable()).getBitmap();
        Bitmap book2Bitmap =((BitmapDrawable)imageFotoBookDos.getDrawable()).getBitmap();
        Bitmap book3Bitmap =((BitmapDrawable)imageFotoBookTres.getDrawable()).getBitmap();
        Bitmap book4Bitmap =((BitmapDrawable)imageFotoBookCuatro.getDrawable()).getBitmap();
        Bitmap book5Bitmap =((BitmapDrawable)imageFotoBookCinco.getDrawable()).getBitmap();


        talentoPresenter.uploadFotos(id,
                ImageFilePath.getPath(this, getImageUri(selfieBitmap)),
                ImageFilePath.getPath(this, getImageUri(book1Bitmap)),
                ImageFilePath.getPath(this, getImageUri(book2Bitmap)),
                ImageFilePath.getPath(this, getImageUri(book3Bitmap)),
                ImageFilePath.getPath(this, getImageUri(book4Bitmap)),
                ImageFilePath.getPath(this, getImageUri(book5Bitmap)));
    }

    public void onChanceTextView(EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String formulario = validaFormulario();
                if (formulario.equals("OK")){
                    btnEnviarParaAprobacion.setEnabled(true);
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorBlanco));
                }else{
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorGrisAccent));
                }
            }
        });
    }


    @OnClick({R.id.btn_mujer, R.id.btn_hombre, R.id.image_foto_book_uno, R.id.image_foto_book_dos, R.id.image_foto_book_tres, R.id.image_foto_book_cuatro, R.id.image_foto_book_cinco, R.id.image_foto_selfie, R.id.btn_enviar_para_aprobacion})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_mujer:
                btnMujer.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnMujer.setTextColor(getResources().getColor(R.color.colorBlanco));

                btnHombre.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnHombre.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                generoSelected = true;
                break;
            case R.id.btn_hombre:
                btnMujer.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnMujer.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                btnHombre.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnHombre.setTextColor(getResources().getColor(R.color.colorBlanco));

                generoSelected = false;
                break;
            case R.id.image_foto_book_uno:
                openSelfie(imageFotoBookUno);
                break;
            case R.id.image_foto_book_dos:
                openSelfie(imageFotoBookDos);
                break;
            case R.id.image_foto_book_tres:
                openSelfie(imageFotoBookTres);
                break;
            case R.id.image_foto_book_cuatro:
                openSelfie(imageFotoBookCuatro);
                break;
            case R.id.image_foto_book_cinco:
                openSelfie(imageFotoBookCinco);
                break;
            case R.id.image_foto_selfie:
                openSelfie(imageFotoSelfie);
                break;
            case R.id.btn_enviar_para_aprobacion:

                String respuesta = validaFormulario();

                if(respuesta.compareTo("OK") == 0){
                    progress.show();

                    Talento talento = new Talento();
                    talento.setTipoSolicitud("2");
                    talento.setNombres(textNombres.getText().toString());
                    talento.setApellidos(textApellidos.getText().toString());
                    talento.setEmail(textEmail.getText().toString());
                    talento.setPassword(textContrasenia.getText().toString());
                    talento.setFechaNacimiento(textAnio.getText() + "-" + textMes.getText() + "-" + textDia.getText());
                    talento.setGenero(generoSelected ? "Masculino" : "Femenino");
                    talento.setUrlInstagram(textInstagram.getText().toString());
                    talentoPresenter.registrar(talento);
                }else{
                    Mensaje mensaje = new Mensaje(this, respuesta, false);
                    mensaje.show();
                }

                break;
        }
    }

    private void openSelfie(ImageView view){
        if (permisosManager.tienePermisosSelfie()){

            int IMAGE_CAPTURE_CODE = 1001;
            imageViewSelected = view;

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            //cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);

        }else{
            permisosManager.solicitarPermisosSelfie();
        }
    }

    private String validaFormulario(){
        if (textNombres.getText().toString().length() <= 0){
            return "Ingresa un nombre";
        }else if (textApellidos.getText().toString().length() <= 0){
            return "Ingresa un apellido";
        }else if (textEmail.getText().toString().length() <= 0){
            return "Ingresa un email";
        }else if (textConfirmarEmail.getText().toString().length() <= 0){
            return "Ingresa la confirmación del email";
        }else if (textContrasenia.getText().toString().length() <= 0){
            return "Ingresa la contraseña";
        }else if (textMes.getText().toString().length() <= 0){
            return "Ingresa el mes de nacimiento";
        }else if (textDia.getText().toString().length() <= 0){
            return "Ingresa el dia de nacimiento";
        }else if (textAnio.getText().toString().length() <= 0){
            return "Ingresa el año de nacimiento";
        }else if(textInstagram.getText().toString().length() <= 0){
            return "Ingresa tu perfil de Instagram";
        } else if (!isChangeFotoBookUno){
            return "Agrega las fotos de Book";
        }else if (!isChangeFotoBookDos){
            return "Agrega las fotos de Book";
        }else if (!isChangeFotoBookTres){
            return "Agrega las fotos de Book";
        }else if (!isChangeFotoBookCuatro){
            return "Agrega las fotos de Book";
        }else if (!isChangeFotoBookCinco){
            return "Agrega las fotos de Book";
        }else if (!isChangeFotoSelfie) {
            return "Agrega la foto de selfie";
        }else if (!checkTerminosCondiciones.isChecked()){
            return "Acepta los terminos y condiciones";
        }else if (!Tools.isValidEmail(textEmail.getText().toString())){
            return "Ingresa un email valido";
        }else if (textEmail.getText().toString().compareTo(textConfirmarEmail.getText().toString()) != 0){
            return "Ingresa un email que conincida con la confirmación";
        }else{
            return "OK";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK){
            //imageViewSelected.setImageURI(imageUri);

            Bitmap selfieBitmap =((BitmapDrawable)imageViewSelected.getDrawable()).getBitmap();

            Glide.with(this).load(imageUri).override(600, 600).into(imageViewSelected);


            if (imageViewSelected.getId() == R.id.image_foto_book_uno){
                isChangeFotoBookUno = true;
            }else if (imageViewSelected.getId() == R.id.image_foto_book_dos){
                isChangeFotoBookDos = true;
            }else if (imageViewSelected.getId() == R.id.image_foto_book_tres){
                isChangeFotoBookTres = true;
            }else if (imageViewSelected.getId() == R.id.image_foto_book_cuatro){
                isChangeFotoBookCuatro = true;
            }else if (imageViewSelected.getId() == R.id.image_foto_book_cinco){
                isChangeFotoBookCinco = true;
            }else if (imageViewSelected.getId() == R.id.image_foto_selfie){
                isChangeFotoSelfie = true;
            }

            String formulario = validaFormulario();
            if (formulario.equals("OK")){
                btnEnviarParaAprobacion.setEnabled(true);
                btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorBlanco));
            }else{
                btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorGrisAccent));
            }
        }
    }

    @Override
    public void permisosSuccess(int permiso) {

    }

    @Override
    public void permisosCancel(int permiso) {

    }
}
