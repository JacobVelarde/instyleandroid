package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.TalentoPanel;

public class AlertDeleteAddDreamTeam extends AlertDialog {

    private Context context;
    private TalentoPanel talentoPanel;

    private CircleImageView circleImageView;
    private TextView textNombre, textDeleteOrNot;

    public AlertDeleteAddDreamTeam(Context context, TalentoPanel talentoPanel){
        super(context);
        this.context = context;
        this.talentoPanel = talentoPanel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_delete_add_dream_team);

        circleImageView = findViewById(R.id.foto);
        textNombre = findViewById(R.id.text_nombre);
        textDeleteOrNot = findViewById(R.id.text_delete_or_not);

        Glide.with(context).load(talentoPanel.getUrl_perfil().trim()).into(circleImageView);
        textNombre.setText(talentoPanel.getNombres().concat(" "+talentoPanel.getApellidos()));
    }
}
