package e.agenciainstyle.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterActivacionesDashBoard;
import e.agenciainstyle.com.activity.adapter.CalendarAlertDialog;
import e.agenciainstyle.com.activity.adapter.FiltroAdapter;
import e.agenciainstyle.com.activity.adapter.FiltrosAlertAdapter;
import e.agenciainstyle.com.model.Booker;
import e.agenciainstyle.com.model.Categoria;
import e.agenciainstyle.com.model.Cliente;
import e.agenciainstyle.com.model.Coordinador;
import e.agenciainstyle.com.model.Dinamica;
import e.agenciainstyle.com.model.Evento;
import e.agenciainstyle.com.model.Marca;
import e.agenciainstyle.com.model.Operador;
import e.agenciainstyle.com.model.Sede;
import e.agenciainstyle.com.model.Talento;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.FiltroActivacion;
import e.agenciainstyle.com.model.response.BusquedaResponse;
import e.agenciainstyle.com.model.response.BusquedaResponseObject;
import e.agenciainstyle.com.presenter.BuscarActivacionesPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;

public class BuscarActivacionesActivity extends AppCompatActivity {

    @BindView(R.id.btn_clientes)
    TextView btnClientes;
    @BindView(R.id.btn_marcas)
    TextView btnMarcas;
    @BindView(R.id.btn_talentos)
    TextView btnTalentos;
    @BindView(R.id.btn_bookers)
    TextView btnBookers;
    @BindView(R.id.btn_coordinadores)
    TextView btnCoordinadores;
    @BindView(R.id.btn_operadores)
    TextView btnOperadores;
    @BindView(R.id.btn_dinamica)
    TextView btnDinamica;
    @BindView(R.id.btn_periodo)
    TextView btnPeriodo;
    @BindView(R.id.btn_sede)
    TextView btnSede;
    @BindView(R.id.lista_activaciones)
    RecyclerView listaActivaciones;
    @BindView(R.id.btn_buscar)
    TextView btnBuscar;
    private Progress progress;

    private BuscarActivacionesPresenter presenter;
    private User user;
    private BusquedaResponseObject busquedaResponseObject;
    private FiltrosAlertAdapter filtrosAlertAdapter;
    private FiltroActivacion filtroActivacion;
    private LinearLayoutManager layoutManager;
    private AdapterActivacionesDashBoard adapterActivacionesDashBoard;
    private ArrayList<Evento> eventos;
    private ArrayList<Evento> eventosSeleccionados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_activaciones);
        ButterKnife.bind(this);
        filtroActivacion = new FiltroActivacion();
        progress = new Progress(this);

        initListaEventos();
        initPresenter();
    }

    private void initListaEventos(){
        eventosSeleccionados = new ArrayList<>();
        eventos = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        listaActivaciones.setLayoutManager(layoutManager);
        listaActivaciones.setHasFixedSize(true);
        adapterActivacionesDashBoard = new AdapterActivacionesDashBoard(eventos, new AdapterActivacionesDashBoard.ClickItem() {
            @Override
            public void click(Evento evento) {
                //DETALLE EVENTO
                Intent intent = new Intent(BuscarActivacionesActivity.this, DetalleEventoActivity.class);
                intent.putExtra("idEvento", evento.getId());
                startActivity(intent);
            }
        });

        adapterActivacionesDashBoard.setInterfaceSelected(new AdapterActivacionesDashBoard.ClickItemSelected() {
            @Override
            public void clickSelected(Evento evento) {
                //EVENTO SELECCIONADO
                evento.setSelected(!evento.isSelected());
                adapterActivacionesDashBoard.notifyDataSetChanged();

                if (evento.isSelected()){
                    eventosSeleccionados.add(evento);
                }else{
                    removerEvento(evento);
                }
            }
        });

        listaActivaciones.setAdapter(adapterActivacionesDashBoard);
    }

    private void removerEvento(Evento evento){
        for (int i = 0; i < eventosSeleccionados.size(); i++){
            if (eventosSeleccionados.get(i).getId().compareTo(evento.getId()) == 0){
                eventosSeleccionados.remove(i);
            }
        }
    }

    private void initPresenter() {
        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        presenter = new BuscarActivacionesPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                eventos.clear();

                filtroActivacion = null;
                filtroActivacion = new FiltroActivacion();

                filtroActivacion.setPeriodo("-1");
                filtroActivacion.setSede("-1");
                filtroActivacion.setMarca(-1);
                filtroActivacion.setDinamica(-1);
                filtroActivacion.setCliente(-1);
                filtroActivacion.setTalento(-1);
                filtroActivacion.setBooker(-1);
                filtroActivacion.setCoordinador(-1);
                filtroActivacion.setOperador("-1");
                filtroActivacion.setCategoriaTalento(-1);

                progress.dismiss();
                BusquedaResponse busquedaResponse = (BusquedaResponse) response;
                busquedaResponseObject = null;
                busquedaResponseObject = busquedaResponse.getObject();
                eventos.addAll(busquedaResponseObject.getActivaciones());
                adapterActivacionesDashBoard.notifyDataSetChanged();

                filtrosAlertAdapter = null;

                filtrosAlertAdapter = new FiltrosAlertAdapter(BuscarActivacionesActivity.this, busquedaResponseObject.getFiltros(), new FiltrosAlertAdapter.FiltrosAlertAdapterItem() {
                    @Override
                    public void itemClick(Object object) {
                        parseObject(object);
                    }
                });

            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                new Mensaje(BuscarActivacionesActivity.this, error, false).show();
            }
        });

        filtroActivacion.setPeriodo("-1");
        filtroActivacion.setSede("-1");
        filtroActivacion.setMarca(-1);
        filtroActivacion.setDinamica(-1);
        filtroActivacion.setCliente(-1);
        filtroActivacion.setTalento(-1);
        filtroActivacion.setBooker(-1);
        filtroActivacion.setCoordinador(-1);
        filtroActivacion.setOperador("-1");
        filtroActivacion.setCategoriaTalento(-1);

        progress.show();
        presenter.buscarActivaciones(filtroActivacion, user.getId(), user.getToken());
    }

    @OnClick({R.id.btn_buscar, R.id.btn_clientes, R.id.btn_marcas, R.id.btn_talentos, R.id.btn_bookers, R.id.btn_coordinadores, R.id.btn_operadores, R.id.btn_dinamica, R.id.btn_periodo, R.id.btn_sede})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_buscar:
                progress.show();
                presenter.buscarActivaciones(filtroActivacion, user.getId(), user.getToken());
                break;
            case R.id.btn_clientes:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.CLIENTE);
                break;
            case R.id.btn_marcas:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.MARCA);
                break;
            case R.id.btn_talentos:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.TALENTO);
                break;
            case R.id.btn_bookers:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.BOOKER);
                break;
            case R.id.btn_coordinadores:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.COORDINADOR);
                break;
            case R.id.btn_operadores:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.OPERADOR);
                break;
            case R.id.btn_dinamica:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.DINAMICA);
                break;
            case R.id.btn_periodo:
                CalendarAlertDialog calendarAlertDialog = new CalendarAlertDialog(this, new CalendarAlertDialog.CalendarDates() {
                    @Override
                    public void datesSelected(Calendar first, Calendar second) {
                        String dateFirst = "";
                        String dateSecond = "";

                        dateFirst = first.get(Calendar.YEAR)+ "-" + first.get(Calendar.MONTH) + "-" + first.get(Calendar.DATE);
                        dateSecond = second.get(Calendar.YEAR)+ "-" + second.get(Calendar.MONTH) + "-" + second.get(Calendar.DATE);

                        filtroActivacion.setPeriodo(dateFirst+"*"+dateSecond);
                    }
                });

                calendarAlertDialog.show();
                break;
            case R.id.btn_sede:
                setTipoFiltro(FiltroAdapter.TIPO_FILTRO.SEDE);
                break;
        }
    }

    private void setTipoFiltro(FiltroAdapter.TIPO_FILTRO tipoFiltro) {
        filtrosAlertAdapter.setTipoFiltro(tipoFiltro);

        filtrosAlertAdapter.filtroVisible(tipoFiltro == FiltroAdapter.TIPO_FILTRO.TALENTO);

        filtrosAlertAdapter.show();
    }

    private void parseObject(Object object) {
        if (object instanceof Cliente) {
            Cliente cliente = ((Cliente) object);
            filtroActivacion.setCliente(Integer.parseInt(cliente.getId()));

        } else if (object instanceof Marca) {
            Marca marca = ((Marca) object);
            filtroActivacion.setMarca(marca.getId());

        } else if (object instanceof Talento) {
            Talento talento = (Talento) object;
            filtroActivacion.setTalento(talento.getId());
            filtroActivacion.setCategoriaTalento(Integer.parseInt(talento.getCategoriaId()));

        } else if (object instanceof Booker) {
            Booker booker = (Booker) object;
            filtroActivacion.setBooker(booker.getId());

        } else if (object instanceof Coordinador) {
            Coordinador coordinador = (Coordinador) object;
            filtroActivacion.setCoordinador(coordinador.getId());

        } else if (object instanceof Operador) {
            Operador operador = (Operador) object;
            filtroActivacion.setOperador(operador.getOperador());

        } else if (object instanceof Dinamica) {
            Dinamica dinamica = ((Dinamica) object);
            filtroActivacion.setDinamica(dinamica.getId());

        } else if (object instanceof Sede) {
            Sede sede = ((Sede) object);
            filtroActivacion.setSede(sede.getSede());

        } else if (object instanceof Categoria) {

        }
    }
}
