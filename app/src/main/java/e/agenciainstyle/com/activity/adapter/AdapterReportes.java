package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.response.ReporteResponse;

public class AdapterReportes extends BaseExpandableListAdapter {

    public ReporteResponse reportes;
    public Context context;

    public AdapterReportes(ReporteResponse reportes, Context context) {
        this.reportes = reportes;
        this.context = context;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return reportes.getObjeto().get(groupPosition).getReporte().getNombre_evento();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String textChild = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_child_expandable_reporte, null);
        }

        TextView textViewChild = (TextView) convertView.findViewById(R.id.text_child);
        textViewChild.setText(textChild);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return reportes.getObjeto().get(groupPosition).getTitulo();
    }

    @Override
    public int getGroupCount() {
        return reportes.getObjeto() != null ? reportes.getObjeto().size() : 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String listTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_header_expandable_reporte, null);
        }

        TextView textViewGroup = (TextView) convertView.findViewById(R.id.header_title);
        textViewGroup.setText(listTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
