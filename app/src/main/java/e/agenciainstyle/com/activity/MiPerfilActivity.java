package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.PerfilResponse;
import e.agenciainstyle.com.presenter.MiPerfilPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;

public class MiPerfilActivity extends AppCompatActivity {

    @BindView(R.id.text_nombre_header)
    TextView textNombreHeader;
    @BindView(R.id.text_perfil)
    TextView textPerfil;
    @BindView(R.id.text_nombre)
    EditText textNombre;
    @BindView(R.id.text_email)
    EditText textEmail;
    @BindView(R.id.text_telefono)
    EditText textTelefono;
    @BindView(R.id.text_fecha_nacimiento)
    EditText textFechaNacimiento;
    @BindView(R.id.image_view)
    CircleImageView imageView;

    private MiPerfilPresenter miPerfilPresenter;
    private User user;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_perfil);
        ButterKnife.bind(this);

        initPresenter();
    }

    private void initPresenter() {
        progress = new Progress(this);
        miPerfilPresenter = new MiPerfilPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                PerfilResponse perfilResponse = (PerfilResponse) response;

                textNombreHeader.setText(perfilResponse.getObjeto().get(0).getNombres());
                textPerfil.setText(perfilResponse.getObjeto().get(0).getPerfil());
                textEmail.setText(perfilResponse.getObjeto().get(0).getCorreo());
                textTelefono.setText(perfilResponse.getObjeto().get(0).getTelefono());
                Glide.with(MiPerfilActivity.this).load(perfilResponse.getObjeto().get(0).getUrl_profile()).into(imageView);

            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                new Mensaje(MiPerfilActivity.this, error, false).show();
            }
        });

        progress.show();
        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        miPerfilPresenter.getPerfil(user.getId(), user.getToken());

    }
}
