package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.AgregaTalentoRequest;
import e.agenciainstyle.com.presenter.AgregaTalentoPresenter;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Tools;

public class AgregarTalentoActivity extends AppCompatActivity {

    @BindView(R.id.btn_mujer)
    TextView btnMujer;
    @BindView(R.id.btn_hombre)
    TextView btnHombre;
    @BindView(R.id.text_nombres)
    EditText textNombres;
    @BindView(R.id.text_apellidos)
    EditText textApellidos;
    @BindView(R.id.text_email)
    EditText textEmail;
    @BindView(R.id.text_confirmar_email)
    EditText textConfirmarEmail;
    @BindView(R.id.text_contrasenia)
    EditText textContrasenia;
    @BindView(R.id.text_mes)
    EditText textMes;
    @BindView(R.id.text_dia)
    EditText textDia;
    @BindView(R.id.text_anio)
    EditText textAnio;
    @BindView(R.id.btn_enviar_para_aprobacion)
    TextView btnEnviarParaAprobacion;

    private AgregaTalentoPresenter agregaTalentoPresenter;
    private Progress progress;
    private boolean showHombre = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_talento);
        ButterKnife.bind(this);

        progress = new Progress(this);

        onChanceTextView(textNombres);
        onChanceTextView(textApellidos);
        onChanceTextView(textEmail);
        onChanceTextView(textConfirmarEmail);
        onChanceTextView(textContrasenia);
        onChanceTextView(textMes);
        onChanceTextView(textDia);
        onChanceTextView(textAnio);

        initPresenter();
    }

    private void initPresenter() {
        agregaTalentoPresenter = new AgregaTalentoPresenter(this, new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                GenericResponse genericResponse = (GenericResponse) response;
                new Mensaje(AgregarTalentoActivity.this,  genericResponse.getMensaje(), false).show();
            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                new Mensaje(AgregarTalentoActivity.this,  error, false).show();
            }
        });
    }

    @OnClick({R.id.btn_mujer, R.id.btn_hombre})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_mujer:
                showHombre = false;
                btnMujer.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnMujer.setTextColor(getResources().getColor(R.color.colorBlanco));

                btnHombre.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnHombre.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));
                break;
            case R.id.btn_hombre:
                showHombre = true;
                btnMujer.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnMujer.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                btnHombre.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnHombre.setTextColor(getResources().getColor(R.color.colorBlanco));
                break;
        }
    }

    private String validaFormulario(){
        if (textNombres.getText().toString().length() <= 0){
            return "Ingresa un nombre";
        }else if (textApellidos.getText().toString().length() <= 0){
            return "Ingresa un apellido";
        }else if (textEmail.getText().toString().length() <= 0){
            return "Ingresa un email";
        }else if (textConfirmarEmail.getText().toString().length() <= 0){
            return "Ingresa la confirmación del email";
        }else if (textContrasenia.getText().toString().length() <= 0){
            return "Ingresa la contraseña";
        }else if (textMes.getText().toString().length() <= 0){
            return "Ingresa el mes de nacimiento";
        }else if (textDia.getText().toString().length() <= 0){
            return "Ingresa el dia de nacimiento";
        }else if (textAnio.getText().toString().length() <= 0){
            return "Ingresa el año de nacimiento";
        }else if (!Tools.isValidEmail(textEmail.getText().toString())){
            return "Ingresa un email valido";
        }else if (textEmail.getText().toString().compareTo(textConfirmarEmail.getText().toString()) != 0){
            return "Ingresa un email que conincida con la confirmación";
        }else{
            return "OK";
        }
    }

    public void onChanceTextView(EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String formulario = validaFormulario();
                if (formulario.equals("OK")){
                    btnEnviarParaAprobacion.setEnabled(true);
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorBlanco));
                }else{
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorGrisAccent));
                }
            }
        });
    }

    @OnClick(R.id.btn_enviar_para_aprobacion)
    public void onViewClicked() {
        //onBackPressed();
        String validForm = validaFormulario();

        if (validForm.compareTo("OK")==0){
            progress.show();

            AgregaTalentoRequest agregaTalentoRequest = new AgregaTalentoRequest();
            agregaTalentoRequest.setNombres(textNombres.getText().toString());
            agregaTalentoRequest.setApellidos(textApellidos.getText().toString());
            agregaTalentoRequest.setEmail(textEmail.getText().toString());
            agregaTalentoRequest.setPassword(textContrasenia.getText().toString());
            agregaTalentoRequest.setTipoSolicitud("2");
            agregaTalentoRequest.setGenero(showHombre?"Masculino":"Femenino");
            agregaTalentoRequest.setFechaNacimiento(textAnio.getText() + "-" + textMes.getText() + "-" + textDia.getText());
            User user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
            agregaTalentoPresenter.addTalento(agregaTalentoRequest, user);
        }
    }
}
