package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Cliente;
import e.agenciainstyle.com.model.response.BanderasResponse;
import e.agenciainstyle.com.presenter.BanderasPresenter;
import e.agenciainstyle.com.presenter.ClientePresenter;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.CodigosBanderasView;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Tools;

public class FormularioUneteClienteActivity extends AppCompatActivity {


    @BindView(R.id.text_nombre)
    EditText textNombre;
    @BindView(R.id.text_apellidos)
    EditText textApellidos;
    @BindView(R.id.text_empresa)
    EditText textEmpresa;
    @BindView(R.id.text_direccion_empresa)
    EditText textDireccionEmpresa;
    @BindView(R.id.text_email_empresa)
    EditText textEmailEmpresa;
    @BindView(R.id.text_confirmar_email)
    EditText textConfirmarEmail;
    @BindView(R.id.text_contrasenia)
    EditText textContrasenia;
    @BindView(R.id.text_cargo_en_la_empresa)
    EditText textCargoEnLaEmpresa;
    @BindView(R.id.text_telefono)
    EditText textTelefono;
    @BindView(R.id.text_linked_in_link)
    EditText textLinkedInLink;
    @BindView(R.id.text_webpage_link)
    EditText textWebpageLink;
    @BindView(R.id.text_instagram_link)
    EditText textInstagramLink;
    @BindView(R.id.btn_enviar_para_aprobacion)
    TextView btnEnviarParaAprobacion;
    @BindView(R.id.imagen_bandera)
    ImageView imagenBandera;
    @BindView(R.id.text_prefijo)
    TextView textPrefijo;

    private ClientePresenter clientePresenter;
    private BanderasPresenter banderasPresenter;
    private CodigosBanderasView codigosBanderasView;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_unete_cliente);
        ButterKnife.bind(this);
        btnEnviarParaAprobacion.setEnabled(false);

        onChanceTextView(textNombre);
        onChanceTextView(textApellidos);
        onChanceTextView(textEmpresa);
        onChanceTextView(textDireccionEmpresa);
        onChanceTextView(textEmailEmpresa);
        onChanceTextView(textConfirmarEmail);
        onChanceTextView(textContrasenia);
        onChanceTextView(textCargoEnLaEmpresa);
        onChanceTextView(textTelefono);
        onChanceTextView(textLinkedInLink);
        onChanceTextView(textWebpageLink);
        onChanceTextView(textInstagramLink);

        progress = new Progress(this);
        initBanderasPresenter();
        initClientePresenter();

    }

    private void initClientePresenter() {
        clientePresenter = new ClientePresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                GenericResponse genericResponse = (GenericResponse) response;

                Mensaje mensaje = new Mensaje(
                        FormularioUneteClienteActivity.this,
                        genericResponse.getMensaje(),
                        false);
                mensaje.setInterface(new Mensaje.ButtonAceptar() {
                    @Override
                    public void onClick() {
                        ActivityManager.removeAll(FormularioUneteClienteActivity.this, SplashActivity.class);
                    }
                });
                mensaje.show();

            }

            @Override
            public void serverSuccess(int request) {

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(
                        FormularioUneteClienteActivity.this,
                        error,
                        false);
                mensaje.show();
            }
        });

    }

    private void initBanderasPresenter() {

        banderasPresenter = new BanderasPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                BanderasResponse banderasResponse = (BanderasResponse) response;

                if (banderasResponse.getObjeto() != null) {
                    if (!banderasResponse.getObjeto().isEmpty()) {
                        codigosBanderasView = new CodigosBanderasView(FormularioUneteClienteActivity.this,
                                banderasResponse.getObjeto(),
                                bandera -> {

                                    textPrefijo.setText(bandera.getPrefijo());

                                });
                    }
                }
            }

            @Override
            public void serverSuccess(int request) {

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(FormularioUneteClienteActivity.this, error, false);
                mensaje.show();
            }
        });
    }

    public void onChanceTextView(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String formulario = validaFormulario();
                if (formulario.equals("OK")) {
                    btnEnviarParaAprobacion.setEnabled(true);
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorBlanco));
                } else {
                    btnEnviarParaAprobacion.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                    btnEnviarParaAprobacion.setTextColor(getResources().getColor(R.color.colorGrisAccent));
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        progress.show();
        banderasPresenter.getBanderas();
    }

    public String validaFormulario() {
        if (textNombre.getText().toString().length() <= 0) {
            return "Ingresa un nombre";
        } else if (textApellidos.getText().toString().length() <= 0) {
            return "Ingresa un apellido";
        } else if (textEmpresa.getText().toString().length() <= 0) {
            return "Ingresa la empresa";
        } else if (textDireccionEmpresa.getText().toString().length() <= 0) {
            return "Ingresa la dirección de la empresa";
        } else if (textEmailEmpresa.getText().toString().length() <= 0) {
            return "Ingresa el email de la empresa";
        } else if (textConfirmarEmail.getText().toString().length() <= 0) {
            return "Ingresa la confirmación del email de la empresa";
        } else if (textContrasenia.getText().toString().length() <= 0) {
            return "Ingresa la contraseña";
        } else if (textCargoEnLaEmpresa.getText().toString().length() <= 0) {
            return "Ingresa el cargo en la empresa";
        } else if (textTelefono.getText().toString().length() <= 0) {
            return "Ingresa el telefono de la empresa";
        } else if (textLinkedInLink.getText().toString().length() <= 0) {
            return "Ingresa el link de Linked In";
        } else if (textWebpageLink.getText().toString().length() <= 0) {
            return "Ingresa el link de la página web";
        } else if (textInstagramLink.getText().toString().length() <= 0) {
            return "Ingresa el link del Instagram de la empresa";
        } else if (!Tools.isValidEmail(textEmailEmpresa.getText().toString())){
            return "Ingresa un email valdio";
        }else if (textEmailEmpresa.getText().toString().compareTo(textConfirmarEmail.getText().toString()) != 0){
            return "Ingresa un email que conincida con la confirmación";
        } else {
            return "OK";
        }
    }

    @OnClick({R.id.imagen_bandera, R.id.text_prefijo, R.id.btn_enviar_para_aprobacion})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imagen_bandera:
            case R.id.text_prefijo:
                if (codigosBanderasView != null){
                    codigosBanderasView.show();
                }
                break;
            case R.id.btn_enviar_para_aprobacion:

                String respuesta = validaFormulario();

                if (respuesta.compareTo("OK") == 0){
                    progress.show();

                    Cliente cliente = new Cliente();
                    cliente.setTipoSolicitud("1");
                    cliente.setNombres(textNombre.getText().toString());
                    cliente.setApellidos(textApellidos.getText().toString());
                    cliente.setEmpresa(textEmpresa.getText().toString());
                    cliente.setDireccion(textDireccionEmpresa.getText().toString());
                    cliente.setEmail(textEmailEmpresa.getText().toString());
                    cliente.setPassword(textContrasenia.getText().toString());
                    cliente.setCargo(textCargoEnLaEmpresa.getText().toString());
                    cliente.setTelefono(textPrefijo.getText().toString().concat(textTelefono.getText().toString()));
                    cliente.setUrlLinkedIn(textLinkedInLink.getText().toString());
                    cliente.setUrlWebSite(textWebpageLink.getText().toString());
                    cliente.setUrlInstagram(textInstagramLink.getText().toString());

                    clientePresenter.registrar(cliente);

                }else{
                    Mensaje mensaje = new Mensaje(this, respuesta, false);
                    mensaje.show();
                }

                break;
        }
    }
}
