package e.agenciainstyle.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterActivacionesDashBoard;
import e.agenciainstyle.com.activity.adapter.AdapterCalendar;
import e.agenciainstyle.com.activity.adapter.AdapterDetalleEventoDia;
import e.agenciainstyle.com.model.Evento;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.EventoCalendarResponse;
import e.agenciainstyle.com.model.response.DashBoardAdministradorResponse;
import e.agenciainstyle.com.model.response.EventoDetalleDiaResponse;
import e.agenciainstyle.com.presenter.PresenterDashboardAdministrador;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;

public class ActivacionesActivity extends AppCompatActivity {

    @BindView(R.id.btn_crear_activacion)
    ImageView btnCrearActivacion;
    @BindView(R.id.btn_filtrar_activaciones)
    ImageView btnFiltrarActivaciones;
    @BindView(R.id.btn_menu_principal)
    ImageView btnMenuPrincipal;
    @BindView(R.id.image_completadas)
    ImageView imageCompletadas;
    @BindView(R.id.image_en_curso)
    ImageView imageEnCurso;
    @BindView(R.id.image_proximas)
    ImageView imageProximas;
    @BindView(R.id.image_total)
    ImageView imageTotal;
    @BindView(R.id.text_numero_completadas)
    TextView textNumeroCompletadas;
    @BindView(R.id.text_numero_en_curso)
    TextView textNumeroEnCurso;
    @BindView(R.id.text_numero_proximas)
    TextView textNumeroProximas;
    @BindView(R.id.text_numero_total)
    TextView textNumeroTotal;
    @BindView(R.id.text_activaciones_visibles)
    TextView textActivacionesVisibles;
    @BindView(R.id.lista_activaciones)
    RecyclerView listaActivaciones;
    @BindView(R.id.arrow_left)
    ImageView arrowLeft;
    @BindView(R.id.text_mes)
    TextView textMes;
    @BindView(R.id.text_anio)
    TextView textAnio;
    @BindView(R.id.arrow_right)
    ImageView arrowRight;
    @BindView(R.id.calendar_grid)
    GridView calendarGrid;
    @BindView(R.id.lista_detalle_dia)
    RecyclerView listaDetalleDia;

    private AdapterActivacionesDashBoard adapterActivacionesDashBoard;
    private AdapterDetalleEventoDia adapterDetalleEventoDia;
    private ArrayList<Evento> eventos;
    private ArrayList<Evento> eventosDia;
    private int diaDetalle;
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    private static final int MAX_CALENDAR_COLUMN = 42;
    private ArrayList<Date> dayValueInCells;
    private AdapterCalendar adapterCalendar;
    private PresenterDashboardAdministrador presenterDashboardAdministrador;
    private User user;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activaciones);
        ButterKnife.bind(this);

        progress = new Progress(this);
        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        dayValueInCells = new ArrayList<>();

        initPresenter();
        initAdaptarActivaciones();
        initAdapterCaldendario();
        initAdapterDetalleDia();
        updateCalendar();

    }

    private void initAdapterDetalleDia() {
        eventosDia = new ArrayList<>();

        adapterDetalleEventoDia = new AdapterDetalleEventoDia(eventosDia, diaDetalle, new AdapterDetalleEventoDia.ClickItem() {
            @Override
            public void click(Evento evento) {
                Intent intent = new Intent(ActivacionesActivity.this, DetalleEventoActivity.class);
                intent.putExtra("idEvento", evento.getId());
                startActivity(intent);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listaDetalleDia.setLayoutManager(layoutManager);
        listaDetalleDia.setHasFixedSize(true);
        listaDetalleDia.setAdapter(adapterDetalleEventoDia);
    }

    private void initPresenter() {
        presenterDashboardAdministrador = new PresenterDashboardAdministrador(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                if (request == PresenterDashboardAdministrador.PANEL) {
                    DashBoardAdministradorResponse dashBoardAdministrador = (DashBoardAdministradorResponse) response;

                    textNumeroCompletadas.setText(String.valueOf(dashBoardAdministrador.getObjeto().getCompletadas()));
                    textNumeroEnCurso.setText(String.valueOf(dashBoardAdministrador.getObjeto().getEncurso()));
                    textNumeroProximas.setText(String.valueOf(dashBoardAdministrador.getObjeto().getProximas()));
                    textNumeroTotal.setText(String.valueOf(dashBoardAdministrador.getObjeto().getTotal()));

                    eventos.clear();
                    eventos.addAll(dashBoardAdministrador.getObjeto().getListado());
                    adapterActivacionesDashBoard.notifyDataSetChanged();

                } else if (request == PresenterDashboardAdministrador.EVENTOS_CALENDAR) {
                    EventoCalendarResponse eventoCalendarResponse = (EventoCalendarResponse) response;
                    if (eventoCalendarResponse.getObjeto().getDias() != null) {
                        if (!eventoCalendarResponse.getObjeto().getDias().isEmpty()) {
                            adapterCalendar.setEvents(eventoCalendarResponse.getObjeto().getDias());
                        } else {
                            adapterCalendar.setEvents(null);
                        }
                    } else {
                        adapterCalendar.setEvents(null);
                    }
                } else if(request == PresenterDashboardAdministrador.EVENTOS_DIA){
                    EventoDetalleDiaResponse eventoDetalleDiaResponse = (EventoDetalleDiaResponse) response;
                    eventosDia.clear();
                    eventosDia.addAll(eventoDetalleDiaResponse.getObjeto().getListado());
                    adapterDetalleEventoDia.dia = diaDetalle;
                    adapterDetalleEventoDia.notifyDataSetChanged();
                }
            }

            @Override
            public void serverSuccess(int request) {

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(ActivacionesActivity.this, error, false);
                mensaje.show();

                textNumeroCompletadas.setText("");
                textNumeroEnCurso.setText("");
                textNumeroProximas.setText("");
                textNumeroTotal.setText("");

                eventos.clear();
                adapterActivacionesDashBoard.notifyDataSetChanged();
            }
        });

        //TODO CARGA POR DEFAULT
        progress.show();
        textActivacionesVisibles.setText("ACTIVACIONES EN CURSO");
        presenterDashboardAdministrador.getPanel("encurso", user.getId(), user.getToken());
    }

    private void initAdapterCaldendario() {
        adapterCalendar = new AdapterCalendar(this, dayValueInCells, cal, new AdapterCalendar.DayCLick() {
            @Override
            public void dayClick(int day, int month, int year) {
                progress.show();
                diaDetalle = day;
                presenterDashboardAdministrador.getDetalleDias(day, month, year, user.getId(), user.getToken());
            }
        });

        calendarGrid.setAdapter(adapterCalendar);
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {

        adapterCalendar.setEvents(null);
        dayValueInCells.clear();
        Calendar mCal = (Calendar) cal.clone();
        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);

        while (dayValueInCells.size() < MAX_CALENDAR_COLUMN) {
            dayValueInCells.add(mCal.getTime());
            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }

        adapterCalendar.notifyDataSetChanged();

        SimpleDateFormat sdf = new SimpleDateFormat("MMMM,yyyy");
        String[] dateToday = sdf.format(cal.getTime()).split(",");
        textMes.setText(dateToday[0].toUpperCase());
        textAnio.setText(dateToday[1]);

        progress.show();
        presenterDashboardAdministrador.getEventosCalendario(textAnio.getText().toString(), textMes.getText().toString(), user.getId(), user.getToken());
    }

    private void initAdaptarActivaciones() {
        eventos = new ArrayList<>();

        adapterActivacionesDashBoard = new AdapterActivacionesDashBoard(eventos, new AdapterActivacionesDashBoard.ClickItem() {
            @Override
            public void click(Evento evento) {
                Intent intent = new Intent(ActivacionesActivity.this, DetalleEventoActivity.class);
                intent.putExtra("idEvento", evento.getId());
                startActivity(intent);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listaActivaciones.setLayoutManager(layoutManager);
        listaActivaciones.setHasFixedSize(true);
        listaActivaciones.setAdapter(adapterActivacionesDashBoard);

    }

    @OnClick({R.id.btn_menu_principal, R.id.btn_crear_activacion, R.id.btn_filtrar_activaciones, R.id.image_completadas, R.id.image_en_curso, R.id.image_proximas, R.id.image_total, R.id.text_numero_completadas, R.id.text_numero_en_curso, R.id.text_numero_proximas, R.id.text_numero_total, R.id.text_activaciones_visibles, R.id.arrow_left, R.id.arrow_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_crear_activacion:
                Toast.makeText(ActivacionesActivity.this, "En construccion", Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_filtrar_activaciones:
                ActivityManager.next(ActivacionesActivity.this, BuscarActivacionesActivity.class);
                break;

            case R.id.btn_menu_principal:
                onBackPressed();
                break;
            case R.id.image_completadas:
                textActivacionesVisibles.setText("ACTIVACIONES COMPLETADAS");
                progress.show();
                presenterDashboardAdministrador.getPanel("completadas", user.getId(), user.getToken());
                break;
            case R.id.image_en_curso:
                textActivacionesVisibles.setText("ACTIVACIONES EN CURSO");
                progress.show();
                presenterDashboardAdministrador.getPanel("encurso", user.getId(), user.getToken());
                break;
            case R.id.image_proximas:
                textActivacionesVisibles.setText("PRÓXIMAS ACTIVACIONES");
                progress.show();
                presenterDashboardAdministrador.getPanel("proximas", user.getId(), user.getToken());
                break;
            case R.id.image_total:
                textActivacionesVisibles.setText("TODAS LAS ACTIVACIONES");
                progress.show();
                presenterDashboardAdministrador.getPanel("total", user.getId(), user.getToken());
                break;
            case R.id.text_numero_completadas:

                break;
            case R.id.text_numero_en_curso:

                break;
            case R.id.text_numero_proximas:

                break;
            case R.id.text_numero_total:

                break;
            case R.id.text_activaciones_visibles:
                break;

            case R.id.arrow_left:
                cal.add(Calendar.MONTH, -1);
                updateCalendar();
                break;
            case R.id.arrow_right:
                cal.add(Calendar.MONTH, 1);
                updateCalendar();
                break;
        }
    }
}
