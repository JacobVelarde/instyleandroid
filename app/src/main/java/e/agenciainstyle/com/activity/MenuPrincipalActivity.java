package e.agenciainstyle.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.CalendarAlertDialog;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.MenuPrincipalEstatusResponse;
import e.agenciainstyle.com.presenter.MenuPrincipalPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Usuario;

public class MenuPrincipalActivity extends AppCompatActivity {

    private ImageView btnClientes;
    private ImageView btnDreamTeam;
    private ImageView btnTalentos;
    private ImageView btnActivacion;
    private ImageView btnReporte;
    private ImageView btnMiPefil;
    private ImageView btnNotificacion;
    private TextView textNumeroNotificaciones;
    private ImageView btnInvitacion;

    private User user;
    private MenuPrincipalPresenter menuPrincipalPresenter;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initPresenter();
        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);

        if (user.getPerfil().compareTo(Usuario.ADMIN.getTipoCliente()) == 0) {
            setContentView(R.layout.activity_menu_principal_admin);
            initViewAdmin();
        }else if (user.getPerfil().compareTo(Usuario.CLIENTE.getTipoCliente()) == 0 ){
            setContentView(R.layout.activity_menu_principal_cliente);
            initViewCliente();
        }else if (user.getPerfil().compareTo(Usuario.TALENTO.getTipoCliente()) == 0){
            setContentView(R.layout.activity_menu_principal_talento);
            initViewTalento();
        }else if (user.getPerfil().compareTo(Usuario.COORDINADOR.getTipoCliente()) == 0){
            setContentView(R.layout.activity_menu_principal_coordinador);
            initViewCoordinador();
        }else if (user.getPerfil().compareTo(Usuario.PRODUCTOR.getTipoCliente()) == 0){
            setContentView(R.layout.activity_menu_principal_productor);
            initViewProductor();
        }
    }

    private void initPresenter(){
        progress = new Progress(this);
        menuPrincipalPresenter = new MenuPrincipalPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                MenuPrincipalEstatusResponse menuPrincipalEstatusResponse = (MenuPrincipalEstatusResponse) response;

                if (menuPrincipalEstatusResponse.getObjeto().getFlag_pendientes() == 0){
                    textNumeroNotificaciones.setVisibility(View.INVISIBLE);
                }else{
                    textNumeroNotificaciones.setText(menuPrincipalEstatusResponse.getObjeto().getFlag_pendientes()+"");
                }
            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                new Mensaje(MenuPrincipalActivity.this, error, false).show();
            }
        });
    }

    private void initViewProductor() {
        btnReporte = findViewById(R.id.btn_reporte);
        btnMiPefil = findViewById(R.id.btn_mi_pefil);
        btnActivacion = findViewById(R.id.btn_activacion);
        btnInvitacion = findViewById(R.id.btn_invitaciones);
        btnNotificacion = findViewById(R.id.btn_notificacion);
        textNumeroNotificaciones = findViewById(R.id.text_numero_notificaciones);

        btnReporte.setOnClickListener(view ->{
            ActivityManager.next(this, ReportesActivity.class);
        });

        btnMiPefil.setOnClickListener(view ->{
            ActivityManager.next(this, MiPerfilActivity.class);

        });

        btnActivacion.setOnClickListener(view -> ActivityManager.next(this, ActivacionesActivity.class));

        btnInvitacion.setOnClickListener(view ->{
            ActivityManager.next(this, InvitacionesActivity.class);
        });

        btnNotificacion.setOnClickListener(view ->{
            ActivityManager.next(this, NotificacionesActivity.class);
        });
    }

    private void initViewCoordinador() {
        btnReporte = findViewById(R.id.btn_reporte);
        btnMiPefil = findViewById(R.id.btn_mi_pefil);
        btnActivacion = findViewById(R.id.btn_activacion);
        btnNotificacion = findViewById(R.id.btn_notificacion);
        textNumeroNotificaciones = findViewById(R.id.text_numero_notificaciones);


        btnReporte.setOnClickListener(view ->{
            ActivityManager.next(this, ReportesActivity.class);
        });

        btnMiPefil.setOnClickListener(view ->{
            ActivityManager.next(this, MiPerfilActivity.class);

        });

        btnActivacion.setOnClickListener(view -> ActivityManager.next(this, ActivacionesActivity.class));

        btnNotificacion.setOnClickListener(view ->{
            ActivityManager.next(this, NotificacionesActivity.class);
        });
    }

    private void initViewTalento() {
        btnReporte = findViewById(R.id.btn_reporte);
        btnMiPefil = findViewById(R.id.btn_mi_pefil);
        btnActivacion = findViewById(R.id.btn_activacion);
        btnInvitacion = findViewById(R.id.btn_invitaciones);
        btnNotificacion = findViewById(R.id.btn_notificacion);
        textNumeroNotificaciones = findViewById(R.id.text_numero_notificaciones);

        btnReporte.setOnClickListener(view ->{
            ActivityManager.next(this, ReportesActivity.class);
        });

        btnMiPefil.setOnClickListener(view ->{
            ActivityManager.next(this, MiPerfilActivity.class);

        });

        btnActivacion.setOnClickListener(view -> ActivityManager.next(this, ActivacionesActivity.class));

        btnInvitacion.setOnClickListener(view ->{
            ActivityManager.next(this, InvitacionesActivity.class);
        });

        btnNotificacion.setOnClickListener(view ->{
            ActivityManager.next(this, NotificacionesActivity.class);
        });
    }

    private void initViewCliente() {
        btnDreamTeam = findViewById(R.id.btn_dream_team);
        btnTalentos = findViewById(R.id.btn_talentos);
        btnActivacion = findViewById(R.id.btn_activacion);
        btnReporte = findViewById(R.id.btn_reporte);
        btnMiPefil = findViewById(R.id.btn_mi_pefil);
        btnNotificacion = findViewById(R.id.btn_notificacion);
        textNumeroNotificaciones = findViewById(R.id.text_numero_notificaciones);

        btnDreamTeam.setOnClickListener(view ->{
            Intent intent = new Intent(this, FiltroTalentosActivity.class);
            intent.putExtra("onlyDreamTeam",true);
            startActivity(intent);
            //Toast.makeText(this, "En construcción",Toast.LENGTH_LONG).show();
        });

        btnTalentos.setOnClickListener(view ->{
            Intent intent = new Intent(this, FiltroTalentosActivity.class);
            intent.putExtra("allTalentos",true);
            startActivity(intent);
        });

        btnActivacion.setOnClickListener(view -> ActivityManager.next(this, ActivacionesActivity.class));

        btnReporte.setOnClickListener(view ->{
            ActivityManager.next(this, ReportesActivity.class);
        });

        btnMiPefil.setOnClickListener(view ->{
            ActivityManager.next(this, MiPerfilActivity.class);
        });

        btnNotificacion.setOnClickListener(view ->{
            ActivityManager.next(this, NotificacionesActivity.class);
        });
    }

    private void initViewAdmin(){

        btnClientes = findViewById(R.id.btn_clientes);
        btnTalentos = findViewById(R.id.btn_talentos);
        btnActivacion = findViewById(R.id.btn_activacion);
        btnReporte = findViewById(R.id.btn_reporte);
        btnMiPefil = findViewById(R.id.btn_mi_pefil);
        btnNotificacion = findViewById(R.id.btn_notificacion);
        textNumeroNotificaciones = findViewById(R.id.text_numero_notificaciones);

        btnClientes.setOnClickListener(view ->{
            Toast.makeText(this, "En construcción",Toast.LENGTH_LONG).show();
        });

        btnTalentos.setOnClickListener(view ->{
            ActivityManager.next(this, FiltroTalentosActivity.class);

        });

        btnActivacion.setOnClickListener(view -> ActivityManager.next(this, ActivacionesActivity.class));

        btnReporte.setOnClickListener(view ->{
            ActivityManager.next(this, ReportesActivity.class);
        });

        btnMiPefil.setOnClickListener(view ->{
            ActivityManager.next(this, MiPerfilActivity.class);
        });

        btnNotificacion.setOnClickListener(view ->{
            ActivityManager.next(this, NotificacionesActivity.class);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        progress.show();
        menuPrincipalPresenter.getStatusPanel(user.getId(), user.getToken());
    }
}
