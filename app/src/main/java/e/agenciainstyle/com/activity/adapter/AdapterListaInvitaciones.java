package e.agenciainstyle.com.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Invitacion;

public class AdapterListaInvitaciones extends RecyclerView.Adapter<AdapterListaInvitaciones.ViewHolder>{

    public interface InvitacionImp{
        void clickAceptar(Invitacion invitacion);
        void clickCancelar(Invitacion invitacion);
    }

    private ArrayList<Invitacion> invitaciones;
    private InvitacionImp invitacionImp;

    public AdapterListaInvitaciones(ArrayList<Invitacion> invitaciones, InvitacionImp invitacionImp){
        this.invitaciones = invitaciones;
        this.invitacionImp = invitacionImp;
    }

    @NonNull
    @Override
    public AdapterListaInvitaciones.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invitacion, parent, false);

        return new AdapterListaInvitaciones.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterListaInvitaciones.ViewHolder holder, int position) {

        Invitacion invitacion = invitaciones.get(position);

        holder.text.setText(invitacion.getTitulo());
        holder.cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invitacionImp.clickCancelar(invitacion);
            }
        });

        holder.aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invitacionImp.clickAceptar(invitacion);
            }
        });
    }

    @Override
    public int getItemCount() {
        return invitaciones.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView text;
        public TextView aceptar;
        public ImageView cancelar;

        public ViewHolder(View view){
            super(view);

            this.text = view.findViewById(R.id.text_invitacion);
            this.aceptar = view.findViewById(R.id.btn_aceptar);
            this.cancelar = view.findViewById(R.id.btn_cancelar);
        }
    }
}
