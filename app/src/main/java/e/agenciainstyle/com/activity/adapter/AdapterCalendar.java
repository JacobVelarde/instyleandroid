package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import e.agenciainstyle.com.R;

public class AdapterCalendar extends ArrayAdapter<Date> {

    private LayoutInflater inflater;
    private Calendar currentCalendar;
    private Context context;
    private DayCLick dayCLick;
    public ArrayList<Integer> daysWithEvents;

    public interface DayCLick{
        void dayClick(int day, int month, int year);
    }

    public AdapterCalendar(Context context, ArrayList<Date> days, Calendar currentDate, DayCLick dayCLick){
        super(context, R.layout.item_calendar_day, days);

        this.context = context;
        this.currentCalendar = currentDate;
        inflater = LayoutInflater.from(context);
        this.dayCLick = dayCLick;
        this.daysWithEvents = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Calendar calendar = Calendar.getInstance();
        Date date = getItem(position);
        calendar.setTime(date);

        int dayValue = calendar.get(Calendar.DAY_OF_MONTH);
        int displayMonth = calendar.get(Calendar.MONTH) + 1;
        int displayYear = calendar.get(Calendar.YEAR);

        int currentMonth = currentCalendar.get(Calendar.MONTH) + 1;
        int currentYear = currentCalendar.get(Calendar.YEAR);

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_calendar_day, parent, false);
        }

        TextView textView = convertView.findViewById(R.id.text_num_day);
        View viewEvent = convertView.findViewById(R.id.view_event);

        if(displayMonth == currentMonth && displayYear == currentYear) {
            textView.setText(String.valueOf(dayValue));

            //TODO VALIDACION PARA MARCAR EL DIA EN CURSO
            /*Calendar calendarToday = Calendar.getInstance();
            int dayToday = calendarToday.get(Calendar.DAY_OF_MONTH);
            int monthToday = calendarToday.get(Calendar.MONTH) + 1;
            int yearToday = calendarToday.get(Calendar.YEAR);

            if (dayToday == dayValue && monthToday == currentMonth && yearToday == currentYear){
                textView.setTextColor(Color.WHITE);
                textView.setBackground(context.getResources().getDrawable(R.drawable.circular_color_green_dark));
                //viewEvent.setVisibility(View.VISIBLE);
            }else{
                textView.setTextColor(context.getResources().getColor(R.color.colorGrisPrimaryDark));
                textView.setBackgroundColor(context.getResources().getColor(R.color.colorBlanco));
                //viewEvent.setVisibility(View.VISIBLE);
            }*/

            if (!daysWithEvents.isEmpty()){
                for (int i = 0; i < daysWithEvents.size(); i++){
                    if (dayValue == daysWithEvents.get(i)){
                        viewEvent.setVisibility(View.VISIBLE);
                        setOnClick(convertView, true, dayValue, displayMonth, displayYear);
                        break;
                    }else{
                        viewEvent.setVisibility(View.INVISIBLE);
                        setOnClick(convertView, false, 0, 0 , 0);
                    }
                }
            }else{
                viewEvent.setVisibility(View.INVISIBLE);
                setOnClick(convertView, false, 0, 0 , 0);
            }

        }else{
            textView.setText("");
            viewEvent.setVisibility(View.INVISIBLE);
            convertView.setOnClickListener(null);
        }

        return convertView;
    }

    public void setEvents(ArrayList<Integer> daysWithEvents){
        this.daysWithEvents.clear();
        if (daysWithEvents != null){
            if (!daysWithEvents.isEmpty()){
                this.daysWithEvents.addAll(daysWithEvents);
            }
        }
        this.notifyDataSetInvalidated();
    }

    private void setOnClick(View view, boolean click, int dayValue, int displayMonth, int displayYear){
        if (click){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, "Tap en el día:" + String.valueOf(dayValue), Toast.LENGTH_SHORT).show();
                    dayCLick.dayClick(dayValue, displayMonth, displayYear);
                }
            });
        }else{
            view.setOnClickListener(null);
        }
    }
}
