package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.response.LoginResponse;
import e.agenciainstyle.com.presenter.LoginPresenter;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.StyleAnimation;
import e.agenciainstyle.com.tools.Tools;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.text_email)
    EditText textEmail;
    @BindView(R.id.text_contrasenia)
    EditText textContrasenia;
    @BindView(R.id.btn_recuperar_contrasenia)
    TextView btnRecuperarContrasenia;
    @BindView(R.id.btnAcceder)
    TextView btnAcceder;
    @BindView(R.id.btn_unete)
    TextView btnUnete;
    @BindView(R.id.layout_login)
    LinearLayout layoutLogin;
    @BindView(R.id.btnCliente)
    TextView btnCliente;
    @BindView(R.id.btnTalento)
    TextView btnTalento;
    @BindView(R.id.layout_opciones)
    LinearLayout layoutOpciones;

    private LoginPresenter presenter;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progress = new Progress(this);
        initPresenter();

    }

    @Override
    protected void onResume() {
        super.onResume();

        btnAcceder.setVisibility(View.VISIBLE);
        layoutLogin.setVisibility(View.VISIBLE);
        layoutOpciones.setVisibility(View.GONE);

        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validaFormulario()){
                    progress.show();
                    getToken();
                }
            }
        });
    }

    private void getToken(){
        Tools.getTokenFirebase(token -> {
            if (token.compareTo("ERROR") == 0){
                getToken();
            }else{
                Log.v("TOKEN_FIREBASE", token);
                presenter.login(textEmail.getText().toString(), textContrasenia.getText().toString(), token);
            }
        });
    }

    private boolean validaFormulario(){
        if (textEmail.getText().toString().length() <= 0){
            Mensaje mensaje  = new Mensaje(LoginActivity.this, "Ingresa un correo", false);
            mensaje.show();
            return false;
        }else if (textContrasenia.getText().toString().length() <= 0){
            Mensaje mensaje  = new Mensaje(LoginActivity.this, "Ingresa una contraseña", false);
            mensaje.show();
            return false;
        }else if (!Tools.isValidEmail(textEmail.getText())){
            Mensaje mensaje  = new Mensaje(LoginActivity.this, "Ingresa un email valido", false);
            mensaje.show();
            return false;
        }

        return true;
    }

    @OnClick({R.id.btn_recuperar_contrasenia, R.id.btn_unete, R.id.btnCliente, R.id.btnTalento})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_recuperar_contrasenia:

                if (textEmail.getText().toString().length() <= 0){
                    Mensaje mensaje = new Mensaje(this, "Ingresa el correo electrónico para reestablecer tu contraseña", false);
                    mensaje.show();
                }else{
                    progress.show();
                    presenter.recuperarContraseña(textEmail.getText().toString());
                }

                break;

            case R.id.btn_unete:
                StyleAnimation.fadeOut(this, btnAcceder, View.INVISIBLE);
                StyleAnimation.fadeOut(this, layoutLogin, View.GONE);
                StyleAnimation.fadeIn(this, layoutOpciones);
                break;
            case R.id.btnCliente:

                ActivityManager.next(this, FormularioUneteClienteActivity.class);

                break;
            case R.id.btnTalento:

                ActivityManager.next(this, FormularioUneteTalentoActivity.class);

                break;
        }
    }

    private void initPresenter(){
        presenter = new LoginPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {

                progress.dismiss();

                if (request == LoginPresenter.LOGIN){

                    LoginResponse loginResponse = (LoginResponse) response;
                    Preferences.saveObject(LoginActivity.this, Preferences.USER, loginResponse.getUser());

                    ActivityManager.next(LoginActivity.this, AreasDeInteresActivity.class);

                }else if (request == LoginPresenter.RECUPERAR_CONTRASENA){

                    GenericResponse genericResponse = (GenericResponse) response;

                    Mensaje mensaje = new Mensaje(LoginActivity.this, genericResponse.getMensaje(), true);
                    mensaje.show();

                }

            }

            @Override
            public void serverSuccess(int request) {
                //Toast.makeText(LoginActivity.this, loginResponse.getMensaje(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(LoginActivity.this, error, false);
                mensaje.show();

            }
        });
    }

}
