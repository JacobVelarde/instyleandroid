package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.response.NotificacionResponse;

public class AdapterNotificaciones extends BaseExpandableListAdapter {

    public interface OnClickChildItem{
        void click(String idNotification);
    }

    public NotificacionResponse notificaciones;
    public Context context;
    private OnClickChildItem onClickChildItem;

    public AdapterNotificaciones(NotificacionResponse notificaciones, Context context, OnClickChildItem onClickChildItem) {
        this.notificaciones = notificaciones;
        this.context = context;
        this.onClickChildItem = onClickChildItem;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return notificaciones.getObjecto().get(groupPosition).getNotificacion().getTitulo();
    }

    private String getIdNotificacion(int groupPosition){
        return notificaciones.getObjecto().get(groupPosition).getNotificacion().getId();
    }

    private String getStatusNotificacion(int groupPosition){
        return notificaciones.getObjecto().get(groupPosition).getNotificacion().getStatus();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String textChild = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_child_expandable_notification, null);
        }

        TextView textViewChild = (TextView) convertView.findViewById(R.id.text_child);
        textViewChild.setText(textChild);

        String estatus = getStatusNotificacion(groupPosition);

        if (estatus.compareTo("1") == 0){ //NOTIFICACION NO LEIDA
            textViewChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickChildItem.click(getIdNotificacion(groupPosition));
                }
            });
        }
        //2 NOTIFICACION LEIDA

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return notificaciones.getObjecto().get(groupPosition).getTitulo();
    }

    @Override
    public int getGroupCount() {
        if (notificaciones.getObjecto() != null){
            return notificaciones.getObjecto().size();
        }else{
            return 0;
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String listTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_header_expandable_notificacion, null);
        }

        TextView textViewGroup = (TextView) convertView.findViewById(R.id.header_title);
        textViewGroup.setText(listTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
