package e.agenciainstyle.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterTalentoDetalle;
import e.agenciainstyle.com.activity.adapter.AlertDeleteAddDreamTeam;
import e.agenciainstyle.com.activity.adapter.FiltroTalentoAlert;
import e.agenciainstyle.com.model.CoordinadorPanel;
import e.agenciainstyle.com.model.TalentoPanel;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.request.FiltraTalentoRequest;
import e.agenciainstyle.com.model.response.TalentoResponse;
import e.agenciainstyle.com.presenter.FiltroTalentosPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Tools;
import e.agenciainstyle.com.tools.Usuario;

public class FiltroTalentosActivity extends AppCompatActivity {

    @BindView(R.id.btn_menu_principal)
    ImageView btnMenuPrincipal;
    @BindView(R.id.btn_menu)
    ImageView btnMenu;
    @BindView(R.id.btn_agregar_talento)
    ImageView btnAgregarTalento;
    @BindView(R.id.btn_talentos)
    TextView btnTalentos;
    @BindView(R.id.btn_coordinadores)
    TextView btnCoordinadores;
    @BindView(R.id.btn_mujer)
    TextView btnMujer;
    @BindView(R.id.btn_hombre)
    TextView btnHombre;
    @BindView(R.id.btn_edad)
    TextView btnEdad;
    @BindView(R.id.btn_altura)
    TextView btnAltura;
    @BindView(R.id.btn_peso)
    TextView btnPeso;
    @BindView(R.id.btn_calzado)
    TextView btnCalzado;
    @BindView(R.id.btn_busto_camisa)
    TextView btnBustoCamisa;
    @BindView(R.id.btn_cintura_saco)
    TextView btnCinturaSaco;
    @BindView(R.id.btn_cadera_pantalon)
    TextView btnCaderaPantalon;
    @BindView(R.id.btn_ojos)
    TextView btnOjos;
    @BindView(R.id.btn_cabello)
    TextView btnCabello;
    @BindView(R.id.btn_buscar)
    TextView btnBuscar;
    @BindView(R.id.lista_talentos)
    GridView listaTalentos;
    @BindView(R.id.btn_atras)
    TextView btnAtras;
    @BindView(R.id.num_paginado)
    TextView numPaginado;
    @BindView(R.id.btn_siguiente)
    TextView btnSiguiente;
    @BindView(R.id.layout_top)
    LinearLayout layoutTop;
    @BindView(R.id.layout_middle)
    LinearLayout layoutMiddle;
    @BindView(R.id.layout_bottom)
    LinearLayout layoutBottom;

    private AdapterTalentoDetalle adapterTalentoDetalle;
    private ArrayList<TalentoPanel> talentoPanelArrayList;
    private ArrayList<CoordinadorPanel> coordinadorPanelArrayList;
    private FiltroTalentosPresenter filtroTalentosPresenter;
    private Progress progress;
    private boolean showHombre = true;
    private boolean showMujer = false;
    FiltraTalentoRequest filtraTalentoRequest;

    private boolean fromDreamTeam = false;
    private boolean allTalentos = false;

    private int rangoPanel = 9;
    private ArrayList<TalentoPanel> talentoPanelArrayListAux = new ArrayList<>();
    private ArrayList<CoordinadorPanel> coordinadorPanelArrayListAux = new ArrayList<>();
    private int numeroPagina = 1;
    private TalentoPanel talentoPanelSeleccionado;
    private User user;
    private boolean isCoordinador = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_talentos);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null){
            if (getIntent().getExtras().containsKey("onlyDreamTeam")){
                fromDreamTeam = true;
            }else if (getIntent().getExtras().containsKey("allTalentos")){
                allTalentos = true;
            }
        }

        filtraTalentoRequest = new FiltraTalentoRequest();
        filtraTalentoRequest.setGenero("Masculino"); //Default
        filtraTalentoRequest.setTipo("3");
        progress = new Progress(this);
        initPresenter();
        initAdapter();

        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);

        if (user.getPerfil().compareTo(Usuario.CLIENTE.getTipoCliente()) == 0 ){
            btnAgregarTalento.setVisibility(View.INVISIBLE);
        }
    }

    private void initPresenter() {
        filtroTalentosPresenter = new FiltroTalentosPresenter(this, new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();

                if (request == FiltroTalentosPresenter.ADD_TALENTO){
                    AlertDeleteAddDreamTeam alertDeleteAddDreamTeam = new AlertDeleteAddDreamTeam(FiltroTalentosActivity.this, talentoPanelSeleccionado);
                    alertDeleteAddDreamTeam.show();
                }else{
                    btnSiguiente.setVisibility(View.VISIBLE);
                    btnAtras.setVisibility(View.VISIBLE);
                    numPaginado.setText("1");
                    rangoPanel = 9;
                    if (request == FiltroTalentosPresenter.PRIMERA_CARGA) {
                        talentoPanelArrayList.clear();
                        talentoPanelArrayListAux.clear();
                        coordinadorPanelArrayList.clear();
                        coordinadorPanelArrayListAux.clear();
                        TalentoResponse talentoResponse = (TalentoResponse) response;
                        if (talentoResponse.getObjeto() != null) {
                            talentoPanelArrayListAux.addAll(talentoResponse.getObjeto().getTalentos());
                            coordinadorPanelArrayListAux.addAll(talentoResponse.getObjeto().getCoordinadores());
                            //talentoPanelArrayList.addAll(talentoResponse.getObjeto());
                            cargaMenuDiez(isCoordinador);
                            //adapterTalentoDetalle.notifyDataSetChanged();
                        }
                    } else if (request == FiltroTalentosPresenter.FILTRO) {
                        talentoPanelArrayList.clear();
                        talentoPanelArrayListAux.clear();

                        coordinadorPanelArrayList.clear();
                        coordinadorPanelArrayListAux.clear();

                        TalentoResponse talentoResponse = (TalentoResponse) response;
                        if (talentoResponse.getObjeto() != null) {
                            talentoPanelArrayListAux.addAll(talentoResponse.getObjeto().getTalentos());
                            coordinadorPanelArrayListAux.addAll(talentoResponse.getObjeto().getCoordinadores());
                            //talentoPanelArrayList.addAll(talentoResponse.getObjeto());
                            cargaMenuDiez(isCoordinador);
                            //adapterTalentoDetalle.notifyDataSetChanged();
                        }
                        adapterTalentoDetalle.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void serverSuccess(int request) {

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                btnSiguiente.setVisibility(View.VISIBLE);
                btnAtras.setVisibility(View.VISIBLE);
                Mensaje mensaje = new Mensaje(FiltroTalentosActivity.this, error, false);
                mensaje.show();
            }
        });

        User user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        progress.show();
        filtroTalentosPresenter.cargaImagenesPrincipales(user);

    }

    private void initAdapter() {
        talentoPanelArrayList = new ArrayList<>();
        coordinadorPanelArrayList = new ArrayList<>();
        adapterTalentoDetalle = new AdapterTalentoDetalle(this, talentoPanelArrayList, new AdapterTalentoDetalle.ClickTalento() {
            @Override
            public void detalleTalento(TalentoPanel talento) {

                if (!fromDreamTeam && !allTalentos){
                    Intent intent = new Intent(FiltroTalentosActivity.this, DetalleTalentoActivity.class);
                    intent.putExtra("id", talento.getUser_id());
                    startActivity(intent);
                }else if (fromDreamTeam){ //Selecciona solo su dream team y el perfil es cliente
                    //DELETE OR NOT DREAM TEAM
                    AlertDeleteAddDreamTeam alertDeleteAddDreamTeam = new AlertDeleteAddDreamTeam(FiltroTalentosActivity.this, talento);
                    alertDeleteAddDreamTeam.show();
                }else if (allTalentos){ //Selecciona de todos los dalentos y el perfil es cliente
                    //SHOW ALL TALENTOS
                    talentoPanelSeleccionado = talento;
                    filtroTalentosPresenter.agregaTalentoDreamTeam(user, talento);

                    /*AlertDeleteAddDreamTeam alertDeleteAddDreamTeam = new AlertDeleteAddDreamTeam(FiltroTalentosActivity.this, talento);
                    alertDeleteAddDreamTeam.show();*/
                }
            }
        });

        adapterTalentoDetalle.notifyDataSetChanged();

        listaTalentos.setAdapter(adapterTalentoDetalle);
    }

    public void cargaMenuDiez(boolean isCoordinador){
        talentoPanelArrayList.clear();
        coordinadorPanelArrayList.clear();

        if (isCoordinador){

            if (coordinadorPanelArrayListAux.size() == 0){
                adapterTalentoDetalle.notifyDataSetChanged();

            }else if(coordinadorPanelArrayListAux.size() >= 9){ //Aplica algoritmo
                if (rangoPanel == 9){
                    ArrayList<TalentoPanel> subLista = new ArrayList<>(coordinadorPanelArrayListAux.subList(0, rangoPanel));
                    talentoPanelArrayList.addAll(subLista);
                    adapterTalentoDetalle.notifyDataSetChanged();
                }else{
                    ArrayList<TalentoPanel> subLista;
                    if (coordinadorPanelArrayListAux.size() >= rangoPanel){
                        subLista = new ArrayList<>(coordinadorPanelArrayListAux.subList(rangoPanel - 9, rangoPanel));
                    }else{
                        subLista = new ArrayList<>(coordinadorPanelArrayListAux.subList(rangoPanel - 9, coordinadorPanelArrayListAux.size() - 1));
                    }
                    talentoPanelArrayList.addAll(subLista);
                    adapterTalentoDetalle.notifyDataSetChanged();
                }

            }else{
                talentoPanelArrayList.addAll(coordinadorPanelArrayListAux);
                adapterTalentoDetalle.notifyDataSetChanged();
            }
        }else{
            if (talentoPanelArrayListAux.size() == 0){
                adapterTalentoDetalle.notifyDataSetChanged();

            }else if(talentoPanelArrayListAux.size() >= 9){ //Aplica algoritmo
                if (rangoPanel == 9){
                    ArrayList<TalentoPanel> subLista = new ArrayList<>(talentoPanelArrayListAux.subList(0, rangoPanel));
                    talentoPanelArrayList.addAll(subLista);
                    adapterTalentoDetalle.notifyDataSetChanged();
                }else{
                    ArrayList<TalentoPanel> subLista;
                    if (talentoPanelArrayListAux.size() >= rangoPanel){
                        subLista = new ArrayList<>(talentoPanelArrayListAux.subList(rangoPanel - 9, rangoPanel));
                    }else{
                        subLista = new ArrayList<>(talentoPanelArrayListAux.subList(rangoPanel - 9, talentoPanelArrayListAux.size() - 1));
                    }
                    talentoPanelArrayList.addAll(subLista);
                    adapterTalentoDetalle.notifyDataSetChanged();
                }

            }else{
                talentoPanelArrayList.addAll(talentoPanelArrayListAux);
                adapterTalentoDetalle.notifyDataSetChanged();
            }
        }
    }

    @OnClick({R.id.btn_menu_principal, R.id.btn_menu, R.id.btn_agregar_talento, R.id.btn_talentos, R.id.btn_coordinadores, R.id.btn_mujer, R.id.btn_hombre, R.id.btn_edad, R.id.btn_altura, R.id.btn_peso, R.id.btn_calzado, R.id.btn_busto_camisa, R.id.btn_cintura_saco, R.id.btn_cadera_pantalon, R.id.btn_ojos, R.id.btn_cabello, R.id.btn_buscar, R.id.btn_atras, R.id.btn_siguiente})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_menu_principal:
            case R.id.btn_menu:
                onBackPressed();
                break;
            case R.id.btn_agregar_talento:
                ActivityManager.next(this, AgregarTalentoActivity.class);
                break;
            case R.id.btn_talentos:
                btnTalentos.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnTalentos.setTextColor(getResources().getColor(R.color.colorBlanco));

                btnCoordinadores.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnCoordinadores.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                layoutBottom.setVisibility(View.VISIBLE);
                layoutMiddle.setVisibility(View.VISIBLE);
                layoutTop.setVisibility(View.VISIBLE);

                filtraTalentoRequest = new FiltraTalentoRequest();
                filtraTalentoRequest.setTipo("3");

                isCoordinador = false;
                rangoPanel = 9;
                btnAtras.setVisibility(View.VISIBLE);
                btnSiguiente.setVisibility(View.VISIBLE);
                cargaMenuDiez(false);

                break;
            case R.id.btn_coordinadores:

                btnCoordinadores.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnCoordinadores.setTextColor(getResources().getColor(R.color.colorBlanco));

                btnTalentos.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnTalentos.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                layoutBottom.setVisibility(View.GONE);
                layoutMiddle.setVisibility(View.GONE);
                layoutTop.setVisibility(View.GONE);

                filtraTalentoRequest = new FiltraTalentoRequest();
                filtraTalentoRequest.setTipo("4");

                isCoordinador = true;
                rangoPanel = 9;
                btnAtras.setVisibility(View.VISIBLE);
                btnSiguiente.setVisibility(View.VISIBLE);
                cargaMenuDiez(true);

                break;
            case R.id.btn_mujer:
                showMujer = true;
                showHombre = false;
                btnMujer.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnMujer.setTextColor(getResources().getColor(R.color.colorBlanco));

                btnHombre.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnHombre.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                filtraTalentoRequest.setGenero("Femenino");

                break;
            case R.id.btn_hombre:
                showMujer = false;
                showHombre = true;
                btnMujer.setBackground(getResources().getDrawable(R.drawable.round_color_green));
                btnMujer.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

                btnHombre.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
                btnHombre.setTextColor(getResources().getColor(R.color.colorBlanco));

                filtraTalentoRequest.setGenero("Masculino");

                break;
            case R.id.btn_edad:
                ArrayList<String> edad = new ArrayList<>();
                for (int i = 18; i < 36; i++) {
                    edad.add(i + "");
                }
                showAlertFiltro("Edad", edad, FiltroTalentoAlert.TIPO_FILTRO.EDAD);
                break;
            case R.id.btn_altura:
                ArrayList<String> altura = new ArrayList<>();
                for (float i = 1.6f; i <= 2; i = i + 0.01f) {
                    altura.add(Tools.formatNumber(i, "#.##"));
                }
                showAlertFiltro("Altura", altura, FiltroTalentoAlert.TIPO_FILTRO.ALTURA);

                break;
            case R.id.btn_peso:
                ArrayList<String> peso = new ArrayList<>();
                for (int i = 45; i <= 110; i++) {
                    peso.add(i + "");
                }
                showAlertFiltro("Peso", peso, FiltroTalentoAlert.TIPO_FILTRO.PESO);

                break;
            case R.id.btn_calzado:
                ArrayList<String> calzado = new ArrayList<>();

                if (showHombre) {
                    for (int i = 22; i <= 31; i++) {
                        calzado.add(i + "");
                    }

                } else if (showMujer) {
                    for (int i = 22; i <= 27; i++) {
                        calzado.add(i + "");
                    }
                }

                showAlertFiltro("Calzado", calzado, FiltroTalentoAlert.TIPO_FILTRO.CALZADO);

                break;
            case R.id.btn_busto_camisa:
                ArrayList<String> busto_camisa = new ArrayList<>();
                for (float i = 0f; i <= 140; i = i + 0.01f) {
                    busto_camisa.add(Tools.formatNumber(i, "#.##"));
                }
                if (showHombre) {
                    showAlertFiltro("Camisa", busto_camisa, FiltroTalentoAlert.TIPO_FILTRO.CAMISA);
                } else if (showMujer) {
                    showAlertFiltro("Busto", busto_camisa, FiltroTalentoAlert.TIPO_FILTRO.BUSTO);
                }

                break;
            case R.id.btn_cintura_saco:
                ArrayList<String> cintura_saco = new ArrayList<>();
                for (float i = 0f; i <= 140; i = i + 0.01f) {
                    cintura_saco.add(Tools.formatNumber(i, "#.##"));
                }
                if (showHombre) {
                    showAlertFiltro("Saco", cintura_saco, FiltroTalentoAlert.TIPO_FILTRO.SACO);

                } else if (showMujer) {
                    showAlertFiltro("Cintura", cintura_saco, FiltroTalentoAlert.TIPO_FILTRO.CINTURA);
                }

                break;
            case R.id.btn_cadera_pantalon:

                ArrayList<String> cadera_pantalon = new ArrayList<>();
                for (float i = 0f; i <= 140; i = i + 0.01f) {
                    cadera_pantalon.add(Tools.formatNumber(i, "#.##"));
                }
                if (showHombre) {
                    showAlertFiltro("Pantalon", cadera_pantalon, FiltroTalentoAlert.TIPO_FILTRO.PANTALON);

                } else if (showMujer) {
                    showAlertFiltro("Cadera", cadera_pantalon, FiltroTalentoAlert.TIPO_FILTRO.CADERA);
                }

                break;
            case R.id.btn_ojos:
                ArrayList<String> opcionesOjos = new ArrayList<>();
                opcionesOjos.add("Negro");
                opcionesOjos.add("Café");
                opcionesOjos.add("Verde");
                opcionesOjos.add("Azul");
                opcionesOjos.add("Gris");
                showAlertFiltro("Ojos", opcionesOjos, FiltroTalentoAlert.TIPO_FILTRO.OJOS);

                break;
            case R.id.btn_cabello:
                ArrayList<String> opcionesCabello = new ArrayList<>();
                opcionesCabello.add("Negro");
                opcionesCabello.add("Castaño");
                opcionesCabello.add("Rubio");
                opcionesCabello.add("Pelirrojo");
                showAlertFiltro("Cabello", opcionesCabello, FiltroTalentoAlert.TIPO_FILTRO.CABELLO);
                break;
            case R.id.btn_buscar:

                progress.show();
                User user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
                if (showMujer){
                    filtraTalentoRequest.setGenero("Femenino");
                }else{
                    filtraTalentoRequest.setGenero("Masculino");
                }
                filtroTalentosPresenter.aplicarFiltro(filtraTalentoRequest, user);

                break;
            case R.id.btn_atras:
                if (numeroPagina != 1){
                    numeroPagina = numeroPagina - 1;
                    numPaginado.setText(numeroPagina+"");
                    rangoPanel = rangoPanel - 9;
                    cargaMenuDiez(isCoordinador);
                    btnSiguiente.setVisibility(View.VISIBLE);
                }else{
                    btnAtras.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.btn_siguiente:
                if (isCoordinador){
                    if ((coordinadorPanelArrayListAux.size() / rangoPanel) > 1){
                        numeroPagina = numeroPagina + 1;
                        numPaginado.setText(numeroPagina + "");
                        rangoPanel = rangoPanel + 9;
                        cargaMenuDiez(isCoordinador);
                        btnAtras.setVisibility(View.VISIBLE);
                    }else{
                        btnSiguiente.setVisibility(View.INVISIBLE);
                    }
                }else{
                    if ((talentoPanelArrayListAux.size() / rangoPanel) > 1){
                        numeroPagina = numeroPagina + 1;
                        numPaginado.setText(numeroPagina + "");
                        rangoPanel = rangoPanel + 9;
                        cargaMenuDiez(isCoordinador);
                        btnAtras.setVisibility(View.VISIBLE);
                    }else{
                        btnSiguiente.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }
    }

    private void showAlertFiltro(String titulo, ArrayList<String> datos, FiltroTalentoAlert.TIPO_FILTRO tipo_filtro) {
        FiltroTalentoAlert filtroTalentoAlert = new FiltroTalentoAlert(this, new FiltroTalentoAlert.AlertFiltroImp() {
            @Override
            public void onSelect(FiltroTalentoAlert.TIPO_FILTRO tipo_filtro, String opcion) {

                switch (tipo_filtro) {
                    case EDAD:
                        filtraTalentoRequest.setEdad(opcion);
                        btnEdad.setText(opcion);
                        break;

                    case ALTURA:
                        filtraTalentoRequest.setAltura_mts(opcion);
                        btnAltura.setText(opcion + "m");
                        break;

                    case PESO:
                        filtraTalentoRequest.setPeso_kg(opcion);
                        btnPeso.setText(opcion + "kg");
                        break;

                    case CALZADO:
                        filtraTalentoRequest.setCalzado(opcion);
                        btnCalzado.setText(opcion);
                        break;

                    case BUSTO:
                        filtraTalentoRequest.setPecho(opcion);
                        btnBustoCamisa.setText(opcion);
                        break;

                    case CAMISA:
                        filtraTalentoRequest.setCamisa(opcion);
                        btnBustoCamisa.setText(opcion);
                        break;

                    case CINTURA:
                        filtraTalentoRequest.setCintura(opcion);
                        btnCinturaSaco.setText(opcion);
                        break;

                    case SACO:
                        filtraTalentoRequest.setSaco(opcion);
                        btnCinturaSaco.setText(opcion);
                        break;

                    case CADERA:
                        filtraTalentoRequest.setCadera(opcion);
                        btnCaderaPantalon.setText(opcion);
                        break;

                    case PANTALON:
                        filtraTalentoRequest.setPantalon(opcion);
                        btnCaderaPantalon.setText(opcion);
                        break;

                    case OJOS:
                        filtraTalentoRequest.setOjos(opcion);
                        btnOjos.setText(opcion);
                        break;

                    case CABELLO:
                        filtraTalentoRequest.setCabello(opcion);
                        btnCabello.setText(opcion);
                        break;

                }
                //Toast.makeText(FiltroTalentosActivity.this, opcion, Toast.LENGTH_LONG).show();
            }
        });
        filtroTalentoAlert.setFiltros(datos, tipo_filtro);
        filtroTalentoAlert.setTitle(titulo);
        filtroTalentoAlert.show();
    }
}
