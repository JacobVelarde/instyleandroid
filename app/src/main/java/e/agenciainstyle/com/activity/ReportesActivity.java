package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterReportes;
import e.agenciainstyle.com.model.Reporte;
import e.agenciainstyle.com.model.TituloReporte;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.ReporteResponse;
import e.agenciainstyle.com.presenter.ReportesPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;

public class ReportesActivity extends AppCompatActivity {

    @BindView(R.id.btn_menu)
    TextView btnMenu;
    @BindView(R.id.btn_e)
    TextView btnE;
    @BindView(R.id.list_reportes)
    ExpandableListView listReportes;

    private AdapterReportes adapter;
    private ReporteResponse reporteResponse;
    private ReportesPresenter reportesPresenter;
    private Progress progress;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportes);
        ButterKnife.bind(this);

        reporteResponse = new ReporteResponse();

        adapter = new AdapterReportes(reporteResponse, this);
        listReportes.setAdapter(adapter);

        initPresenter();
    }

    @OnClick(R.id.btn_menu)
    public void onViewClicked() {
        onBackPressed();
    }

    private void initPresenter() {
        progress = new Progress(this);
        reportesPresenter = new ReportesPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();

                ArrayList<TituloReporte> reportes = new ArrayList<>();
                ReporteResponse reporteResponse = (ReporteResponse) response;
                for(int i = 0; i < reporteResponse.getObjeto().size(); i++){
                    Reporte reporte = reporteResponse.getObjeto().get(i).getReporte();
                    TituloReporte tituloReporte = new TituloReporte();
                    tituloReporte.setTitulo("Reporte");
                    Reporte reporteCustom = new Reporte();
                    reporteCustom.setId_evento(reporte.getId_evento());
                    reporteCustom.setNombre_evento(reporte.getNombre_evento());
                    reportes.add(tituloReporte);
                }

                reporteResponse.setObjeto(reportes);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                new Mensaje(ReportesActivity.this, error, false).show();
            }
        });

        progress.show();
        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        reportesPresenter.getReportes(user.getId(), user.getToken());

    }
}
