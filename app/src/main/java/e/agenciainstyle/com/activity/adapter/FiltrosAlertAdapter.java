package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Categoria;
import e.agenciainstyle.com.model.Talento;
import e.agenciainstyle.com.model.response.FiltrosResponse;

public class FiltrosAlertAdapter extends AlertDialog {

    public interface FiltrosAlertAdapterItem{
        void itemClick(Object object);
    }

    @BindView(R.id.spinner_opciones)
    Spinner spinnerOpciones;
    @BindView(R.id.lista_filtro)
    RecyclerView listaFiltro;

    private FiltrosResponse filtrosResponse;
    private LinearLayoutManager layoutManager;
    private FiltroAdapter filtroAdapter;
    private boolean filtroVisible = true;
    private ArrayAdapter<Categoria> adapterCategoriaModelos;
    private ArrayList<Categoria> categoriaModelos;
    private FiltrosAlertAdapterItem filtrosAlertAdapterItem;

    public FiltrosAlertAdapter(Context context, FiltrosResponse filtrosResponse, FiltrosAlertAdapterItem filtrosAlertAdapterItem) {
        super(context);
        this.filtrosResponse = filtrosResponse;
        layoutManager = new LinearLayoutManager(context);

        categoriaModelos = filtrosResponse.getCategorias();
        adapterCategoriaModelos = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, categoriaModelos);
        this.filtrosAlertAdapterItem = filtrosAlertAdapterItem;

        filtroAdapter = new FiltroAdapter(new ArrayList<>(), new FiltroAdapter.FiltroAdapterClick() {
            @Override
            public void itemClick(Object object) {
                filtrosAlertAdapterItem.itemClick(object);
                dismiss();
            }
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_filtros_adapter);
        ButterKnife.bind(this);


        listaFiltro.setLayoutManager(layoutManager);
        listaFiltro.setHasFixedSize(true);
        listaFiltro.setAdapter(filtroAdapter);

        spinnerOpciones.setAdapter(adapterCategoriaModelos);

        if (filtroVisible){
            spinnerOpciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Categoria categoria = categoriaModelos.get(position);

                    ArrayList<Talento> talentos = new ArrayList<>();

                    for (Talento talento : filtrosResponse.getTalentos()){
                        if (talento.getCategoriaId().compareTo(categoria.getId()) == 0){
                            talentos.add(talento);
                        }
                    }

                    filtroAdapter.setFiltro(talentos);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    public void filtroVisible(boolean visible){
        filtroVisible = visible;
    }

    public void setTipoFiltro(FiltroAdapter.TIPO_FILTRO tipoFiltro){
        switch (tipoFiltro){
            case CLIENTE:
                filtroAdapter.setFiltro(filtrosResponse.getClientes());
                break;

            case MARCA:
                filtroAdapter.setFiltro(filtrosResponse.getMarcas());
                break;

            case TALENTO:
                filtroAdapter.setFiltro(filtrosResponse.getTalentos());
                break;

            case BOOKER:
                filtroAdapter.setFiltro(filtrosResponse.getBookers());
                break;

            case COORDINADOR:
                filtroAdapter.setFiltro(filtrosResponse.getCoordinadores());
                break;

            case OPERADOR:
                filtroAdapter.setFiltro(filtrosResponse.getOperadores());
                break;

            case DINAMICA:
                filtroAdapter.setFiltro(filtrosResponse.getDinamica());
                break;

            case SEDE:
                filtroAdapter.setFiltro(filtrosResponse.getSede());
                break;

            case CATEGORIA:
                filtroAdapter.setFiltro(filtrosResponse.getCategorias());
                break;
        }
    }

    @Override
    public void show() {
        super.show();

        if (filtroVisible){
            spinnerOpciones.setVisibility(View.VISIBLE);
        }else{
            spinnerOpciones.setVisibility(View.INVISIBLE);
        }
    }
}
