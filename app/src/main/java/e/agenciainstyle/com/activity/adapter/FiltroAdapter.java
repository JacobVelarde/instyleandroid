package e.agenciainstyle.com.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Booker;
import e.agenciainstyle.com.model.Categoria;
import e.agenciainstyle.com.model.Cliente;
import e.agenciainstyle.com.model.Coordinador;
import e.agenciainstyle.com.model.Dinamica;
import e.agenciainstyle.com.model.Marca;
import e.agenciainstyle.com.model.Operador;
import e.agenciainstyle.com.model.Sede;
import e.agenciainstyle.com.model.Talento;

public class FiltroAdapter extends RecyclerView.Adapter<FiltroAdapter.ViewHolder> {

    public enum TIPO_FILTRO{
        CLIENTE,
        MARCA,
        TALENTO,
        BOOKER,
        COORDINADOR,
        OPERADOR,
        DINAMICA,
        SEDE,
        CATEGORIA
    }

    public interface FiltroAdapterClick{
        void itemClick(Object object);
    }

    private ArrayList<Object> filtro;
    private TIPO_FILTRO tipo_filtro;
    private FiltroAdapterClick filtroAdapterClick;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;

        public ViewHolder(View view){
            super(view);

            this.textView = view.findViewById(R.id.text_filtro);
        }
    }

    public FiltroAdapter(ArrayList<Object> filtro, FiltroAdapterClick filtroAdapterClick){
        this.filtro = filtro;
        this.filtroAdapterClick = filtroAdapterClick;
    }

    public void setFiltro(ArrayList<?> filtro){
        this.filtro.clear();
        this.filtro.addAll(filtro);

        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filtro, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Object object = filtro.get(position);

        String nombre = "";

        if (object instanceof Cliente){
            nombre = ((Cliente) object).getEmpresa();
        }else if (object instanceof Marca){
            nombre = ((Marca) object).getNombre();
        }else if (object instanceof Talento){
            Talento talento = (Talento) object;
            nombre = talento.getNombres() + " " + talento.getApaterno() + " " + talento.getAmaterno();
        }else if (object instanceof Booker){
            Booker booker = (Booker) object;
            nombre = booker.getNombres() + " " + booker.getApaterno() + " " + booker.getAmaterno();
        }else if (object instanceof Coordinador){
            Coordinador coordinador = (Coordinador) object;
            nombre = coordinador.getNombres() + " " + coordinador.getApaterno() + " " + coordinador.getAmaterno();
        }else if (object instanceof Operador){
            Operador operador = (Operador) object;
            nombre = operador.getOperador();
        }else if (object instanceof Dinamica){
            nombre = ((Dinamica) object).getDescripcion();
        }else if (object instanceof Sede){
            nombre = ((Sede) object).getSede();
        }else if (object instanceof Categoria){
            nombre = ((Categoria) object).getNombre();
        }

        holder.textView.setText(nombre);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtroAdapterClick.itemClick(object);
            }
        });

    }

    @Override
    public int getItemCount() {
        return filtro.size();
    }
}
