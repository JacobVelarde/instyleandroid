package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.TalentoPanel;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Usuario;

public class AdapterTalentoDetalle extends ArrayAdapter<TalentoPanel> {

    public interface ClickTalento{
        void detalleTalento(TalentoPanel talentoPanel);
    }

    private LayoutInflater inflater;
    private ClickTalento clickTalento;
    private User user;

    public AdapterTalentoDetalle(Context context, ArrayList<TalentoPanel> talentos, ClickTalento clickTalento){
        super(context, R.layout.item_talento_detalle, talentos);

        this.clickTalento = clickTalento;
        this.inflater = LayoutInflater.from(context);

        user = (User) Preferences.getObjectWithKey(context, Preferences.USER, User.class);

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        TalentoPanel talento = getItem(position);

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_talento_detalle, parent, false);
        }

        CircleImageView image = convertView.findViewById(R.id.image_foto);
        TextView textNombre = convertView.findViewById(R.id.text_nombre);
        ImageButton imageStar = convertView.findViewById(R.id.img_star);

        if (talento.getUrl_perfil() != null){
            Glide.with(convertView)
                    .load(talento.getUrl_perfil().trim())
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .into(image);
        }

        textNombre.setText(talento.getNombres() + "\n" + talento.getApellidos());

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTalento.detalleTalento(talento);
            }
        });

        if (user.getPerfil().compareTo(Usuario.CLIENTE.getTipoCliente()) == 0){
            if (talento.getEn_dreamteam().compareTo("0") == 0){
                imageStar.setVisibility(View.VISIBLE);
            }else{
                imageStar.setVisibility(View.INVISIBLE);
            }
        }else{
            imageStar.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }
}
