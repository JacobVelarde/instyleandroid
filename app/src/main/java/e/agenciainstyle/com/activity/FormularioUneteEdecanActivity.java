package e.agenciainstyle.com.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Multimedia;
import e.agenciainstyle.com.model.PrimeraVez;
import e.agenciainstyle.com.model.request.EdecanRequest;
import e.agenciainstyle.com.model.response.PrimeraVezResponse;
import e.agenciainstyle.com.presenter.FormularioEdecanPresenter;
import e.agenciainstyle.com.server.GenericResponse;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.PermisosManager;
import e.agenciainstyle.com.tools.Progress;
import e.agenciainstyle.com.tools.Tools;

public class FormularioUneteEdecanActivity extends AppCompatActivity implements PermisosManager.PermisosManagerListener {

    @BindView(R.id.btn_mujer)
    TextView btnMujer;
    @BindView(R.id.btn_hombre)
    TextView btnHombre;
    @BindView(R.id.seek_bar_edad)
    SeekBar seekBarEdad;
    @BindView(R.id.layout_edad)
    LinearLayout layoutEdad;
    @BindView(R.id.seek_bar_altura)
    SeekBar seekBarAltura;
    @BindView(R.id.layout_altura)
    LinearLayout layoutAltura;
    @BindView(R.id.seek_bar_busto)
    SeekBar seekBarBusto;
    @BindView(R.id.layout_busto)
    LinearLayout layoutBusto;
    @BindView(R.id.seek_bar_cintura)
    SeekBar seekBarCintura;
    @BindView(R.id.layout_cintura)
    LinearLayout layoutCintura;
    @BindView(R.id.seek_bar_cadera)
    SeekBar seekBarCadera;
    @BindView(R.id.layout_cadera)
    LinearLayout layoutCadera;
    @BindView(R.id.seek_bar_peso)
    SeekBar seekBarPeso;
    @BindView(R.id.layout_peso)
    LinearLayout layoutPeso;
    @BindView(R.id.seek_bar_calzado)
    SeekBar seekBarCalzado;
    @BindView(R.id.layout_calzado)
    LinearLayout layoutCalzado;
    @BindView(R.id.btn_modelo)
    TextView btnModelo;
    @BindView(R.id.btn_edecan)
    TextView btnEdecan;
    @BindView(R.id.btn_actriz)
    TextView btnActriz;
    @BindView(R.id.btn_imagen)
    TextView btnImagen;
    @BindView(R.id.btn_promotora)
    TextView btnPromotora;
    @BindView(R.id.btn_animadora)
    TextView btnAnimadora;
    @BindView(R.id.btn_conductora)
    TextView btnConductora;
    @BindView(R.id.btn_coordinadora)
    TextView btnCoordinadora;
    @BindView(R.id.btn_bailarina)
    TextView btnBailarina;
    @BindView(R.id.btn_influencer)
    TextView btnInfluencer;
    @BindView(R.id.layout_areas_de_despecialidad)
    LinearLayout layoutAreasDeDespecialidad;
    @BindView(R.id.image_foto_selfie_uno)
    CircleImageView imageFotoSelfieUno;
    @BindView(R.id.image_foto_selfie_dos)
    CircleImageView imageFotoSelfieDos;
    @BindView(R.id.image_foto_selfie_tres)
    CircleImageView imageFotoSelfieTres;
    @BindView(R.id.layout_selfies)
    LinearLayout layoutSelfies;
    @BindView(R.id.image_foto_book_uno)
    CircleImageView imageFotoBookUno;
    @BindView(R.id.image_foto_book_dos)
    CircleImageView imageFotoBookDos;
    @BindView(R.id.image_foto_book_tres)
    CircleImageView imageFotoBookTres;
    @BindView(R.id.image_foto_book_cuatro)
    CircleImageView imageFotoBookCuatro;
    @BindView(R.id.image_foto_book_cinco)
    CircleImageView imageFotoBookCinco;
    @BindView(R.id.image_foto_book_seis)
    CircleImageView imageFotoBookSeis;
    @BindView(R.id.image_foto_polaroid_uno)
    CircleImageView imageFotoPolaroidUno;
    @BindView(R.id.image_foto_polaroid_dos)
    CircleImageView imageFotoPolaroidDos;
    @BindView(R.id.image_foto_polaroid_tres)
    CircleImageView imageFotoPolaroidTres;
    @BindView(R.id.image_foto_polaroid_cuatro)
    CircleImageView imageFotoPolaroidCuatro;
    @BindView(R.id.image_foto_polaroid_cinco)
    CircleImageView imageFotoPolaroidCinco;
    @BindView(R.id.image_foto_polaroid_seis)
    CircleImageView imageFotoPolaroidSeis;
    @BindView(R.id.image_video_demo_reel)
    CircleImageView imageVideoDemoReel;
    @BindView(R.id.btn_adjuntar_seguro)
    TextView btnAdjuntarSeguro;
    @BindView(R.id.btn_adjuntar_ine)
    TextView btnAdjuntarIne;
    @BindView(R.id.btn_adjuntar_seguro_extranjero)
    TextView btnAdjuntarSeguroExtranjero;
    @BindView(R.id.btn_adjuntar_ine_extranjero)
    TextView btnAdjuntarIneExtranjero;
    @BindView(R.id.btn_guardar)
    TextView btnGuardar;
    @BindView(R.id.seek_bar_camisa)
    SeekBar seekBarCamisa;
    @BindView(R.id.layout_camisa)
    LinearLayout layoutCamisa;
    @BindView(R.id.seek_bar_saco)
    SeekBar seekBarSaco;
    @BindView(R.id.layout_saco)
    LinearLayout layoutSaco;
    @BindView(R.id.seek_bar_pantalon)
    SeekBar seekBarPantalon;
    @BindView(R.id.layout_pantalon)
    LinearLayout layoutPantalon;
    @BindView(R.id.spinner_ojos)
    Spinner spinnerOjos;
    @BindView(R.id.spinner_cabello)
    Spinner spinnerCabello;
    @BindView(R.id.text_edad)
    TextView textEdad;
    @BindView(R.id.text_altura)
    TextView textAltura;
    @BindView(R.id.text_altura_ingles)
    TextView textAlturaIngles;
    @BindView(R.id.text_camisa)
    TextView textCamisa;
    @BindView(R.id.text_camisa_ingles)
    TextView textCamisaIngles;
    @BindView(R.id.text_saco)
    TextView textSaco;
    @BindView(R.id.text_saco_ingles)
    TextView textSacoIngles;
    @BindView(R.id.text_pantalon)
    TextView textPantalon;
    @BindView(R.id.text_pantalon_ingles)
    TextView textPantalonIngles;
    @BindView(R.id.text_busto)
    TextView textBusto;
    @BindView(R.id.text_busto_ingles)
    TextView textBustoIngles;
    @BindView(R.id.text_cintura)
    TextView textCintura;
    @BindView(R.id.text_cintura_ingles)
    TextView textCinturaIngles;
    @BindView(R.id.text_cadera)
    TextView textCadera;
    @BindView(R.id.text_cadera_ingles)
    TextView textCaderaIngles;
    @BindView(R.id.text_peso)
    TextView textPeso;
    @BindView(R.id.text_peso_ingles)
    TextView textPesoIngles;
    @BindView(R.id.text_calzado)
    TextView textCalzado;
    @BindView(R.id.text_calzado_ingles)
    TextView textCalzadoIngles;
    @BindView(R.id.text_calzado_min_limite_ingles)
    TextView textCalzadoMinLimiteIngles;
    @BindView(R.id.text_calzado_min_limite)
    TextView textCalzadoMinLimite;
    @BindView(R.id.text_calzado_max_limite_ingles)
    TextView textCalzadoMaxLimiteIngles;
    @BindView(R.id.text_calzado_max_limite)
    TextView textCalzadoMaxLimite;

    private boolean statusModelo = false, statusEdecan = false, statusActriz = false,
            statusImagen = false, statusPromotora = false,
            statusAnimadora = false, statusConductora = false, statusCoordinadora = false,
            statusBailarina = false, statusInfluencer = false,
            statusMasculino = true, statusFemenino = false;

    private PermisosManager permisosManager;
    private ImageView imageViewSelected;
    private Uri imageUri;

    private int IMAGE_CAPTURE_CODE = 1001;

    private ArrayList<String> opcionesOjos;
    private ArrayList<String> opcionesCabello;

    private FormularioEdecanPresenter formularioEdecanPresenter;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_unete_edecan);
        ButterKnife.bind(this);

        progress = new Progress(this);

        opcionesOjos = new ArrayList<>();
        opcionesOjos.add("Selecciona");
        opcionesOjos.add("Negro");
        opcionesOjos.add("Café");
        opcionesOjos.add("Verde");
        opcionesOjos.add("Azul");
        opcionesOjos.add("Gris");

        opcionesCabello = new ArrayList<>();
        opcionesCabello.add("Cualquiera");
        opcionesCabello.add("Negro");
        opcionesCabello.add("Castaño");
        opcionesCabello.add("Rubio");
        opcionesCabello.add("Pelirrojo");

        ArrayAdapter<String> adapterOjos = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, opcionesOjos);
        ArrayAdapter<String> adapterCabello = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, opcionesCabello);

        spinnerCabello.setAdapter(adapterCabello);
        spinnerOjos.setAdapter(adapterOjos);

        permisosManager = new PermisosManager(this, this);
        initSeekBar();
        showFormularioSexo();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initEdecanPresenter();

            }
        }, 1000);

    }

    private void initEdecanPresenter() {
        formularioEdecanPresenter = new FormularioEdecanPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();

                if (request == FormularioEdecanPresenter.FORMULARIO_EDECAN){
                    GenericResponse genericResponse = (GenericResponse) response;
                    Mensaje mensaje = new Mensaje(FormularioUneteEdecanActivity.this, genericResponse.getMensaje(), false);
                    mensaje.setInterface(new Mensaje.ButtonAceptar() {
                        @Override
                        public void onClick() {
                            Toast.makeText(FormularioUneteEdecanActivity.this, "Dashboard de talento en construcción",Toast.LENGTH_LONG).show();
                        }
                    });
                    mensaje.show();
                }else if (request == FormularioEdecanPresenter.PRIMERA_VEZ){
                    if (response != null){
                        PrimeraVez primeraVez = ((PrimeraVezResponse) response).getObjeto();
                        if (primeraVez.getGenero().equals("Femenino")){

                            showFormularioMujer();

                            for (int i = 0; i < primeraVez.getMultimedia().size(); i++) {
                                Multimedia multimedia = primeraVez.getMultimedia().get(i);
                                if (multimedia.getCategoria().compareTo("selfie_perfil") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoSelfieDos);
                                }
                                if (multimedia.getCategoria().compareTo("book_1") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoBookUno);
                                }
                                if (multimedia.getCategoria().compareTo("book_2") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoBookDos);
                                }
                                if (multimedia.getCategoria().compareTo("book_3") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoBookTres);
                                }
                                if (multimedia.getCategoria().compareTo("book_4") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoBookCuatro);
                                }
                                if (multimedia.getCategoria().compareTo("book_5") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoBookCinco);
                                }
                                if (multimedia.getCategoria().compareTo("book_6") == 0) {
                                    Glide.with(FormularioUneteEdecanActivity.this)
                                            .load(multimedia.getUrl())
                                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                            .into(imageFotoBookSeis);
                                }
                            }


                        }else{

                            showFormularioHombre();

                        }
                    }else{
                        //DASHBOARD TALENTO
                    }
                }
            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(FormularioUneteEdecanActivity.this, error, false);
                mensaje.show();
            }
        }, FormularioUneteEdecanActivity.this);

        progress.show();
        formularioEdecanPresenter.getPrimerosDatos();
    }

    private void initSeekBar() {
        //TODO Para calcular el SeekBar
        //TODO https://stackoverflow.com/questions/20762001/how-to-set-seekbar-min-and-max-value

        int stepEdad = 1;
        int maxEdad = 35;
        int minEdad = 18;

        seekBarEdad.setMax((maxEdad - minEdad) / stepEdad);

        seekBarEdad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int value = minEdad + (progress * stepEdad);
                textEdad.setText("EDAD: " + value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepAltura = 0.01f;
        float maxAltura = 2;
        float minAltura = 1.6f;

        seekBarAltura.setMax(Math.round((maxAltura - minAltura) / stepAltura));

        seekBarAltura.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minAltura + (progress * stepAltura);
                textAltura.setText("ALTURA: " + Tools.formatNumber(value, "#.##") + " m");
                textAlturaIngles.setText(Tools.formatNumber(value * 3.28084, "#.##") + " ft");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepCamisa = 0.01f;
        float maxCamisa = 140;
        float minCamisa = 0;

        seekBarCamisa.setMax(Math.round((maxCamisa - minCamisa) / stepCamisa));

        seekBarCamisa.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCamisa + (progress * stepCamisa);
                textCamisa.setText("CAMISA: " + Tools.formatNumber(value, "#.##") + " cm");
                textCamisaIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepSaco = 0.01f;
        float maxSaco = 140;
        float minSaco = 0;

        seekBarSaco.setMax(Math.round((maxSaco - minSaco) / stepSaco));

        seekBarSaco.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCamisa + (progress * stepCamisa);
                textSaco.setText("SACO: " + Tools.formatNumber(value, "#.##" + " cm"));
                textSacoIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepPantalon = 0.01f;
        float maxPantalon = 140;
        float minPantalon = 0;

        seekBarPantalon.setMax(Math.round((maxPantalon - minPantalon) / stepPantalon));

        seekBarPantalon.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minPantalon + (progress * stepPantalon);
                textPantalon.setText("PANTALÓN: " + Tools.formatNumber(value, "#.##" + " cm"));
                textPantalonIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepBusto = 0.01f;
        float maxBusto = 140;
        float minBusto = 0;

        seekBarBusto.setMax(Math.round((maxBusto - minBusto) / stepBusto));

        seekBarBusto.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minPantalon + (progress * stepPantalon);
                textBusto.setText("BUSTO: " + Tools.formatNumber(value, "#.##" + " cm"));
                textBustoIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepCintura = 0.01f;
        float maxCintura = 140;
        float minCintura = 0;

        seekBarCintura.setMax(Math.round((maxCintura - minCintura) / stepCintura));

        seekBarCintura.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCintura + (progress * stepCintura);
                textCintura.setText("CINTURA: " + Tools.formatNumber(value, "#.##" + " cm"));
                textCinturaIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepCadera = 0.01f;
        float maxCadera = 140;
        float minCadera = 0;

        seekBarCadera.setMax(Math.round((maxCadera - minCadera) / stepCadera));

        seekBarCadera.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minCadera + (progress * stepCadera);
                textCadera.setText("CADERA: " + Tools.formatNumber(value, "#.##" + " cm"));
                textCaderaIngles.setText(Tools.formatNumber(value * 0.393701, "#.###") + " in");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        float stepPeso = 1;
        float maxPeso = 110;
        float minPeso = 45;

        seekBarPeso.setMax(Math.round((maxPeso - minPeso) / stepPeso));

        seekBarPeso.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = minPeso + (progress * stepPeso);
                textPeso.setText("PESO: " + Tools.formatNumber(value, "#.##" + " kg"));
                textPesoIngles.setText(Tools.formatNumber(value * 2.20462, "#.##") + " lb");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        initSeekBarCalzado(statusMasculino);

    }

    private void initSeekBarCalzado(boolean isHombre) {

        float stepCalzado = 0.5f;
        float maxCalzado = 0f;
        float minCalzado = 0f;

        if (isHombre) {

            maxCalzado = 31;
            minCalzado = 22;

            textCalzadoMinLimite.setText("22 cm");
            textCalzadoMaxLimite.setText("31 cm");

            textCalzadoMinLimiteIngles.setText("4 in");
            textCalzadoMaxLimiteIngles.setText("13 in");
        } else {

            maxCalzado = 27;
            minCalzado = 22;

            textCalzadoMinLimite.setText("22 cm");
            textCalzadoMaxLimite.setText("27 cm");

            textCalzadoMinLimiteIngles.setText("5 in");
            textCalzadoMaxLimiteIngles.setText("10 in");
        }


        seekBarCalzado.setMax(Math.round((maxCalzado - minCalzado) / stepCalzado));

        float finalMinCalzado = minCalzado;
        seekBarCalzado.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = finalMinCalzado + (progress * stepCalzado);
                textCalzado.setText("CALZADO: " + value + " cm");
                if (isHombre) {
                    textCalzadoIngles.setText(value - 18 + " in");
                } else {
                    textCalzadoIngles.setText(value - 17 + " in");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @OnClick({R.id.btn_mujer, R.id.btn_hombre, R.id.seek_bar_edad, R.id.layout_edad, R.id.seek_bar_altura, R.id.layout_altura, R.id.seek_bar_busto, R.id.layout_busto, R.id.seek_bar_cintura, R.id.layout_cintura, R.id.seek_bar_cadera, R.id.layout_cadera, R.id.seek_bar_peso, R.id.layout_peso, R.id.seek_bar_calzado, R.id.layout_calzado, R.id.btn_modelo, R.id.btn_edecan, R.id.btn_actriz, R.id.btn_imagen, R.id.btn_promotora, R.id.btn_animadora, R.id.btn_conductora, R.id.btn_coordinadora, R.id.btn_bailarina, R.id.btn_influencer, R.id.layout_areas_de_despecialidad, R.id.image_foto_selfie_uno, R.id.image_foto_selfie_dos, R.id.image_foto_selfie_tres, R.id.layout_selfies, R.id.image_foto_book_uno, R.id.image_foto_book_dos, R.id.image_foto_book_tres, R.id.image_foto_book_cuatro, R.id.image_foto_book_cinco, R.id.image_foto_book_seis, R.id.image_foto_polaroid_uno, R.id.image_foto_polaroid_dos, R.id.image_foto_polaroid_tres, R.id.image_foto_polaroid_cuatro, R.id.image_foto_polaroid_cinco, R.id.image_foto_polaroid_seis, R.id.image_video_demo_reel, R.id.btn_adjuntar_seguro, R.id.btn_adjuntar_ine, R.id.btn_adjuntar_seguro_extranjero, R.id.btn_adjuntar_ine_extranjero, R.id.btn_guardar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_mujer:

                showFormularioMujer();

                break;
            case R.id.btn_hombre:

                showFormularioHombre();

                break;
            case R.id.seek_bar_edad:
                break;
            case R.id.layout_edad:
                break;
            case R.id.seek_bar_altura:
                break;
            case R.id.layout_altura:
                break;
            case R.id.seek_bar_busto:
                break;
            case R.id.layout_busto:
                break;
            case R.id.seek_bar_cintura:
                break;
            case R.id.layout_cintura:
                break;
            case R.id.seek_bar_cadera:
                break;
            case R.id.layout_cadera:
                break;
            case R.id.seek_bar_peso:
                break;
            case R.id.layout_peso:
                break;
            case R.id.seek_bar_calzado:
                break;
            case R.id.layout_calzado:
                break;
            case R.id.btn_modelo:
                statusModelo = changeStatus(btnModelo, statusModelo);
                break;
            case R.id.btn_edecan:
                statusEdecan = changeStatus(btnEdecan, statusEdecan);
                break;
            case R.id.btn_actriz:
                statusActriz = changeStatus(btnActriz, statusActriz);
                break;
            case R.id.btn_imagen:
                statusImagen = changeStatus(btnImagen, statusImagen);
                break;
            case R.id.btn_promotora:
                statusPromotora = changeStatus(btnPromotora, statusPromotora);
                break;
            case R.id.btn_animadora:
                statusAnimadora = changeStatus(btnAnimadora, statusAnimadora);
                break;
            case R.id.btn_conductora:
                statusConductora = changeStatus(btnConductora, statusConductora);
                break;
            case R.id.btn_coordinadora:
                statusCoordinadora = changeStatus(btnCoordinadora, statusCoordinadora);
                break;
            case R.id.btn_bailarina:
                statusBailarina = changeStatus(btnBailarina, statusBailarina);
                break;
            case R.id.btn_influencer:
                statusInfluencer = changeStatus(btnInfluencer, statusInfluencer);
                break;
            case R.id.layout_areas_de_despecialidad:
                break;
            case R.id.image_foto_selfie_uno:
                openSelfie(imageFotoSelfieUno);
                break;
            case R.id.image_foto_selfie_dos:
                openSelfie(imageFotoSelfieDos);
                break;
            case R.id.image_foto_selfie_tres:
                openSelfie(imageFotoSelfieTres);
                break;
            case R.id.layout_selfies:
                break;
            case R.id.image_foto_book_uno:
                openSelfie(imageFotoBookUno);
                break;
            case R.id.image_foto_book_dos:
                openSelfie(imageFotoBookDos);
                break;
            case R.id.image_foto_book_tres:
                openSelfie(imageFotoBookTres);
                break;
            case R.id.image_foto_book_cuatro:
                openSelfie(imageFotoBookCuatro);
                break;
            case R.id.image_foto_book_cinco:
                openSelfie(imageFotoBookCinco);
                break;
            case R.id.image_foto_book_seis:
                openSelfie(imageFotoBookSeis);
                break;
            case R.id.image_foto_polaroid_uno:
                openSelfie(imageFotoPolaroidUno);
                break;
            case R.id.image_foto_polaroid_dos:
                openSelfie(imageFotoPolaroidDos);
                break;
            case R.id.image_foto_polaroid_tres:
                openSelfie(imageFotoPolaroidTres);
                break;
            case R.id.image_foto_polaroid_cuatro:
                openSelfie(imageFotoPolaroidCuatro);
                break;
            case R.id.image_foto_polaroid_cinco:
                openSelfie(imageFotoPolaroidCinco);
                break;
            case R.id.image_foto_polaroid_seis:
                openSelfie(imageFotoPolaroidSeis);
                break;
            case R.id.image_video_demo_reel:
                break;
            case R.id.btn_adjuntar_seguro:
                break;
            case R.id.btn_adjuntar_ine:
                break;
            case R.id.btn_adjuntar_seguro_extranjero:
                break;
            case R.id.btn_adjuntar_ine_extranjero:
                break;
            case R.id.btn_guardar:
                progress.show();
                EdecanRequest edecanRequest = new EdecanRequest();

                String edadString = textEdad.getText().toString().replace("EDAD:","").replace("EDAD","").trim();
                edecanRequest.setEdad(edadString.compareTo("") == 0 ? 0 : Integer.parseInt(edadString));

                edecanRequest.setGenero(statusMasculino ? "Masculino" : "Femenino");

                String alturaString = textAltura.getText().toString().replace("ALTURA:","").replace("ALTURA","").replace("m","").trim();
                edecanRequest.setAltura_mts(alturaString.compareTo("") == 0 ? 0 : Double.parseDouble(alturaString));

                String pesoString = textPeso.getText().toString().replace("PESO:","").replace("PESO","").replace("kg","").trim();
                edecanRequest.setPesoKg(pesoString.compareTo("") == 0 ? 0 : Double.parseDouble(pesoString));

                String calzadoString = textCalzado.getText().toString().replace("CALZADO:", "").replace("CALZADO", "").replace("cm","").trim();
                edecanRequest.setCalzado(calzadoString.compareTo("") == 0 ? 0 : Double.parseDouble(calzadoString));

                edecanRequest.setOjos((String) spinnerOjos.getSelectedItem());
                edecanRequest.setCabello((String) spinnerCabello.getSelectedItem());
                edecanRequest.setNacionalidad(1);

                if (statusMasculino){
                    String sacoString = textSaco.getText().toString().replace("SACO:","").replace("SACO","").replace("cm","").trim();
                    edecanRequest.setSaco(sacoString.compareTo("") == 0 ? 0 : Double.parseDouble(sacoString));

                    String camisaString = textCamisa.getText().toString().replace("CAMISA:","").replace("CAMISA","").replace("cm","").trim();
                    edecanRequest.setCamisa(camisaString.compareTo("") == 0 ? 0 : Double.parseDouble(camisaString));

                    String pantalonString = textPantalon.getText().toString().replace("PANTALÓN:","").replace("PANTALÓN","").replace("cm","").trim();
                    edecanRequest.setPantalon(pantalonString.compareTo("") == 0 ? 0 : Double.parseDouble(pantalonString));
                }else{

                    String pechoString = textBusto.getText().toString().replace("BUSTO:","").replace("BUSTO","").replace("cm","").trim();
                    edecanRequest.setPecho(pechoString.compareTo("") == 0 ? 0 : Double.parseDouble(pesoString));

                    String cinturaString = textCintura.getText().toString().replace("CINTURA:","").replace("CINTURA","").replace("cm","").trim();
                    edecanRequest.setCintura(cinturaString.compareTo("") == 0 ? 0 : Double.parseDouble(cinturaString));

                    String caderaString = textCadera.getText().toString().replace("CADERA:","").replace("CADERA","").replace("cm","").trim();
                    edecanRequest.setCadera(caderaString.compareTo("") == 0 ? 0 : Double.parseDouble(caderaString));
                }

                edecanRequest.setModelo(statusModelo);
                edecanRequest.setEdecan(statusEdecan);
                edecanRequest.setActriz(statusActriz);
                edecanRequest.setImagen(statusImagen);
                edecanRequest.setPromotor(statusPromotora);
                edecanRequest.setAnimador(statusAnimadora);
                edecanRequest.setConductor(statusConductora);
                edecanRequest.setCoordinador(statusCoordinadora);
                edecanRequest.setBailarin(statusBailarina);
                edecanRequest.setInfluencer(statusInfluencer);

                formularioEdecanPresenter.save(edecanRequest);
                break;
        }
    }

    private void showFormularioMujer(){
        btnMujer.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
        btnMujer.setTextColor(getResources().getColor(R.color.colorBlanco));

        btnHombre.setBackground(getResources().getDrawable(R.drawable.round_color_green));
        btnHombre.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

        statusFemenino = true;
        statusMasculino = false;
        showFormularioSexo();
        initSeekBarCalzado(statusMasculino);
    }

    private void showFormularioHombre(){
        btnMujer.setBackground(getResources().getDrawable(R.drawable.round_color_green));
        btnMujer.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

        btnHombre.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
        btnHombre.setTextColor(getResources().getColor(R.color.colorBlanco));

        statusFemenino = false;
        statusMasculino = true;
        showFormularioSexo();
        initSeekBarCalzado(statusMasculino);
    }

    private boolean changeStatus(TextView buttonText, boolean status) {

        if (status) {
            buttonText.setBackground(getResources().getDrawable(R.drawable.round_color_green));
            buttonText.setTextColor(getResources().getColor(R.color.colorTurquesaAcent));

            return false;

        } else {
            buttonText.setBackground(getResources().getDrawable(R.drawable.background_color_green_dark));
            buttonText.setTextColor(getResources().getColor(R.color.colorBlanco));

            return true;
        }
    }

    private void openSelfie(ImageView view) {
        if (permisosManager.tienePermisosSelfie()) {

            imageViewSelected = view;

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            //cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);

        } else {
            permisosManager.solicitarPermisosSelfie();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (IMAGE_CAPTURE_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                imageViewSelected.setImageURI(imageUri);
            }
        }
    }

    private void showFormularioSexo() {
        if (statusMasculino) {
            layoutCamisa.setVisibility(View.VISIBLE);
            layoutSaco.setVisibility(View.VISIBLE);
            layoutPantalon.setVisibility(View.VISIBLE);

            layoutBusto.setVisibility(View.GONE);
            layoutCintura.setVisibility(View.GONE);
            layoutCadera.setVisibility(View.GONE);

            btnEdecan.setText("GIO");
            btnActriz.setText("ACTOR");
            btnPromotora.setText("PROMOTOR");
            btnAnimadora.setText("ANIMADOR");
            btnConductora.setText("CONDUCTOR");
            btnCoordinadora.setText("COORDINADOR");
            btnBailarina.setText("BAILARIN");

        } else if (statusFemenino) {
            layoutCamisa.setVisibility(View.GONE);
            layoutSaco.setVisibility(View.GONE);
            layoutPantalon.setVisibility(View.GONE);

            layoutBusto.setVisibility(View.VISIBLE);
            layoutCintura.setVisibility(View.VISIBLE);
            layoutCadera.setVisibility(View.VISIBLE);

            btnEdecan.setText("EDECAN");
            btnActriz.setText("ACRTÍZ");
            btnPromotora.setText("PROMOTORA");
            btnAnimadora.setText("ANIMADORA");
            btnConductora.setText("CONDUCTORA");
            btnCoordinadora.setText("COORDINADORA");
            btnBailarina.setText("BAILARINA");
        }
    }

    @Override
    public void permisosSuccess(int permiso) {

    }

    @Override
    public void permisosCancel(int permiso) {

    }
}
