package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import e.agenciainstyle.com.R;

public class CalendarAlertDialog extends AlertDialog {

    public interface CalendarDates{
        void datesSelected(Calendar first, Calendar second);
    }

    @BindView(R.id.calendar)
    DateRangeCalendarView calendarView;
    @BindView(R.id.btn_aceptar)
    TextView btnAceptar;

    private Context mContext;
    private CalendarDates calendarDates;

    public CalendarAlertDialog(Context context, CalendarDates calendarDates){
        super(context);
        this.mContext = context;
        this.calendarDates = calendarDates;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_alert_dialog);
        ButterKnife.bind(this);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarDates != null){
                    if (calendarView.getStartDate() != null && calendarView.getEndDate() != null){
                        calendarDates.datesSelected(calendarView.getStartDate(), calendarView.getEndDate());
                    }
                }

                dismiss();
            }
        });
    }
}
