package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.tools.ActivityManager;
import e.agenciainstyle.com.tools.Usuario;

public class IniciarUneteActivity extends AppCompatActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.btn_iniciar_sesion)
    TextView btnIniciarSesion;
    @BindView(R.id.btn_unete)
    TextView btnUnete;
    private Usuario tipoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_unete);
        ButterKnife.bind(this);

        tipoUsuario = (Usuario) getIntent().getExtras().get("tipoCliente");

    }

    @OnClick({R.id.btn_iniciar_sesion, R.id.btn_unete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_iniciar_sesion:

                ActivityManager.next(this, LoginActivity.class);

                break;
            case R.id.btn_unete:

                if (tipoUsuario == Usuario.CLIENTE){
                    ActivityManager.next(this, FormularioUneteClienteActivity.class);
                }else{
                    ActivityManager.next(this, FormularioUneteTalentoActivity.class);
                }

                break;
        }
    }
}
