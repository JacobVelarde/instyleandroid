package e.agenciainstyle.com.activity.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Talento;

public class AdapterTalentosDetalleEvento extends ArrayAdapter<Talento> {

    private LayoutInflater inflater;

    public AdapterTalentosDetalleEvento(Context context, ArrayList<Talento> talentos){
        super(context, R.layout.item_talento_evento, talentos);

        this.inflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Talento talento = getItem(position);

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_talento_evento, parent, false);
        }

        CircleImageView image = convertView.findViewById(R.id.image_foto);
        TextView textNombre = convertView.findViewById(R.id.text_nombre);

        if (talento.getImageProfile() != null){
            Glide.with(convertView)
                    .load(talento.getImageProfile().trim())
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .into(image);
        }

        textNombre.setText(talento.getNombres().concat(talento.getApellidos()));

        return convertView;
    }
}
