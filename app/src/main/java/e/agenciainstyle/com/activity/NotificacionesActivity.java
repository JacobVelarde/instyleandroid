package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterNotificaciones;
import e.agenciainstyle.com.model.NotificacionServicio;
import e.agenciainstyle.com.model.Notification;
import e.agenciainstyle.com.model.TituloNotificacion;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.NotificacionResponse;
import e.agenciainstyle.com.model.response.NotificacionServicioResponse;
import e.agenciainstyle.com.presenter.NotificacionesPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;

public class NotificacionesActivity extends AppCompatActivity implements AdapterNotificaciones.OnClickChildItem {

    @BindView(R.id.btn_close)
    TextView btnClose;
    @BindView(R.id.list_notificaciones)
    ExpandableListView listNotificaciones;
    private AdapterNotificaciones adapter;
    private NotificacionResponse notificacionResponse;

    private NotificacionesPresenter notificacionesPresenter;
    private Progress progress;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);
        ButterKnife.bind(this);

        notificacionResponse = new NotificacionResponse();
        adapter = new AdapterNotificaciones(notificacionResponse, this, this);
        listNotificaciones.setAdapter(adapter);

        initPresenter();
    }

    @OnClick(R.id.btn_close)
    public void onViewClicked() {
        super.onBackPressed();
    }

    private void initPresenter(){
        progress = new Progress(this);
        notificacionesPresenter = new NotificacionesPresenter(new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();

                NotificacionServicioResponse notificacionServicioResponse = (NotificacionServicioResponse) response;

                ArrayList<TituloNotificacion> notificaciones = new ArrayList<>();

                for (int i = 0; i < notificacionServicioResponse.getObjeto().getNotificaciones().size(); i++){
                    NotificacionServicio notificacionServicio = notificacionServicioResponse.getObjeto().getNotificaciones().get(i);

                    TituloNotificacion tituloNotificacion = new TituloNotificacion();
                    tituloNotificacion.setTitulo(notificacionServicio.getTitle());
                    Notification notification = new Notification();
                    notification.setId(notificacionServicio.getNotificacion_id());
                    notification.setTitulo(notificacionServicio.getBody());
                    notification.setStatus(notificacionServicio.getStatus());
                    tituloNotificacion.setNotificacion(notification);

                    notificaciones.add(tituloNotificacion);
                }

                notificacionResponse.setObjecto(notificaciones);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void serverSuccess(int request) {
                progress.dismiss();
            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(NotificacionesActivity.this, error, false);
                mensaje.show();
            }
        });
        progress.show();
        user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        notificacionesPresenter.getNotificaciones(user.getId(), user.getToken());
    }

    @Override
    public void click(String idNotification) {
        notificacionesPresenter.notificationRead(user.getId(), user.getToken(), idNotification);
    }
}
