package e.agenciainstyle.com.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import e.agenciainstyle.com.R;
import e.agenciainstyle.com.activity.adapter.AdapterTalentosDetalleEvento;
import e.agenciainstyle.com.model.Evento;
import e.agenciainstyle.com.model.Talento;
import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.model.response.DetalleEventoResponse;
import e.agenciainstyle.com.presenter.DetalleEventoPresenter;
import e.agenciainstyle.com.server.ServerImp;
import e.agenciainstyle.com.tools.Mensaje;
import e.agenciainstyle.com.tools.Preferences;
import e.agenciainstyle.com.tools.Progress;

public class DetalleEventoActivity extends AppCompatActivity {

    @BindView(R.id.btn_close)
    ImageView btnClose;
    @BindView(R.id.btn_editar)
    ImageView btnEditar;
    @BindView(R.id.text_nombre)
    EditText textNombre;
    @BindView(R.id.text_tipo_activacion)
    EditText textTipoActivacion;
    @BindView(R.id.text_cliente)
    EditText textCliente;
    @BindView(R.id.text_productor)
    EditText textProductor;
    @BindView(R.id.text_marca)
    EditText textMarca;
    @BindView(R.id.text_coordinadores)
    EditText textCoordinadores;
    @BindView(R.id.text_nombre_evento)
    TextView textNombreEvento;
    @BindView(R.id.text_lugar_evento)
    TextView textLugarEvento;
    @BindView(R.id.text_fecha_evento)
    TextView textFechaEvento;
    @BindView(R.id.text_hora_inicio_evento)
    TextView textHoraInicioEvento;
    @BindView(R.id.text_hora_fin_evento)
    TextView textHoraFinEvento;
    @BindView(R.id.grid_talentos)
    GridView gridTalentos;

    private String idEvento;
    private AdapterTalentosDetalleEvento adapterTalentosDetalleEvento;
    private ArrayList<Talento> talentos;
    private DetalleEventoPresenter detalleEventoPresenter;
    private Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_evento);
        ButterKnife.bind(this);

        progress = new Progress(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("idEvento")) {
                idEvento = bundle.getString("idEvento");
            }
        }


        initAdapterTalentos();
        initPresenterDetalle();
    }

    private void initPresenterDetalle() {
        detalleEventoPresenter = new DetalleEventoPresenter(this, new ServerImp() {
            @Override
            public void serverSuccess(Object response, int request) {
                progress.dismiss();
                DetalleEventoResponse detalleEventoResponse = (DetalleEventoResponse) response;
                Evento evento = detalleEventoResponse.getObjeto();

                textNombre.setText(evento.getNombre());
                textTipoActivacion.setText(evento.getTipo());
                textCliente.setText(evento.getCliente());
                textProductor.setText(evento.getProductor());
                textMarca.setText(evento.getMarca());

                textLugarEvento.setText(evento.getUbicacion());
                textFechaEvento.setText(evento.getFecha());
                textHoraInicioEvento.setText(evento.getHoraInicio());
                textHoraFinEvento.setText(evento.getHoraFinal());

                talentos.clear();
                talentos.addAll(evento.getTalentos());
                adapterTalentosDetalleEvento.notifyDataSetChanged();

            }

            @Override
            public void serverSuccess(int request) {

            }

            @Override
            public void serverError(String error, int request) {
                progress.dismiss();
                Mensaje mensaje = new Mensaje(DetalleEventoActivity.this, error, false);
                mensaje.show();
            }
        });


        User user = (User) Preferences.getObjectWithKey(this, Preferences.USER, User.class);
        progress.show();
        detalleEventoPresenter.getDetalle(user, idEvento);
    }

    private void initAdapterTalentos() {
        talentos = new ArrayList<>();
        adapterTalentosDetalleEvento = new AdapterTalentosDetalleEvento(this, talentos);

        //Talento talento = new Talento();
        //talento.setNombres("Jacob Velarde");
        //talento.setImageProfile("https://e.agenciainstyle.com/source/2019/09/12/santi_valverde.png");
        //talentos.add(talento);
        //talentos.add(talento);

        gridTalentos.setAdapter(adapterTalentosDetalleEvento);
    }

    @OnClick({R.id.btn_close, R.id.btn_editar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                onBackPressed();
                break;
            case R.id.btn_editar:
                break;
        }
    }
}
