package e.agenciainstyle.com.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Evento;

public class AdapterActivacionesDashBoard extends RecyclerView.Adapter<AdapterActivacionesDashBoard.ViewHolder> {

    public interface ClickItem{
        void click(Evento evento);
    }

    public interface ClickItemSelected{
        void clickSelected(Evento evento);
    }

    private ArrayList<Evento> activaciones;
    private ClickItem clickItem;
    private ClickItemSelected clickItemSelected;

    public AdapterActivacionesDashBoard(ArrayList<Evento> activaciones, ClickItem clickItem){
        this.activaciones = activaciones;
        this.clickItem = clickItem;
    }

    public void setInterfaceSelected(ClickItemSelected clickItemSelected){
        this.clickItemSelected = clickItemSelected;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activaciones_dashboard, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Evento evento = activaciones.get(position);

        holder.textView.setText(evento.getNombre());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItem.click(evento);
            }
        });

        holder.imageViewSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickItemSelected != null){
                    clickItemSelected.clickSelected(evento);
                }
            }
        });


        //SIRVE PARA SELECCIONAR EVENTOS
        if (clickItemSelected != null){
            holder.imageViewSelected.setVisibility(View.VISIBLE);

            if (evento.isSelected()){
                holder.imageViewSelected.setImageDrawable(ContextCompat.getDrawable(holder.imageViewSelected.getContext(), R.mipmap.camera));
            }else{
                holder.imageViewSelected.setImageDrawable(ContextCompat.getDrawable(holder.imageViewSelected.getContext(), R.mipmap.plus_green));
            }

        }else{
            holder.imageViewSelected.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return activaciones.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        public ImageView imageView;
        public ImageView imageViewSelected;

        public ViewHolder(View view){
            super(view);

            this.textView = view.findViewById(R.id.text);
            this.imageView = view.findViewById(R.id.image_view);
            this.imageViewSelected = view.findViewById(R.id.image_view_selected);

        }
    }
}
