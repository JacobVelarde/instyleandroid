package e.agenciainstyle.com.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.Evento;

public class AdapterDetalleEventoDia extends RecyclerView.Adapter<AdapterDetalleEventoDia.ViewHolder> {

    public interface ClickItem{
        void click(Evento evento);
    }

    private ArrayList<Evento> eventos;
    public int dia;
    private ClickItem clickItem;

    public AdapterDetalleEventoDia(ArrayList<Evento> eventos, int dia, ClickItem clickItem){
        this.eventos = eventos;
        this.clickItem = clickItem;
        this.dia = dia;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detalle_evento_dia, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Evento evento = eventos.get(position);

        holder.textDia.setText(String.valueOf(dia));
        holder.textNombreEvento.setText(evento.getNombre());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItem.click(evento);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textDia, textNombreEvento;

        public ViewHolder(View view){
            super(view);

            this.textDia = view.findViewById(R.id.text_dia);
            this.textNombreEvento = view.findViewById(R.id.text_nombre_evento);

        }
    }
}
