package e.agenciainstyle.com.activity.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import e.agenciainstyle.com.R;


public class FiltroTalentoAlert extends AlertDialog {

    public enum TIPO_FILTRO{
        EDAD,
        ALTURA,
        PESO,
        CALZADO,
        BUSTO,
        CAMISA,
        CINTURA,
        SACO,
        CADERA,
        PANTALON,
        OJOS,
        CABELLO
    }

    @BindView(R.id.text_titulo)
    TextView textTitulo;
    @BindView(R.id.lista_filtros)
    ListView listaFiltros;
    private String title;
    private Context context;
    private ArrayList<String> filtros;
    private TIPO_FILTRO tipo_filtro;
    private AlertFiltroImp alertFiltroImp;

    public FiltroTalentoAlert(Context context, AlertFiltroImp alertFiltroImp) {
        super(context);
        this.context = context;
        filtros = new ArrayList<>();
        this.alertFiltroImp = alertFiltroImp;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFiltros(ArrayList<String> filtros, TIPO_FILTRO tipo_filtro) {
        this.filtros = filtros;
        this.tipo_filtro = tipo_filtro;
    }

    public interface AlertFiltroImp{
        void onSelect(TIPO_FILTRO tipo_filtro, String opcion);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_filtro_talento);
        ButterKnife.bind(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.item_text_talento, filtros);

        listaFiltros.setAdapter(adapter);
        textTitulo.setText(title);

        setCancelable(true);

        listaFiltros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String opcion = filtros.get(position);

                alertFiltroImp.onSelect(tipo_filtro, opcion.toUpperCase());
                dismiss();
            }
        });
    }
}
