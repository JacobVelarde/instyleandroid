package e.agenciainstyle.com.activity.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import e.agenciainstyle.com.R;
import e.agenciainstyle.com.model.AreaInteres;

public class AdapterCarousel extends RecyclerView.Adapter<AdapterCarousel.ViewHolder> {

    public interface CarouselClickItem{
        void carouselClick(AreaInteres areaInteres);
    }

    private CarouselClickItem carouselClickItem;
    private ArrayList<AreaInteres> areaInteresArrayList;
    private boolean positionCero = true, positionUno = false;


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        public ImageView imageView;

        public ViewHolder(View view){
            super(view);

            this.textView = view.findViewById(R.id.textAdapter);
            this.imageView = view.findViewById(R.id.imageAdapter);

        }
    }

    public AdapterCarousel(ArrayList<AreaInteres> areaInteres, CarouselClickItem carouselClickItem){
        this.areaInteresArrayList = areaInteres;
        this.carouselClickItem = carouselClickItem;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_area_interes, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AreaInteres areaInteres;

        if (position <= 2){
            areaInteres = areaInteresArrayList.get(position);
        }else{
            if (positionCero){

                areaInteres = areaInteresArrayList.get(0);
                positionCero = false;
                positionUno = true;

            }else if (positionUno){

                areaInteres = areaInteresArrayList.get(1);
                positionUno = false;

            }else{

                areaInteres = areaInteresArrayList.get(2);
                positionCero = true;
            }
        }

        holder.textView.setText(Html.fromHtml(areaInteres.getText()));
        holder.imageView.setImageDrawable(holder.textView.getContext().getResources().getDrawable(areaInteres.getImage()));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carouselClickItem.carouselClick(areaInteres);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }


}
