package e.agenciainstyle.com.model.request;

import com.google.gson.annotations.SerializedName;

public class IdRequest {

    @SerializedName("user_id")
    private String userId;

    public IdRequest() {
    }

    public IdRequest(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
