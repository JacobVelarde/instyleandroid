package e.agenciainstyle.com.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import e.agenciainstyle.com.model.Evento;

public class ListaDetalleEvento {

    @SerializedName("listado")
    @Expose
    private List<Evento> listado = null;

    public List<Evento> getListado() {
return listado;
}

    public void setListado(List<Evento> listado) {
this.listado = listado;
}

}