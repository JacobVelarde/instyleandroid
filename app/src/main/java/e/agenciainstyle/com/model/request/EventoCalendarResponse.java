package e.agenciainstyle.com.model.request;

import e.agenciainstyle.com.model.Evento;
import e.agenciainstyle.com.server.GenericResponse;

public class EventoCalendarResponse extends GenericResponse {

    private Evento objeto;

    public EventoCalendarResponse(Evento objeto) {
        this.objeto = objeto;
    }

    public EventoCalendarResponse(int codigo, String mensaje, Evento objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public Evento getObjeto() {
        return objeto;
    }

    public void setObjeto(Evento objeto) {
        this.objeto = objeto;
    }
}
