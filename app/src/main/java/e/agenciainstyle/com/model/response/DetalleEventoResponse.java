package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.Evento;
import e.agenciainstyle.com.server.GenericResponse;

public class DetalleEventoResponse extends GenericResponse {

    private Evento objeto;

    public DetalleEventoResponse() {
    }

    public DetalleEventoResponse(Evento objeto) {
        this.objeto = objeto;
    }

    public DetalleEventoResponse(int codigo, String mensaje, Evento objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public Evento getObjeto() {
        return objeto;
    }

    public void setObjeto(Evento objeto) {
        this.objeto = objeto;
    }
}
