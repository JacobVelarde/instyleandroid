package e.agenciainstyle.com.model;

import java.util.ArrayList;

public class TalentoDetalle {

    private String user_id;
    private String genero;
    private String nacionalidad;
    private String nombres;
    private String apellidos;
    private String edad;
    private String altura_mts;
    private String peso_kg;
    private String calzado;
    private String ojos;
    private String cabello;
    private String h_saco;
    private String h_camisa;
    private String h_pantalon;
    private String m_pecho;
    private String m_cintura;
    private String m_cadera;

    private ArrayList<Multimedia> multimedia;

    public TalentoDetalle() {
    }

    public TalentoDetalle(String user_id, String genero, String nacionalidad, String nombres, String apellidos, String edad, String altura_mts, String peso_kg, String calzado, String ojos, String cabello, String h_saco, String h_camisa, String h_pantalon, String m_pecho, String m_cintura, String m_cadera, ArrayList<Multimedia> multimedia) {
        this.user_id = user_id;
        this.genero = genero;
        this.nacionalidad = nacionalidad;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.altura_mts = altura_mts;
        this.peso_kg = peso_kg;
        this.calzado = calzado;
        this.ojos = ojos;
        this.cabello = cabello;
        this.h_saco = h_saco;
        this.h_camisa = h_camisa;
        this.h_pantalon = h_pantalon;
        this.m_pecho = m_pecho;
        this.m_cintura = m_cintura;
        this.m_cadera = m_cadera;
        this.multimedia = multimedia;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getAltura_mts() {
        return altura_mts;
    }

    public void setAltura_mts(String altura_mts) {
        this.altura_mts = altura_mts;
    }

    public String getPeso_kg() {
        return peso_kg;
    }

    public void setPeso_kg(String peso_kg) {
        this.peso_kg = peso_kg;
    }

    public String getCalzado() {
        return calzado;
    }

    public void setCalzado(String calzado) {
        this.calzado = calzado;
    }

    public String getOjos() {
        return ojos;
    }

    public void setOjos(String ojos) {
        this.ojos = ojos;
    }

    public String getCabello() {
        return cabello;
    }

    public void setCabello(String cabello) {
        this.cabello = cabello;
    }

    public String getH_saco() {
        return h_saco;
    }

    public void setH_saco(String h_saco) {
        this.h_saco = h_saco;
    }

    public String getH_camisa() {
        return h_camisa;
    }

    public void setH_camisa(String h_camisa) {
        this.h_camisa = h_camisa;
    }

    public String getH_pantalon() {
        return h_pantalon;
    }

    public String getM_pecho() {
        return m_pecho;
    }

    public void setM_pecho(String m_pecho) {
        this.m_pecho = m_pecho;
    }

    public String getM_cintura() {
        return m_cintura;
    }

    public void setM_cintura(String m_cintura) {
        this.m_cintura = m_cintura;
    }

    public String getM_cadera() {
        return m_cadera;
    }

    public void setM_cadera(String m_cadera) {
        this.m_cadera = m_cadera;
    }

    public void setH_pantalon(String h_pantalon) {
        this.h_pantalon = h_pantalon;
    }

    public ArrayList<Multimedia> getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(ArrayList<Multimedia> multimedia) {
        this.multimedia = multimedia;
    }
}
