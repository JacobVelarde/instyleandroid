package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.DashBoardAdministrador;
import e.agenciainstyle.com.server.GenericResponse;

public class DashBoardAdministradorResponse extends GenericResponse {

    private DashBoardAdministrador objeto;

    public DashBoardAdministradorResponse(DashBoardAdministrador objeto) {
        this.objeto = objeto;
    }

    public DashBoardAdministradorResponse(int codigo, String mensaje, DashBoardAdministrador objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public DashBoardAdministrador getObjeto() {
        return objeto;
    }

    public void setObjeto(DashBoardAdministrador objeto) {
        this.objeto = objeto;
    }

    @Override
    public String toString() {
        return "DashBoardAdministradorResponse{" +
                "objeto=" + objeto +
                '}';
    }
}
