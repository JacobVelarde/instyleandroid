package e.agenciainstyle.com.model;

public class Operador {

    private String operador;

    public Operador(String operador) {
        this.operador = operador;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }
}
