package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.PrimeraVez;
import e.agenciainstyle.com.server.GenericResponse;

public class PrimeraVezResponse extends GenericResponse {

    private PrimeraVez objeto;

    public PrimeraVezResponse(PrimeraVez objeto) {
        this.objeto = objeto;
    }

    public PrimeraVezResponse(int codigo, String mensaje, PrimeraVez objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public PrimeraVez getObjeto() {
        return objeto;
    }

    public void setObjeto(PrimeraVez objeto) {
        this.objeto = objeto;
    }
}
