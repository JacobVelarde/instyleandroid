package e.agenciainstyle.com.model.request;

import com.google.gson.annotations.SerializedName;

public class AgregaTalentoRequest {

    @SerializedName("tipo_solicitud")
    private String tipoSolicitud;
    private String genero;
    private String nombres;
    private String apellidos;
    private String email;
    private String password;
    @SerializedName("fecha_nacimiento")
    private String fechaNacimiento;

    public AgregaTalentoRequest() {
    }

    public AgregaTalentoRequest(String tipoSolicitud, String genero, String nombres, String apellidos, String email, String password, String fechaNacimiento) {
        this.tipoSolicitud = tipoSolicitud;
        this.genero = genero;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.password = password;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}
