package e.agenciainstyle.com.model;

public class TituloNotificacion {

    private String titulo;
    private Notification notificacion;

    public TituloNotificacion() {
    }

    public TituloNotificacion(String titulo, Notification notificacion) {
        this.titulo = titulo;
        this.notificacion = notificacion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Notification getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Notification notificacion) {
        this.notificacion = notificacion;
    }
}
