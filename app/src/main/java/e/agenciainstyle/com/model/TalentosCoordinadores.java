package e.agenciainstyle.com.model;

import java.util.ArrayList;

public class TalentosCoordinadores {

    private ArrayList<TalentoPanel> talentos;
    private ArrayList<CoordinadorPanel> coordinadores;

    public TalentosCoordinadores() {
    }

    public TalentosCoordinadores(ArrayList<TalentoPanel> talentos, ArrayList<CoordinadorPanel> coordinadores) {
        this.talentos = talentos;
        this.coordinadores = coordinadores;
    }

    public ArrayList<TalentoPanel> getTalentos() {
        return talentos;
    }

    public void setTalentos(ArrayList<TalentoPanel> talentos) {
        this.talentos = talentos;
    }

    public ArrayList<CoordinadorPanel> getCoordinadores() {
        return coordinadores;
    }

    public void setCoordinadores(ArrayList<CoordinadorPanel> coordinadores) {
        this.coordinadores = coordinadores;
    }
}
