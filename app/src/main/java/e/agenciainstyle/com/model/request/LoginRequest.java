package e.agenciainstyle.com.model.request;

import com.google.gson.Gson;

import java.io.Serializable;

public class LoginRequest implements Serializable {

    private String username;
    private String password;
    private String imei;
    private String sistema;

    public LoginRequest() {
    }

    public LoginRequest(String username, String password, String imei, String sistema) {
        this.username = username;
        this.password = password;
        this.imei = imei;
        this.sistema = sistema;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    @Override
    public String toString() {
        return "LoginRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", imei='" + imei + '\'' +
                '}';
    }

    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
