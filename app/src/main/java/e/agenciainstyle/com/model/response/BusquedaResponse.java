package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.server.GenericResponse;

public class BusquedaResponse extends GenericResponse {

    private BusquedaResponseObject objeto;

    public BusquedaResponse(BusquedaResponseObject object) {
        this.objeto = object;
    }

    public BusquedaResponse(int codigo, String mensaje, BusquedaResponseObject object) {
        super(codigo, mensaje);
        this.objeto = object;
    }

    public BusquedaResponseObject getObject() {
        return objeto;
    }

    public void setObject(BusquedaResponseObject object) {
        this.objeto = object;
    }
}
