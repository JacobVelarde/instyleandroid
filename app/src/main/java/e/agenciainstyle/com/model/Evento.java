package e.agenciainstyle.com.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Evento {

    @SerializedName(value = "id", alternate = {"id_evento"})
    private String id;
    @SerializedName("nombre_evento")
    private String nombre;
    @SerializedName("tipo_evento")
    private String tipo;
    private String cliente;
    private String productor;
    private String marca;

    private String ubicacion;
    @SerializedName(value = "fecha_evento", alternate = {"fecha"})
    private String fecha;
    @SerializedName("hora_inicio")
    private String horaInicio;
    @SerializedName("hora_final")
    private String horaFinal;
    @SerializedName("total_talentos")
    private int totalTalentos;
    private ArrayList<Talento> talentos;
    private String duracion;
    @SerializedName("costo_coordinador")
    private String costoCoordinador;
    @SerializedName("total_evento")
    private String total;
    @SerializedName("status_evento")
    private String statusEvento;

    private ArrayList<Integer> dias;

    private boolean isSelected;

    public Evento() {
    }

    public Evento(String id, String nombre, String tipo, String cliente, String productor, String marca, String ubicacion, String fecha, String horaInicio, String horaFinal, int totalTalentos, ArrayList<Talento> talentos, String duracion, String costoCoordinador, String total, String statusEvento, ArrayList<Integer> dias) {
        this.id = id;
        this.nombre = nombre;
        this.tipo = tipo;
        this.cliente = cliente;
        this.productor = productor;
        this.marca = marca;
        this.ubicacion = ubicacion;
        this.fecha = fecha;
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.totalTalentos = totalTalentos;
        this.talentos = talentos;
        this.duracion = duracion;
        this.costoCoordinador = costoCoordinador;
        this.total = total;
        this.statusEvento = statusEvento;
        this.dias = dias;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    public int getTotalTalentos() {
        return totalTalentos;
    }

    public void setTotalTalentos(int totalTalentos) {
        this.totalTalentos = totalTalentos;
    }

    public ArrayList<Talento> getTalentos() {
        return talentos;
    }

    public void setTalentos(ArrayList<Talento> talentos) {
        this.talentos = talentos;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getCostoCoordinador() {
        return costoCoordinador;
    }

    public void setCostoCoordinador(String costoCoordinador) {
        this.costoCoordinador = costoCoordinador;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatusEvento() {
        return statusEvento;
    }

    public void setStatusEvento(String statusEvento) {
        this.statusEvento = statusEvento;
    }

    public ArrayList<Integer> getDias() {
        return dias;
    }

    public void setDias(ArrayList<Integer> dias) {
        this.dias = dias;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
