package e.agenciainstyle.com.model;

public class Coordinador {

    private int id;
    private String nombres;
    private String apaterno;
    private String amaterno;

    public Coordinador(int id, String nombres, String apaterno, String amaterno) {
        this.id = id;
        this.nombres = nombres;
        this.apaterno = apaterno;
        this.amaterno = amaterno;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }
}
