package e.agenciainstyle.com.model;

import java.io.Serializable;

public class Bandera implements Serializable {

    private String bandera;
    private String prefijo;
    private String nombre;

    public Bandera() {
    }

    public Bandera(String bandera, String prefijo, String nombre) {
        this.bandera = bandera;
        this.prefijo = prefijo;
        this.nombre = nombre;
    }

    public String getBandera() {
        return bandera;
    }

    public void setBandera(String bandera) {
        this.bandera = bandera;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Bandera{" +
                "bandera='" + bandera + '\'' +
                ", prefijo='" + prefijo + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
