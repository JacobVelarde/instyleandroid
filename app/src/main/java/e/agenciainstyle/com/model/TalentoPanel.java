package e.agenciainstyle.com.model;

public class TalentoPanel {

    private String user_id;
    private String nombres;
    private String apellidos;
    private String url_perfil;
    private String en_dreamteam;

    public TalentoPanel() {
    }

    public TalentoPanel(String user_id, String nombres, String apellidos, String url_perfil, String en_dreamteam) {
        this.user_id = user_id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.url_perfil = url_perfil;
        this.en_dreamteam = en_dreamteam;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUrl_perfil() {
        return url_perfil;
    }

    public void setUrl_perfil(String url_perfil) {
        this.url_perfil = url_perfil;
    }

    public String getEn_dreamteam() {
        return en_dreamteam;
    }

    public void setEn_dreamteam(String en_dreamteam) {
        this.en_dreamteam = en_dreamteam;
    }
}
