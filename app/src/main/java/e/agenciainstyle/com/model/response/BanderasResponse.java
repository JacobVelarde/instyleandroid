package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.Bandera;
import e.agenciainstyle.com.server.GenericResponse;

public class BanderasResponse extends GenericResponse {

    private ArrayList<Bandera> objeto;

    public BanderasResponse(int codigo, String mensaje, ArrayList<Bandera> objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public ArrayList<Bandera> getObjeto() {
        return objeto;
    }

    public void setObjeto(ArrayList<Bandera> objeto) {
        this.objeto = objeto;
    }

    @Override
    public String toString() {
        return "BanderasResponse{" +
                "objeto=" + objeto +
                '}';
    }
}
