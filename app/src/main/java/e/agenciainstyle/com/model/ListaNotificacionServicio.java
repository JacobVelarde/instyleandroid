package e.agenciainstyle.com.model;

import java.util.ArrayList;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class ListaNotificacionServicio {

    private String user_id;
    private ArrayList<NotificacionServicio> notificaciones;

    public ListaNotificacionServicio() {
    }

    public ListaNotificacionServicio(String user_id, ArrayList<NotificacionServicio> notificaciones) {
        this.user_id = user_id;
        this.notificaciones = notificaciones;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<NotificacionServicio> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(ArrayList<NotificacionServicio> notificaciones) {
        this.notificaciones = notificaciones;
    }
}
