package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.TalentosCoordinadores;
import e.agenciainstyle.com.server.GenericResponse;

public class TalentoResponse extends GenericResponse {

    TalentosCoordinadores objeto;

    public TalentoResponse(TalentosCoordinadores objeto) {
        this.objeto = objeto;
    }

    public TalentoResponse(int codigo, String mensaje, TalentosCoordinadores objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public TalentosCoordinadores getObjeto() {
        return objeto;
    }

    public void setObjeto(TalentosCoordinadores objeto) {
        this.objeto = objeto;
    }
}
