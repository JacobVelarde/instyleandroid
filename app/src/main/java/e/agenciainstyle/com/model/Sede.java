package e.agenciainstyle.com.model;

public class Sede {

    private String sede;

    public Sede(String sede) {
        this.sede = sede;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }
}
