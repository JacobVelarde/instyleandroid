package e.agenciainstyle.com.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("id_user")
    private String id;
    private String token;
    private String perfil;
    @SerializedName("primera_vez")
    private String primeraVez;

    public User() {
    }

    public User(String id, String token, String perfil, String primeraVez) {
        this.id = id;
        this.token = token;
        this.perfil = perfil;
        this.primeraVez = primeraVez;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getPrimeraVez() {
        return primeraVez;
    }

    public void setPrimeraVez(String primeraVez) {
        this.primeraVez = primeraVez;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", token='" + token + '\'' +
                ", perfil='" + perfil + '\'' +
                ", primeraVez='" + primeraVez + '\'' +
                '}';
    }
}
