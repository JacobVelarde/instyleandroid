package e.agenciainstyle.com.model;

import java.util.ArrayList;

import e.agenciainstyle.com.model.Multimedia;

public class PrimeraVez {

    private String genero;
    private ArrayList<Multimedia> multimedia;

    public PrimeraVez(String genero, ArrayList<Multimedia> multimedia) {
        this.genero = genero;
        this.multimedia = multimedia;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public ArrayList<Multimedia> getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(ArrayList<Multimedia> multimedia) {
        this.multimedia = multimedia;
    }
}
