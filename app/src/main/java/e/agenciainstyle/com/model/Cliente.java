package e.agenciainstyle.com.model;

import com.google.gson.annotations.SerializedName;

import e.agenciainstyle.com.model.request.UsuarioRequest;

public class Cliente extends UsuarioRequest {

    private String id;
    private String empresa;
    private String direccion;
    private String cargo;
    private String telefono;
    @SerializedName("url_linked")
    private String urlLinkedIn;
    @SerializedName("url_website")
    private String urlWebSite;
    @SerializedName("url_instagram")
    private String urlInstagram;

    public Cliente() {
    }

    public Cliente(String id, String empresa, String direccion, String cargo, String telefono, String urlLinkedIn, String urlWebSite, String urlInstagram) {
        this.id = id;
        this.empresa = empresa;
        this.direccion = direccion;
        this.cargo = cargo;
        this.telefono = telefono;
        this.urlLinkedIn = urlLinkedIn;
        this.urlWebSite = urlWebSite;
        this.urlInstagram = urlInstagram;
    }

    public Cliente(String tipoSolicitud, String nombres, String apellidos, String email, String password, String id, String empresa, String direccion, String cargo, String telefono, String urlLinkedIn, String urlWebSite, String urlInstagram) {
        super(tipoSolicitud, nombres, apellidos, email, password);
        this.id = id;
        this.empresa = empresa;
        this.direccion = direccion;
        this.cargo = cargo;
        this.telefono = telefono;
        this.urlLinkedIn = urlLinkedIn;
        this.urlWebSite = urlWebSite;
        this.urlInstagram = urlInstagram;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrlLinkedIn() {
        return urlLinkedIn;
    }

    public void setUrlLinkedIn(String urlLinkedIn) {
        this.urlLinkedIn = urlLinkedIn;
    }

    public String getUrlWebSite() {
        return urlWebSite;
    }

    public void setUrlWebSite(String urlWebSite) {
        this.urlWebSite = urlWebSite;
    }

    public String getUrlInstagram() {
        return urlInstagram;
    }

    public void setUrlInstagram(String urlInstagram) {
        this.urlInstagram = urlInstagram;
    }
}
