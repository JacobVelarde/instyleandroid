package e.agenciainstyle.com.model;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class MenuPrincipalEstatus {

    private String user_id;
    private int flag_pendientes;
    private int flag_btnpanico;

    public MenuPrincipalEstatus(String user_id, int flag_pendientes, int flag_btnpanico) {
        this.user_id = user_id;
        this.flag_pendientes = flag_pendientes;
        this.flag_btnpanico = flag_btnpanico;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getFlag_pendientes() {
        return flag_pendientes;
    }

    public void setFlag_pendientes(int flag_pendientes) {
        this.flag_pendientes = flag_pendientes;
    }

    public int getFlag_btnpanico() {
        return flag_btnpanico;
    }

    public void setFlag_btnpanico(int flag_btnpanico) {
        this.flag_btnpanico = flag_btnpanico;
    }
}
