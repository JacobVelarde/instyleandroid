package e.agenciainstyle.com.model.response;

import com.google.gson.annotations.SerializedName;

import e.agenciainstyle.com.server.GenericResponse;

public class FotosUneteTalentoResponse extends GenericResponse {

    private IdSolicitud objeto;

    public IdSolicitud getObjeto() {
        return objeto;
    }

    public void setObjeto(IdSolicitud objeto) {
        this.objeto = objeto;
    }

    public static class IdSolicitud{
        @SerializedName("id_solicitud")
        private int idSolicitud;

        public int getIdSolicitud() {
            return idSolicitud;
        }

        public void setIdSolicitud(int idSolicitud) {
            this.idSolicitud = idSolicitud;
        }
    }

}
