package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.ListaNotificacionServicio;
import e.agenciainstyle.com.server.GenericResponse;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class NotificacionServicioResponse extends GenericResponse {

    private ListaNotificacionServicio objeto;

    public NotificacionServicioResponse(ListaNotificacionServicio objeto) {
        this.objeto = objeto;
    }

    public NotificacionServicioResponse(int codigo, String mensaje, ListaNotificacionServicio objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public ListaNotificacionServicio getObjeto() {
        return objeto;
    }

    public void setObjeto(ListaNotificacionServicio objeto) {
        this.objeto = objeto;
    }
}
