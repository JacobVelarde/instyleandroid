package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.TituloNotificacion;
import e.agenciainstyle.com.server.GenericResponse;

public class NotificacionResponse extends GenericResponse {

    private ArrayList<TituloNotificacion> objecto;

    public NotificacionResponse() {
    }

    public NotificacionResponse(ArrayList<TituloNotificacion> objecto) {
        this.objecto = objecto;
    }

    public ArrayList<TituloNotificacion> getObjecto() {
        return objecto;
    }

    public void setObjecto(ArrayList<TituloNotificacion> objecto) {
        this.objecto = objecto;
    }
}
