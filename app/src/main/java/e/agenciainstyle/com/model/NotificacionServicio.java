package e.agenciainstyle.com.model;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class NotificacionServicio {

    public String notificacion_id;
    public String title;
    public String body;
    public String status;

    public NotificacionServicio() {
    }

    public NotificacionServicio(String notificacion_id, String title, String body, String status) {
        this.notificacion_id = notificacion_id;
        this.title = title;
        this.body = body;
        this.status = status;
    }

    public String getNotificacion_id() {
        return notificacion_id;
    }

    public void setNotificacion_id(String notificacion_id) {
        this.notificacion_id = notificacion_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
