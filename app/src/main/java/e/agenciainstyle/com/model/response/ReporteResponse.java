package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.TituloReporte;
import e.agenciainstyle.com.server.GenericResponse;

public class ReporteResponse extends GenericResponse {

    ArrayList<TituloReporte> objeto;

    public ReporteResponse() {
    }

    public ReporteResponse(ArrayList<TituloReporte> objeto) {
        this.objeto = objeto;
    }

    public ReporteResponse(int codigo, String mensaje, ArrayList<TituloReporte> objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public ArrayList<TituloReporte> getObjeto() {
        return objeto;
    }

    public void setObjeto(ArrayList<TituloReporte> objeto) {
        this.objeto = objeto;
    }
}
