package e.agenciainstyle.com.model.request;

import com.google.gson.annotations.SerializedName;

public class FiltroActivacion {

    @SerializedName("pe")
    private String periodo;
    @SerializedName("se")
    private String sede;
    @SerializedName("ma")
    private int marca;
    @SerializedName("di")
    private int dinamica;
    @SerializedName("cl")
    private int cliente;
    @SerializedName("ta")
    private int talento;
    @SerializedName("bo")
    private int booker;
    @SerializedName("co")
    private int coordinador;
    @SerializedName("op")
    private String operador;
    @SerializedName("tac")
    private int categoriaTalento;

    public FiltroActivacion() {
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public int getMarca() {
        return marca;
    }

    public void setMarca(int marca) {
        this.marca = marca;
    }

    public int getDinamica() {
        return dinamica;
    }

    public void setDinamica(int dinamica) {
        this.dinamica = dinamica;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getTalento() {
        return talento;
    }

    public void setTalento(int talento) {
        this.talento = talento;
    }

    public int getBooker() {
        return booker;
    }

    public void setBooker(int booker) {
        this.booker = booker;
    }

    public int getCoordinador() {
        return coordinador;
    }

    public void setCoordinador(int coordinador) {
        this.coordinador = coordinador;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public int getCategoriaTalento() {
        return categoriaTalento;
    }

    public void setCategoriaTalento(int categoriaTalento) {
        this.categoriaTalento = categoriaTalento;
    }
}

