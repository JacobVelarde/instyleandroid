package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.Booker;
import e.agenciainstyle.com.model.Categoria;
import e.agenciainstyle.com.model.Cliente;
import e.agenciainstyle.com.model.Coordinador;
import e.agenciainstyle.com.model.Dinamica;
import e.agenciainstyle.com.model.Marca;
import e.agenciainstyle.com.model.Operador;
import e.agenciainstyle.com.model.Sede;
import e.agenciainstyle.com.model.Talento;

public class FiltrosResponse {

    private ArrayList<Cliente> clientes;
    private ArrayList<Marca> marcas;
    private ArrayList<Talento> talentos;
    private ArrayList<Booker> bookers;
    private ArrayList<Coordinador> coordinadores;
    private ArrayList<Operador> operadores;
    private ArrayList<Dinamica> dinamica;
    private ArrayList<Sede> sede;
    private ArrayList<Categoria> categorias;

    public FiltrosResponse() {
    }

    public FiltrosResponse(ArrayList<Cliente> clientes, ArrayList<Marca> marcas, ArrayList<Talento> talentos, ArrayList<Booker> bookers, ArrayList<Coordinador> coordinadores, ArrayList<Operador> operadores, ArrayList<Dinamica> dinamica, ArrayList<Sede> sede, ArrayList<Categoria> categorias) {
        this.clientes = clientes;
        this.marcas = marcas;
        this.talentos = talentos;
        this.bookers = bookers;
        this.coordinadores = coordinadores;
        this.operadores = operadores;
        this.dinamica = dinamica;
        this.sede = sede;
        this.categorias = categorias;
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }

    public ArrayList<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(ArrayList<Marca> marcas) {
        this.marcas = marcas;
    }

    public ArrayList<Talento> getTalentos() {
        return talentos;
    }

    public void setTalentos(ArrayList<Talento> talentos) {
        this.talentos = talentos;
    }

    public ArrayList<Booker> getBookers() {
        return bookers;
    }

    public void setBookers(ArrayList<Booker> bookers) {
        this.bookers = bookers;
    }

    public ArrayList<Coordinador> getCoordinadores() {
        return coordinadores;
    }

    public void setCoordinadores(ArrayList<Coordinador> coordinadores) {
        this.coordinadores = coordinadores;
    }

    public ArrayList<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(ArrayList<Operador> operadores) {
        this.operadores = operadores;
    }

    public ArrayList<Dinamica> getDinamica() {
        return dinamica;
    }

    public void setDinamicas(ArrayList<Dinamica> dinamicas) {
        this.dinamica = dinamicas;
    }

    public ArrayList<Sede> getSede() {
        return sede;
    }

    public void setSede(ArrayList<Sede> sede) {
        this.sede = sede;
    }

    public void setDinamica(ArrayList<Dinamica> dinamica) {
        this.dinamica = dinamica;
    }

    public ArrayList<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }
}
