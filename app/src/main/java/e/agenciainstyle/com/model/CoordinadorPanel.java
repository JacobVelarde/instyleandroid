package e.agenciainstyle.com.model;

public class CoordinadorPanel extends TalentoPanel {

    private boolean isCoordinador;

    public CoordinadorPanel(boolean isCoordinador) {
        this.isCoordinador = isCoordinador;
    }

    public CoordinadorPanel(String user_id, String nombres, String apellidos, String url_perfil, String en_dreamteam, boolean isCoordinador) {
        super(user_id, nombres, apellidos, url_perfil, en_dreamteam);
        this.isCoordinador = isCoordinador;
    }

    public boolean isCoordinador() {
        return isCoordinador;
    }

    public void setCoordinador(boolean coordinador) {
        isCoordinador = coordinador;
    }
}
