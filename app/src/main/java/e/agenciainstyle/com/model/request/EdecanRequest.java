package e.agenciainstyle.com.model.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EdecanRequest implements Serializable {

    private int edad;
    private String genero;
    @SerializedName("altura_mts")
    private double altura_mts;
    @SerializedName("peso_kg")
    private double pesoKg;
    private double calzado;
    private String ojos;
    private String cabello;
    private int nacionalidad;
    @SerializedName("h_saco")
    private double saco;
    @SerializedName("h_camisa")
    private double camisa;
    @SerializedName("h_pantalon")
    private double pantalon;
    @SerializedName("m_pecho")
    private double pecho;
    @SerializedName("m_cintura")
    private double cintura;
    @SerializedName("m_cadera")
    private double cadera;
    private boolean modelo;
    private boolean edecan;
    private boolean actriz;
    private boolean imagen;
    private boolean promotor;
    private boolean animador;
    private boolean conductor;
    private boolean coordinador;
    private boolean bailarin;
    private boolean influencer;

    public EdecanRequest() {
    }


    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public double getAltura_mts() {
        return altura_mts;
    }

    public void setAltura_mts(double altura_mts) {
        this.altura_mts = altura_mts;
    }

    public double getPesoKg() {
        return pesoKg;
    }

    public void setPesoKg(double pesoKg) {
        this.pesoKg = pesoKg;
    }

    public double getCalzado() {
        return calzado;
    }

    public void setCalzado(double calzado) {
        this.calzado = calzado;
    }

    public String getOjos() {
        return ojos;
    }

    public void setOjos(String ojos) {
        this.ojos = ojos;
    }

    public String getCabello() {
        return cabello;
    }

    public void setCabello(String cabello) {
        this.cabello = cabello;
    }

    public int getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(int nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public double getSaco() {
        return saco;
    }

    public void setSaco(double saco) {
        this.saco = saco;
    }

    public double getCamisa() {
        return camisa;
    }

    public void setCamisa(double camisa) {
        this.camisa = camisa;
    }

    public double getPantalon() {
        return pantalon;
    }

    public void setPantalon(double pantalon) {
        this.pantalon = pantalon;
    }

    public double getPecho() {
        return pecho;
    }

    public void setPecho(double pecho) {
        this.pecho = pecho;
    }

    public double getCintura() {
        return cintura;
    }

    public void setCintura(double cintura) {
        this.cintura = cintura;
    }

    public double getCadera() {
        return cadera;
    }

    public void setCadera(double cadera) {
        this.cadera = cadera;
    }

    public boolean isModelo() {
        return modelo;
    }

    public void setModelo(boolean modelo) {
        this.modelo = modelo;
    }

    public boolean isEdecan() {
        return edecan;
    }

    public void setEdecan(boolean edecan) {
        this.edecan = edecan;
    }

    public boolean isActriz() {
        return actriz;
    }

    public void setActriz(boolean actriz) {
        this.actriz = actriz;
    }

    public boolean isImagen() {
        return imagen;
    }

    public void setImagen(boolean imagen) {
        this.imagen = imagen;
    }

    public boolean isPromotor() {
        return promotor;
    }

    public void setPromotor(boolean promotor) {
        this.promotor = promotor;
    }

    public boolean isAnimador() {
        return animador;
    }

    public void setAnimador(boolean animador) {
        this.animador = animador;
    }

    public boolean isConductor() {
        return conductor;
    }

    public void setConductor(boolean conductor) {
        this.conductor = conductor;
    }

    public boolean isCoordinador() {
        return coordinador;
    }

    public void setCoordinador(boolean coordinador) {
        this.coordinador = coordinador;
    }

    public boolean isBailarin() {
        return bailarin;
    }

    public void setBailarin(boolean bailarin) {
        this.bailarin = bailarin;
    }

    public boolean isInfluencer() {
        return influencer;
    }

    public void setInfluencer(boolean influencer) {
        this.influencer = influencer;
    }
}
