package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.Perfil;
import e.agenciainstyle.com.server.GenericResponse;

/**
 * Created by Jacob Velarde on 06,September,2020
 */
public class PerfilResponse extends GenericResponse {

    private ArrayList<Perfil> objeto;

    public PerfilResponse(ArrayList<Perfil> objeto) {
        this.objeto = objeto;
    }

    public PerfilResponse(int codigo, String mensaje, ArrayList<Perfil> objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public ArrayList<Perfil> getObjeto() {
        return objeto;
    }

    public void setObjeto(ArrayList<Perfil> objeto) {
        this.objeto = objeto;
    }
}
