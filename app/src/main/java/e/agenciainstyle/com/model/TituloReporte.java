package e.agenciainstyle.com.model;

public class TituloReporte {

    private String titulo;
    private Reporte reporte;

    public TituloReporte() {
    }

    public TituloReporte(String titulo, Reporte reporte) {
        this.titulo = titulo;
        this.reporte = reporte;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Reporte getReporte() {
        return reporte;
    }

    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    }
}
