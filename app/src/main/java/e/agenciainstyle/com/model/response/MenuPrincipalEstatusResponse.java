package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.MenuPrincipalEstatus;
import e.agenciainstyle.com.server.GenericResponse;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class MenuPrincipalEstatusResponse extends GenericResponse {

    MenuPrincipalEstatus objeto;

    public MenuPrincipalEstatusResponse(MenuPrincipalEstatus objeto) {
        this.objeto = objeto;
    }

    public MenuPrincipalEstatusResponse(int codigo, String mensaje, MenuPrincipalEstatus objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public MenuPrincipalEstatus getObjeto() {
        return objeto;
    }

    public void setObjeto(MenuPrincipalEstatus objeto) {
        this.objeto = objeto;
    }
}
