package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.Reporte;

/**
 * Created by Jacob Velarde on 08,September,2020
 */
public class ListaReportesFavoritos {

    private ArrayList<Reporte> eventos_favoritos;

    public ListaReportesFavoritos(ArrayList<Reporte> eventos_favoritos) {
        this.eventos_favoritos = eventos_favoritos;
    }

    public ArrayList<Reporte> getEventos_favoritos() {
        return eventos_favoritos;
    }

    public void setEventos_favoritos(ArrayList<Reporte> eventos_favoritos) {
        this.eventos_favoritos = eventos_favoritos;
    }
}
