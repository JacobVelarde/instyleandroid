package e.agenciainstyle.com.model.request;

import com.google.gson.annotations.SerializedName;

public class UsuarioRequest {

    @SerializedName("tipo_solicitud")
    private String tipoSolicitud;
    private String nombres;
    private String apellidos;
    private String email;
    private String password;

    public UsuarioRequest() {
    }

    public UsuarioRequest(String tipoSolicitud, String nombres, String apellidos, String email, String password) {
        this.tipoSolicitud = tipoSolicitud;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.password = password;
    }

    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
