package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.server.GenericResponse;

/**
 * Created by Jacob Velarde on 08,September,2020
 */
public class ReportesResponse extends GenericResponse {

    private ListaReportesFavoritos objeto;

    public ReportesResponse(ListaReportesFavoritos objeto) {
        this.objeto = objeto;
    }

    public ReportesResponse(int codigo, String mensaje, ListaReportesFavoritos objeto) {
        super(codigo, mensaje);
        this.objeto = objeto;
    }

    public ListaReportesFavoritos getObjeto() {
        return objeto;
    }

    public void setObjeto(ListaReportesFavoritos objeto) {
        this.objeto = objeto;
    }
}
