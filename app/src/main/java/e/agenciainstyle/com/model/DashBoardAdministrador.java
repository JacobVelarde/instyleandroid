package e.agenciainstyle.com.model;

import java.util.ArrayList;

import e.agenciainstyle.com.server.GenericResponse;

public class DashBoardAdministrador extends GenericResponse {

    private int completadas;
    private int encurso;
    private int proximas;
    private int total;
    private ArrayList<Evento> listado;

    public DashBoardAdministrador() {
    }

    public DashBoardAdministrador(int codigo, String mensaje, int completadas, int encurso, int proximas, int total, ArrayList<Evento> listado) {
        super(codigo, mensaje);
        this.completadas = completadas;
        this.encurso = encurso;
        this.proximas = proximas;
        this.total = total;
        this.listado = listado;
    }

    public DashBoardAdministrador(int completadas, int encurso, int proximas, int total, ArrayList<Evento> listado) {
        this.completadas = completadas;
        this.encurso = encurso;
        this.proximas = proximas;
        this.total = total;
        this.listado = listado;
    }

    public int getCompletadas() {
        return completadas;
    }

    public void setCompletadas(int completadas) {
        this.completadas = completadas;
    }

    public int getEncurso() {
        return encurso;
    }

    public void setEncurso(int encurso) {
        this.encurso = encurso;
    }

    public int getProximas() {
        return proximas;
    }

    public void setProximas(int proximas) {
        this.proximas = proximas;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<Evento> getListado() {
        return listado;
    }

    public void setListado(ArrayList<Evento> listado) {
        this.listado = listado;
    }
}
