package e.agenciainstyle.com.model;

public class Notification {

    private String id;
    private String titulo;
    private String status;
    public Notification() {
    }

    public Notification(String id, String titulo, String status) {
        this.id = id;
        this.titulo = titulo;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
