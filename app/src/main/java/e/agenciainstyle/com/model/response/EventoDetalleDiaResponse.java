package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.server.GenericResponse;

public class EventoDetalleDiaResponse extends GenericResponse {

    private ListaDetalleEvento objeto;

    public EventoDetalleDiaResponse(ListaDetalleEvento objeto) {
        this.objeto = objeto;
    }

    public ListaDetalleEvento getObjeto() {
        return objeto;
    }

    public void setObjeto(ListaDetalleEvento objeto) {
        this.objeto = objeto;
    }
}
