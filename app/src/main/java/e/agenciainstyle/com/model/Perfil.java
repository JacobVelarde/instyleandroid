package e.agenciainstyle.com.model;

/**
 * Created by Jacob Velarde on 06,September,2020
 */
public class Perfil {

    private String nombres;
    private String apellidos;
    private String correo;
    private String telefono;
    private String perfil;
    private String url_profile;

    public Perfil() {
    }

    public Perfil(String nombres, String apellidos, String correo, String telefono, String perfil, String url_profile) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.telefono = telefono;
        this.perfil = perfil;
        this.url_profile = url_profile;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getUrl_profile() {
        return url_profile;
    }

    public void setUrl_profile(String url_profile) {
        this.url_profile = url_profile;
    }
}
