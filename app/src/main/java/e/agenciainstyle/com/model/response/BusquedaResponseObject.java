package e.agenciainstyle.com.model.response;

import java.util.ArrayList;

import e.agenciainstyle.com.model.Evento;

public class BusquedaResponseObject {

    private FiltrosResponse filtros;
    private ArrayList<Evento> activaciones;

    public BusquedaResponseObject() {
    }

    public BusquedaResponseObject(FiltrosResponse filtros, ArrayList<Evento> activaciones) {
        this.filtros = filtros;
        this.activaciones = activaciones;
    }

    public FiltrosResponse getFiltros() {
        return filtros;
    }

    public void setFiltros(FiltrosResponse filtros) {
        this.filtros = filtros;
    }

    public ArrayList<Evento> getActivaciones() {
        return activaciones;
    }

    public void setActivaciones(ArrayList<Evento> activaciones) {
        this.activaciones = activaciones;
    }
}
