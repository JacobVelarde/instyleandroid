package e.agenciainstyle.com.model;

public class Reporte {

    private String id_evento;
    private String nombre_evento;

    public Reporte() {
    }

    public Reporte(String id_evento, String nombre_evento) {
        this.id_evento = id_evento;
        this.nombre_evento = nombre_evento;
    }

    public String getId_evento() {
        return id_evento;
    }

    public void setId_evento(String id_evento) {
        this.id_evento = id_evento;
    }

    public String getNombre_evento() {
        return nombre_evento;
    }

    public void setNombre_evento(String nombre_evento) {
        this.nombre_evento = nombre_evento;
    }
}
