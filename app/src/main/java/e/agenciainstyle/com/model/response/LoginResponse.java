package e.agenciainstyle.com.model.response;

import com.google.gson.annotations.SerializedName;

import e.agenciainstyle.com.model.User;
import e.agenciainstyle.com.server.GenericResponse;

public class LoginResponse extends GenericResponse {

    @SerializedName("objeto")
    private User user;

    public LoginResponse() {
    }

    public LoginResponse(int codigo, String mensaje, User user) {
        super(codigo, mensaje);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "user=" + user +
                '}';
    }
}
