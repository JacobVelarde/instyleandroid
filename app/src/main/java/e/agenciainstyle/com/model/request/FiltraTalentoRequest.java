package e.agenciainstyle.com.model.request;

public class FiltraTalentoRequest {

    private String genero;
    private String edad;
    private String altura_mts;
    private String peso_kg;
    private String calzado;
    private String ojos;
    private String cabello;
    private String saco;
    private String camisa;
    private String pantalon;
    private String pecho;
    private String cintura;
    private String cadera;
    private String especialidad;
    private String nacionalidad;
    private String tipo;

    public FiltraTalentoRequest() {
        genero = "0";
        edad = "0";
        altura_mts = "0";
        peso_kg = "0";
        calzado = "0";
        ojos = "0";
        cabello = "0";
        saco = "0";
        camisa = "0";
        pantalon = "0";
        pecho = "0";
        cintura = "0";
        cadera = "0";
        especialidad = "0";
        nacionalidad = "0";
        tipo = "0";
    }

    public FiltraTalentoRequest(String genero, String edad, String altura_mts, String peso_kg, String calzado, String ojos, String cabello, String saco, String camisa, String pantalon, String pecho, String cintura, String cadera, String especialidad, String tipo) {
        this.genero = genero;
        this.edad = edad;
        this.altura_mts = altura_mts;
        this.peso_kg = peso_kg;
        this.calzado = calzado;
        this.ojos = ojos;
        this.cabello = cabello;
        this.saco = saco;
        this.camisa = camisa;
        this.pantalon = pantalon;
        this.pecho = pecho;
        this.cintura = cintura;
        this.cadera = cadera;
        this.especialidad = especialidad;
        this.tipo = tipo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getAltura_mts() {
        return altura_mts;
    }

    public void setAltura_mts(String altura_mts) {
        this.altura_mts = altura_mts;
    }

    public String getPeso_kg() {
        return peso_kg;
    }

    public void setPeso_kg(String peso_kg) {
        this.peso_kg = peso_kg;
    }

    public String getCalzado() {
        return calzado;
    }

    public void setCalzado(String calzado) {
        this.calzado = calzado;
    }

    public String getOjos() {
        return ojos;
    }

    public void setOjos(String ojos) {
        this.ojos = ojos;
    }

    public String getCabello() {
        return cabello;
    }

    public void setCabello(String cabello) {
        this.cabello = cabello;
    }

    public String getSaco() {
        return saco;
    }

    public void setSaco(String saco) {
        this.saco = saco;
    }

    public String getCamisa() {
        return camisa;
    }

    public void setCamisa(String camisa) {
        this.camisa = camisa;
    }

    public String getPantalon() {
        return pantalon;
    }

    public void setPantalon(String pantalon) {
        this.pantalon = pantalon;
    }

    public String getPecho() {
        return pecho;
    }

    public void setPecho(String pecho) {
        this.pecho = pecho;
    }

    public String getCintura() {
        return cintura;
    }

    public void setCintura(String cintura) {
        this.cintura = cintura;
    }

    public String getCadera() {
        return cadera;
    }

    public void setCadera(String cadera) {
        this.cadera = cadera;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
