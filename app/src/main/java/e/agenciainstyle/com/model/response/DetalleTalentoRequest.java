package e.agenciainstyle.com.model.response;

import e.agenciainstyle.com.model.TalentoDetalle;
import e.agenciainstyle.com.server.GenericResponse;

public class DetalleTalentoRequest extends GenericResponse {

    private TalentoDetalle objeto;

    public DetalleTalentoRequest(TalentoDetalle objeto) {
        this.objeto = objeto;
    }

    public DetalleTalentoRequest(int codigo, String mensaje, TalentoDetalle object) {
        super(codigo, mensaje);
        this.objeto = object;
    }

    public TalentoDetalle getObjeto() {
        return objeto;
    }

    public void setObject(TalentoDetalle objeto) {
        this.objeto = objeto;
    }
}
