package e.agenciainstyle.com.model;

/**
 * Created by Jacob Velarde on 05,September,2020
 */
public class IdNotificacion {

    private String notificacion_id;

    public IdNotificacion() {
    }

    public IdNotificacion(String notificacion_id) {
        this.notificacion_id = notificacion_id;
    }

    public String getNotificacion_id() {
        return notificacion_id;
    }

    public void setNotificacion_id(String notificacion_id) {
        this.notificacion_id = notificacion_id;
    }
}
